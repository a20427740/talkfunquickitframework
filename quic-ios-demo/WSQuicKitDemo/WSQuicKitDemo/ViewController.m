//
//  ViewController.m
//  WSQuicKitDemo
//
//  Created by yansheng wei on 2019/6/27.
//  Copyright © 2019 WangSu Inc. All rights reserved.
//

#import "ViewController.h"
#import "WSQuicUseInterface.h"
#import "CNCRecorderMenuViewController.h"
#import "ScanQrcodeViewController.h"

@interface ViewController ()<UIGestureRecognizerDelegate,UITextFieldDelegate,WSQuicUseDelegate, ScanQrcodeDelegate>

@property (nonatomic, strong) IBOutlet UIView *mEnterBar1;
@property (nonatomic, strong) IBOutlet UIView *mEnterBar2;

@property (nonatomic, strong) IBOutlet UITextField *mAppKeyField;
@property (nonatomic, strong) IBOutlet UITextField *mAppSecretField;

@property (nonatomic, strong) IBOutlet UILabel *mVersionLabel;
@property (nonatomic, strong) IBOutlet UIButton *mButton;

//方便测试隐藏控件
@property (nonatomic, strong) IBOutlet UITextField *mUrlField;
@property (nonatomic, strong) IBOutlet UIButton *mLoadButton;

@property (nonatomic, strong) MBProgressHUD *hud;

///是否已鉴权
@property (nonatomic, assign, getter=isAuth) BOOL auth;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // 防顶部遮挡
    self.edgesForExtendedLayout =UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.title = @"网宿Quic推流";
    [WSQuicUseInterface instance].delegate = self;
    [self InitialViewSetting];
    
    [self checkPermission];
}

- (void)checkPermission {
    AVAuthorizationStatus videoAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (videoAuthStatus == AVAuthorizationStatusNotDetermined) {// 未询问用户是否授权
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            NSLog(@"requestAccessForMediaTypeVideo granted:%@", @(granted));
            
            AVAuthorizationStatus audioAuthStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
            if (audioAuthStatus == AVAuthorizationStatusNotDetermined) {// 未询问用户是否授权
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
                    NSLog(@"requestAccessForMediaTypeAudio granted:%@", @(granted));
                }];
            }
        }];
    }
}

- (void)InitialViewSetting {
    self.navigationItem.rightBarButtonItem = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(scanQrcodeAction:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0 , 0, 30, 25);
        [button setImage:[UIImage imageNamed:@"scan_qrcode"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"scan_qrcode"] forState:UIControlStateHighlighted];
        [self.view addSubview:button];
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        rightItem;
    });
    
    self.mEnterBar1.layer.borderWidth = 1.0f;
    self.mEnterBar1.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    self.mEnterBar2.layer.borderWidth = 1.0f;
    self.mEnterBar2.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    NSString *app_id = [[NSUserDefaults standardUserDefaults] objectForKey:kQrCodeKey_QuicKitAppId];
    NSString *auth_key = [[NSUserDefaults standardUserDefaults] objectForKey:kQrCodeKey_QuicKitAuthKey];
    self.mAppKeyField.text = app_id;
    self.mAppSecretField.text = auth_key;
    
    self.mVersionLabel.font = [UIFont systemFontOfSize:15];
    self.mVersionLabel.textColor = [UIColor darkGrayColor];
    self.mVersionLabel.textAlignment = NSTextAlignmentCenter;
    self.mVersionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.mVersionLabel.text = [NSString stringWithFormat:@"版本号:%@  build号:%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    [self add_gesture_recognizer];
}

- (void)scanQrcodeAction:(UIButton *)sender {
    ScanQrcodeViewController *scanQrcodeViewController = [ScanQrcodeViewController new];
    scanQrcodeViewController.delegate = self;
    [self.navigationController pushViewController:scanQrcodeViewController animated:YES];
}

- (IBAction)action_authLogin:(UIButton *)sender {
    dispatch_async(dispatch_get_global_queue(0, 0), ^(){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.auth) {
                [self pushNextView];
                return;
            }
            
            NSString *appKey = self.mAppKeyField.text;
            NSString *appSecret = self.mAppSecretField.text;
            
            if (!self.hud) {
                self.hud = [[MBProgressHUD alloc] initWithView:self.view];
            }
            self.hud.removeFromSuperViewOnHide = YES;
            [self.view addSubview:self.hud];
            [self.hud show:YES];
            
            if (appKey.length <= 0 || appSecret.length <= 0) {
                self.hud.labelText = @"鉴权信息不能为空";
                [self.hud hide:YES afterDelay:1];
            } else {
                self.hud.labelText = @"正在MUF鉴权";
                [WSQuicUseInterface async_register_quic_kit:appKey with_app_secret:appSecret];
            }
        });
    });
}

- (IBAction)action_loader:(UIButton *)btn {
    self.mLoadButton.hidden = YES;
    self.mUrlField.hidden = YES;

    NSObject *o = [self _get_data_from_loader:self.mUrlField.text];
    if (![o isKindOfClass:[NSDictionary class]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"加载信息失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        
        return;
    }
    
   NSDictionary *d = (NSDictionary *)o;
    [self updateAuthInfo:d];
}

- (void)updateAuthInfo:(NSDictionary *)info {
    NSString *tmp = nil;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    {
        tmp = [info objectForKey:@"a1"];
        if (tmp != nil && [tmp length] > 0) {
            self.mAppKeyField.text = tmp;
            [userDefaults setObject:tmp forKey:kQrCodeKey_QuicKitAppId];
        }
    }
    
    {
        tmp = [info objectForKey:@"k1"];
        if (tmp != nil && [tmp length] > 0) {
            self.mAppSecretField.text = tmp;
            [userDefaults setObject:tmp forKey:kQrCodeKey_QuicKitAuthKey];
        }
    }
    
    {
        tmp = [info objectForKey:@"a2"];
        if (tmp != nil && [tmp length] > 0) {
            [userDefaults setObject:tmp forKey:kQrCodeKey_StreamPushAppId];
        }
    }
    
    {
        tmp = [info objectForKey:@"k2"];
        if (tmp != nil && [tmp length] > 0) {
            [userDefaults setObject:tmp forKey:kQrCodeKey_StreamPushAuthKey];
        }
    }
    
    {
        tmp = [info objectForKey:@"a3"];
        if (tmp != nil && [tmp length] > 0) {
            [userDefaults setObject:tmp forKey:kQrCodeKey_PlayerAppId];
        }
    }
    
    {
        tmp = [info objectForKey:@"k3"];
        if (tmp != nil && [tmp length] > 0) {
            [userDefaults setObject:tmp forKey:kQrCodeKey_PlayerAuthKey];
        }
    }
    
    {
        tmp = [info objectForKey:@"url_push"];
        if (tmp != nil && [tmp length] > 0) {
            [userDefaults setObject:tmp forKey:kQrCodeKey_StreamPushRtmpUrl];
        }
    }
    
    {
        tmp = [info objectForKey:@"url_play"];
        if (tmp != nil && [tmp length] > 0) {
            [userDefaults setObject:tmp forKey:kQrCodeKey_PlayerRtmpUrl];
        }
    }
  
    {
        NSObject *quic_hook_config = [info objectForKey:@"quic_cfg"];
        if (quic_hook_config != nil) {
            [userDefaults setObject:quic_hook_config forKey:kQrCodeKey_QuicConfig];
        }
    }
    
    [userDefaults synchronize];
}

- (NSObject *)_get_data_from_loader:(NSString *)base_url {
    NSString *fullUrl = [NSString stringWithFormat:@"%@",base_url];
    NSURL *url = [NSURL URLWithString:fullUrl];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.timeoutInterval = 5.0;
    
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    NSData *rsp_data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    
    if (httpResponse.statusCode != 200) {
        return nil;
    }
    
    NSString *rsp_str = [[NSString alloc] initWithData:rsp_data encoding:NSUTF8StringEncoding];
    
    NSError *jsonError;
    NSData *objectData = [rsp_str dataUsingEncoding:NSUTF8StringEncoding];
    NSObject *result = [NSJSONSerialization JSONObjectWithData:objectData
                                                       options:NSJSONReadingMutableContainers
                                                         error:&jsonError];
    
    return result;
}

#pragma mark - UITextFieldDelegate
- (void)textField_resignFirstResponder {
    [self.mAppKeyField resignFirstResponder];
    [self.mAppSecretField resignFirstResponder];
    [self.mUrlField resignFirstResponder];
}

#pragma mark - UIGestureRecognizerDelegate
//手势
- (void)add_gesture_recognizer {
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap_gesture:)];
    tapGes.delegate = self;
    
    UISwipeGestureRecognizer *swipeLeftGes = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handle_swipes:)];
    swipeLeftGes.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeLeftGes.numberOfTouchesRequired = 2;
    swipeLeftGes.delegate = self;
    
    UISwipeGestureRecognizer *swipeRightGes = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handle_swipes:)];
    swipeRightGes.direction = UISwipeGestureRecognizerDirectionRight;
    swipeRightGes.numberOfTouchesRequired = 2;
    swipeRightGes.delegate = self;
    
    [self.view addGestureRecognizer:tapGes];
//    [self.view addGestureRecognizer:swipeLeftGes];
    [self.view addGestureRecognizer:swipeRightGes];
}

- (void)tap_gesture:(UITapGestureRecognizer *)tapGes {
    [self textField_resignFirstResponder];
}

#pragma mark 以下代码纯属方便测试 非DEMO业务代码
- (void)handle_swipes:(UISwipeGestureRecognizer *)sender {
    //纯属方便测试 非DEMO业务代码
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self textField_resignFirstResponder];
        self.auth = NO;
        self.mUrlField.hidden = YES;
        self.mLoadButton.hidden = YES;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"您已经清空鉴权信息 请重新鉴权" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (sender.direction == UISwipeGestureRecognizerDirectionRight) {
        self.mUrlField.hidden = NO;
        self.mLoadButton.hidden = NO;
        return;
    }
}

#pragma mark - wsquicdelegate
- (void)registerResult:(NSInteger)resultCode {
  dispatch_async(dispatch_get_main_queue(), ^{
    int delay = 1;
    if (resultCode == 0) {
      self.hud.labelText = @"MUF鉴权成功";
      self.auth = YES;
    } else {
      self.auth = NO;
      delay = 2;
      self.hud.labelText = @"MUF鉴权失败,请重新启动APP";
    }
    [self.hud hide:YES afterDelay:delay];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      if (self.auth) {
        [[NSUserDefaults standardUserDefaults] setObject:self.mAppKeyField.text forKey:@"WSQuicKit_App_ID"];
        [[NSUserDefaults standardUserDefaults] setObject:self.mAppSecretField.text forKey:@"WSQuicKit_Auth_Key"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self pushNextView];
      }
      [self.mButton setEnabled:YES];
    });
  });
}

- (void)pushNextView {
    CNCRecorderMenuViewController *vc = [[CNCRecorderMenuViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma --mark ScanQrcodeDelegate
- (void)scanQrcodeResult:(NSString *)metadata {
    NSLog(@"%@", metadata);
    NSError *jsonError;
    NSData *objectData = [metadata dataUsingEncoding:NSUTF8StringEncoding];
    NSObject *result = [NSJSONSerialization JSONObjectWithData:objectData
                                                       options:NSJSONReadingMutableContainers
                                                         error:&jsonError];
    
    NSDictionary *d = (NSDictionary *)result;
    [self updateAuthInfo:d];
}

@end
