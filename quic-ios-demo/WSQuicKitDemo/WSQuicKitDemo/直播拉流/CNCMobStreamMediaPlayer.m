//
//  CNCMobStreamMediaPlayer.m
//  CNCMobStreamDemo
//
//  Created by yansheng wei on 2019/5/16.
//  Copyright © 2019 cad. All rights reserved.
//

#import "CNCMobStreamMediaPlayer.h"
#import <WSQuicKit/WSQuicKit.h>
#import "CNCDemoFunc.h"

@interface CNCMobStreamMediaPlayer () {
    CGFloat screen_w_;
    CGFloat screen_h_;
}

@property (nonatomic, strong) CNCMediaPlayerController *mPlayController;

@property (nonatomic, strong) UIButton *closeButton;

@property (nonatomic, assign, getter=isClosed) BOOL closed;

@property (nonatomic, retain) UILabel *stat;

@end

@implementation CNCMobStreamMediaPlayer
- (void)dealloc {
    NSLog(@"CNCMobStreamMediaPlayer dealloc");
    self.closeButton = nil;
    self.mPlayController = nil;
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.closed = NO;
    if (self.rtmp_name) {
        [self onStartPlayRTMP:self.rtmp_name surfaceView:self.view];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    int y = 10 + (IPHONE_X ? 24 : 0);
    CGFloat buttonWidth = 50;
    int screen_w_ = self.view.bounds.size.width;
    UIButton *btn = [[[UIButton alloc] initWithFrame:CGRectMake(screen_w_-buttonWidth, y, buttonWidth, buttonWidth)] autorelease];
    btn.tag = 10000;
    btn.backgroundColor = [UIColor clearColor];
    [btn setImage:[UIImage imageNamed:@"clear_input"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(actionDismissViewController:) forControlEvents:UIControlEventTouchUpInside];
    self.closeButton = btn;
    [self.view addSubview:self.closeButton];
    
    screen_w_ = [UIScreen mainScreen].bounds.size.width;
    
    self.stat = ({
        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(30, 20, screen_w_ - 2 * 30, 30)] autorelease];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor redColor];
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont boldSystemFontOfSize:14];
        
        label;
    });
    [self.view addSubview:self.stat];
    
    [self updateStat];
}

- (void)onStartPlayRTMP:(NSString *)url surfaceView:(UIView *)view {
    
    __block NSString *pull_url = url;
    __block UIView *surface = view;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"player init with url:%@", pull_url);
        CNCMediaPlayerController *player = [[[CNCMediaPlayerController alloc] initWithContentURL:[NSURL URLWithString:pull_url]] autorelease];
        player.scalingMode = CNC_MP_VIDEO_SCALE_MODE_ASPECTFIT;
        player.videoDecoderMode = CNC_VIDEO_DECODER_MODE_SOFTWARE;
        player.playMode = CNC_MEDIA_PLAYER_MODE_LIVE;
        player.maxCacheTime = 10000;
        player.minBufferTime = 1000;
        player.maxBufferSize = 15;
        player.cncDNSTimeout = 1*1000;
        player.shouldAutoClearCache = 1;
        player.shouldAutoPlay = YES;
        player.view.frame = surface.bounds;
        player.view.tag = 123321;
        player.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [surface insertSubview:player.view atIndex:0];
        [player setLogLevel:CNC_MediaPlayer_Loglevel_Warn];
        self.mPlayController = player;
        [self.mPlayController prepareToPlay];
        
        // 注册播放器通知
        [self registerPlayerNotification];
    });
}

- (void)onStopPlayRTMP {
    __block UIView *surface = self.view;
    self.closed =  YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIView *subview = [surface viewWithTag:123321];
        [subview removeFromSuperview];
        
        // 移除播放器通知
        [self removePlayerNotification];
        // 关闭播放器
        [self.mPlayController stop];
        [self.mPlayController shutdown];
        self.mPlayController = nil;
        
        //解决ijk自动锁屏问题
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    });
}

#pragma mark -MediaPlayer Notification
/**
 注册播放器通知
 */
- (void)registerPlayerNotification {
    // 注册播放器播放停止通知(监听停止通知，用于流切换时刷新播放器)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncMediaPlayerPlayDidFinish:) name:CNCMediaPlayerPlayDidFinishNotification object:self.mPlayController];
    // 播放器状态
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncMediaPlayerStatus:) name:CNCMediaPlayerStatusCodeNotification object:self.mPlayController];
    
    // 流切换
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncMediaPlayerFindNewStream:) name:CNCMediaPlayerFindNewStream object:self.mPlayController];
}

/**
 移除播放器通知
 */
- (void)removePlayerNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CNCMediaPlayerPlayDidFinishNotification object:self.mPlayController];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CNCMediaPlayerStatusCodeNotification object:self.mPlayController];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CNCMediaPlayerFindNewStream object:self.mPlayController];
}

/**
 播放器停止播放通知
 
 @param notification 通知对象
 */
- (void)cncMediaPlayerPlayDidFinish:(NSNotification *)notification {
    // TODO:播放器播放停止时，尝试重连一次，如果源还在，会继续播放
    NSLog(@"player finish notify %@", notification);
    CNCMediaPlayerController  *player = [notification object] ;
    
    CNCMediaPlayerDidFinish nKeyValue = [[[notification userInfo] valueForKey:CNCMediaPlayerPlayDidFinishUserInfoKey] integerValue];
    switch (nKeyValue) {
        case CNC_MEDIA_PLAYER_DID_FINISH_END:
        case CNC_MEDIA_PLAYER_DID_FINISH_ERROR: {
            [self reloadPlayer:player];
        }
            break;
        default:
            break;
    }
}

/**
 播放器状态通知
 
 @param notification 通知对象
 */
- (void)cncMediaPlayerStatus:(NSNotification *)notification {
    CNCMediaPlayerController  *player = [notification object] ;
    CNC_MediaPlayer_ret_Code nKeyValue = [[[notification userInfo] valueForKey:CNCMediaPlayerStatusKey] integerValue];
    
    switch (nKeyValue) {
        case CNC_MediaPlayer_RCode_Parse_URL_Failed:
        case CNC_MediaPlayer_RCode_Player_Connect_Server_Failed: {
            [self reloadPlayer:player];
        }
            break;
        default:
            break;
    }
}

/**
 流数据变更通知
 @param notification 通知对象
 */
- (void)cncMediaPlayerFindNewStream:(NSNotification *)notification {
    CNCMediaPlayerController  *player = [notification object] ;
    [self reloadPlayer:player]; // 流数据变更时 进行重连
}

- (void)reloadPlayer:(CNCMediaPlayerController *)player  {
    __weak CNCMediaPlayerController *wplayer = player;
    __weak CNCMobStreamMediaPlayer *wself = self;
    dispatch_time_t timer = dispatch_time(DISPATCH_TIME_NOW,  5.0 * NSEC_PER_SEC);
    dispatch_after(timer, dispatch_get_main_queue(), ^(void){
        __strong CNCMobStreamMediaPlayer *sself = wself;
        if ([sself isClosed]) {
          NSLog(@"已经关闭了，不要再进来了!!");
        } else {
          __strong CNCMediaPlayerController *splayer = wplayer;
          NSLog(@"GCD-----%@",[NSThread currentThread]);
          splayer.minBufferTime = 1000;
          [splayer reloadWithContentURL:nil fromStart:YES];
        }
    });
}


#pragma mark -

- (void)actionDismissViewController:(id)sender {
  [self onStopPlayRTMP];
  [self dismissViewControllerAnimated:YES completion:^(){
      // 上报QUIC日志
      [WSQuicKit reportQuicLog];
  }];
}


- (void)updateStat {
    NSString* infoQuic = [NSString stringWithFormat:@"Quic %@", self.bQuicEnable ? @"Opened" : @"Closed"];
    _stat.text = infoQuic;
}
@end
