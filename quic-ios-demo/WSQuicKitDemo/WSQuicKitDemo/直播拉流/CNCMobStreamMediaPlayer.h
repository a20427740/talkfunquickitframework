//
//  CNCMobStreamMediaPlayer.h
//  CNCMobStreamDemo
//
//  Created by yansheng wei on 2019/5/16.
//  Copyright © 2019 cad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CNCMobStreamMediaPlayer : UIViewController
    
@property (nonatomic, copy) NSString *rtmp_name;

@property (nonatomic) BOOL bQuicEnable;

@end

NS_ASSUME_NONNULL_END
