//
//  CNCPlayerViewController.h
//  CNCMediaPlayerDemo
//
//  Created by Hjf on 2017/12/22.
//  Copyright © 2017年 CNC. All rights reserved.
//

#import <UIKit/UIKit.h>

//#ifdef CNC_MediaPlayer_Full
//#import "CNCMediaPlayerFramework/CNCMediaPlayerController.h"
//#import "CNCMediaPlayerFramework/CNCMediaPlayerFramework.h"
//#else
//#import "CNCLiveMediaPlayerFramework/CNCMediaPlayerController.h"
//#import "CNCLiveMediaPlayerFramework/CNCMediaPlayerFramework.h"
//#endif

@interface CNCPlayerSetting : NSObject

@property (nonatomic) BOOL isAutoPlay;
@property (nonatomic) BOOL isLive;
@property (nonatomic) BOOL isHardware;
@property (nonatomic) BOOL isAutoClearCache;
@property (nonatomic) BOOL isDisableDecodeBackground;
@property (nonatomic) BOOL isPlayBackground;
@property (nonatomic) BOOL isAccelerateOpen;
@property (nonatomic) BOOL isMixOtherPlayer;
@property (nonatomic) BOOL isSuperAccelerate;
@property (nonatomic) BOOL isLowLatencyMode;
@property (nonatomic) BOOL isHLSAdaptation;
@property (nonatomic) BOOL isLocalCache;
@property (nonatomic) BOOL isAccurateSeek;
@property (nonatomic) BOOL isOpenIV;
@property (nonatomic) BOOL isOpenHLSEncryption;
@property (nonatomic) int isMutliOpen;

@property (nonatomic) NSInteger minBufferTime;
@property (nonatomic) NSInteger minBufferByte;
@property (nonatomic) NSInteger maxCacheTime;
@property (nonatomic) NSInteger maxBufferSize;
@property (nonatomic) int gifScale;
@property (nonatomic) int64_t HLSDefaultBitrate;

@property (nonatomic) NSInteger cacheVideoCount;
@property (nonatomic) NSInteger cacheVideoSize;
@property (nonatomic) NSInteger cacheVideoType; //1 count, 2 size

@property (nonatomic) NSUInteger loopCount;
@property (nonatomic) BOOL loopOn;

@property (nonatomic) BOOL isProxy;
@property (nonatomic) BOOL isHTTPS;
@property (nonatomic, assign) NSUInteger    type;
@property (nonatomic, strong) NSString      *socksUser;
@property (nonatomic, strong) NSString      *socksPwd;
@property (nonatomic, strong) NSString      *socksIP;
@property (nonatomic, strong) NSString      *socksPort;
@property (nonatomic, strong) NSString      *hlsDecryptionVideoId;


@property (nonatomic, strong) NSString      *urlString;
@property (nonatomic, strong) CNCPlayerSetting  *mutilSetting;

@property (nonatomic) BOOL isOpenVodCache;
@property (nonatomic) NSUInteger bufferType;

//preload
@property (nonatomic) BOOL isOpenPreload;
@property (nonatomic) BOOL isOpenPreloadCycTest;
@property (nonatomic) NSUInteger preloadCount;
@property (nonatomic) NSUInteger preloadByte;
@property (nonatomic) NSUInteger preloadSourceType;

//dash
@property (nonatomic) BOOL isOpenDashDefaultBandWidth;
@property (nonatomic) int dashDefaultVideoBandWidth;
@property (nonatomic) int dashDefaultAudioBandWidth;

//timeout
@property (nonatomic) NSUInteger dnsTimeout;
@property (nonatomic) BOOL isOpenDNSTimeout;
@property (nonatomic) NSUInteger connectTimeout;
@property (nonatomic) BOOL isOpenConnecTimeout;

+ (instancetype)defaultSetting;
//+ (instancetype)defaultSettingCopy;
@end

@interface CNCPlayerViewController : UIViewController
@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) CNCPlayerSetting *setting;
@property (nonatomic) BOOL bQuicEnable;

- (void)shutdownPlayer;
- (void)adpaterFor2OpenIn1;
@end
