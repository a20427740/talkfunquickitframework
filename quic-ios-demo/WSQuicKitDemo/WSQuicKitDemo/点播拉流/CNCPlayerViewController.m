//
//  CNCPlayerViewController.m
//  CNCMediaPlayerDemo
//
//  Created by Hjf on 2017/12/22.
//  Copyright © 2017年 CNC. All rights reserved.
//

#import "CNCPlayerViewController.h"
#import "CNCPlayerCommonFunc.h"
#import <AssetsLibrary/AssetsLibrary.h>

#import "MBProgressHUD.h"
// 获取ip相关
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <net/if.h>

#import <sys/utsname.h>

#import <MediaPlayer/MPVolumeView.h>
#import "CNCInfoTableViewController.h"

#define Screen_Record_Alert_Tag 2000
#define NETWORK_INVAIL @"NETWORK_INVAIL"
#define NETWORK_CELLUAR @"NETWORK_CELLUAR"
#define NETWORK_WIFI @"WIFI"

@implementation CNCPlayerSetting

+ (instancetype)defaultSettingCopy {
  CNCPlayerSetting *setting = [[CNCPlayerSetting alloc] init];
  return setting;
}

+ (instancetype)defaultSetting {
  return [CNCPlayerSetting defaultSettingCopy];
}

- (instancetype)init {
  if (self = [super init]) {
    self.isLive = NO;
    self.isAutoPlay = YES;
    self.isHardware = NO;
    self.isAccelerateOpen = YES;
    self.isAutoClearCache = NO;
    self.isMixOtherPlayer = NO;
    self.isPlayBackground = NO;
    self.isDisableDecodeBackground = NO;
    self.isSuperAccelerate = NO;
    self.isLowLatencyMode = NO;
    self.isHLSAdaptation = NO;
    self.isLocalCache = NO;
    self.isAccurateSeek = NO;
    self.maxCacheTime = 5000;
    self.minBufferTime = 1000;
    self.minBufferByte= 256000;
    self.maxBufferSize = 15;
    self.gifScale = 270;
    self.HLSDefaultBitrate = 0;
    self.cacheVideoCount = 10;
    self.cacheVideoSize = 50;
    self.loopCount = 0;
    self.isProxy = NO;
  }
  return self;
}


@end

//#define Use_Global_Volume
@interface CNCPlayerViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    // 刷新控制界面显示定时器
    NSTimer *_refresh_timer;
    // 刷新动态信息定时器
    NSTimer *_refresh_dyinfo_timer;
    // 播放器是否stop
    BOOL _isVideoOver;
    // 播放异常，结束播放
    BOOL _isFinishError;
    // 进度条是否正在拖拉
    BOOL _isMediaSliderBeingDragged;
    
    // 是否开始录制
    BOOL _isRecordStart;
    // 是否开始录屏
    BOOL _isScreenRecordStart;
    // 断网重连
    /* 是否断网过 */
    BOOL _isConnectionBreak; // 切换网络时判断
    // IP变化时判断
    BOOL _isSecondTime;
    // 监听ip是否变化定时器
    NSTimer *_startNotifyTimer;
    // 记录是否是进入后台导致的停止录制，若是，则录制结束的原因要报suspend，此处是demo演示
    BOOL _isBackgroundStopRecord;
    // 当前的显示比例
    CncMpVideoScalingMode _currentScaling;
    // 时移上限
    NSInteger _timeShiftDuration;
    // 控制缓冲动画图片数组
    NSArray *_arrayAnimation;
    // 记录日志沙盒路径
    NSString *_logFilePath;
    /* 第一个点击点 */
    CGPoint     _first_point;
    /* 最后一个点击点的x坐标 */
    NSInteger   _lastPosition_x;
    /* 最后一个点击点的y坐标 */
    NSInteger   _lastPosition_y;
    /* 是否第一次进度拖拉 */
    BOOL        _is_first_translation;
    /* 拖拉方向 */
    WSGestureDirection          _pan_direction;
    /* 音量控制 */
    UISlider    *_sliderVolume;
    // 记录当前屏幕亮度
    CGFloat _currentBrightness;
    // 记录进入播放前的屏幕亮度
    CGFloat _originBrightness;
    // 播放速率倍数
    NSInteger _playBackRate;
    // 是否正在答题中
    BOOL _isQuestion;
    // 记录上次题目
    NSString *_lastQuestionStr;
    // HLS码率选择记录
    NSInteger _HLSBitrateCount;
    // HLS码率数组
    NSArray *_HLSBitrateArray;
    // 检测卡顿时长，进行切换码率提示
    NSInteger _startBufferTime;
    // 监听缓冲等待时间,进行切换码率提醒
    NSTimer *_alertChangeBitrateTimer;
    // 记录网络不稳定时的errorCode
    NSString *_lastErrorCode;
    
    NSMutableString *logMessageCache;
    
    CALayer *screenRecordLayer;
    CGRect screenRecordRect;
    UIView *screenRecordToRecordView;
    
    int _cur_recording_type;
    
    BOOL loggingCancel;
    BOOL recordTypeChoose;
    BOOL adpatorForMutliOpen;
    //    CGRect infoTableFrame;
    
    
    //cnc dash
    NSInteger cncDashVideoSelectedIndex;
    NSInteger cncDashAudioSelectedIndex;
    
}
@property (weak, nonatomic) IBOutlet UIView *viewGesture;
// xib控件
@property (weak, nonatomic) IBOutlet UIView *viewDisplayVideo;
@property (weak, nonatomic) IBOutlet UILabel *labelVideoTitle;
@property (weak, nonatomic) IBOutlet UITextView *textViewLog;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecordVideo;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecordGIF;
@property (weak, nonatomic) IBOutlet UIButton *buttonRecordScreen;
@property (weak, nonatomic) IBOutlet UIButton *buttonMediaInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonTimeShift;
@property (weak, nonatomic) IBOutlet UIButton *buttonFullScreen;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlayOrPause;
@property (weak, nonatomic) IBOutlet UIButton *buttonHLSBitrate;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayBackRate;


@property (strong, nonatomic) IBOutlet UIView *viewPlayerUI;
@property (weak, nonatomic) IBOutlet UILabel *labelCurrentPlayTime;
@property (weak, nonatomic) IBOutlet UILabel *labelVIdeoDuration;
@property (weak, nonatomic) IBOutlet UIProgressView *progressViewCache;
@property (weak, nonatomic) IBOutlet UISlider *sliderCurrentPlayTime;
@property (weak, nonatomic) IBOutlet UISlider *sliderTimeShift;
@property (weak, nonatomic) IBOutlet UIButton *btnDash;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopViewDisplayVideo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftViewDisplayVideo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRightViewDisplayVideo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewDisplayVideo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftBack;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftVolume;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftRate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftBitrate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftPlay;

@property (strong, nonatomic) CNCInfoTableViewController *infoTableViewController;

//L
@property (weak, nonatomic) IBOutlet UIButton *buttonReload;

@property (strong, nonatomic) UITableView *media_infomation;        // 视频信息
@property (strong, nonatomic) UIButton *buttonRecordScreenView;     // 截图
@property (strong, nonatomic) UIView *viewFullScreen;               // 全屏
@property (strong, nonatomic) UIImageView *imageViewWaiting;        // 缓冲动画

// 监听网络变化
@property (nonatomic) BOOL          startNotification;
@property (atomic,strong) NSString* lastIPAddress;
@property (nonatomic, strong) CNCMediaPlayerController *player;
@property (nonatomic, strong) CNCMediaPlayerController *player1;
@property (nonatomic, strong) CNCMediaPlayerController *player2;
@property (nonatomic, strong) CNCMediaPlayerController *player3;
@property (nonatomic, strong) CNCMediaPlayerController *player4;
@property (weak, nonatomic) IBOutlet UIButton *btnMute;

//iv
@property (weak, nonatomic) IBOutlet UIButton *btnIVModel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segIVDisplayModel;
@property (weak, nonatomic) IBOutlet UISlider *ivPercentSlide;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ivTvTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *urlHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *urlTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *labelDashStatus;

//手势图层
@property (strong, nonatomic) UIView *viewFullScreenGesture;               //全屏手势图层
@property (strong, nonatomic) UIView *viewDisplayVideoGesture;               //半全屏手势图层

//cnc dash
@property (strong, nonatomic) NSArray *cncDashVideoArray;
@property (strong, nonatomic) NSArray *cncDashAudioArray;


@end
//#define Test_Dismiss
@implementation CNCPlayerViewController
- (void)testDismiss {
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        while (1) {
            sleep(1);

            if (self.player.currentPlaybackTime > 1) {
                break;
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.player.audioDuration > 0) {
                [self OnClickback:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Test_Dismiss" object:nil userInfo:@{@"type":@(0)}];
            }
            
        });
        
    });
}

- (void)viewDidLoad {

#ifdef Test_Dismiss
    [self testDismiss];
#endif
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self initData];
    [self initPlayer];
    [self initNotification];
    [self initView];
    [self interfaceOrientation:UIInterfaceOrientationPortrait];
    
    [self showLog:[NSString stringWithFormat:@"Quic %@", self.bQuicEnable ? @"Opened" : @"Closed"]];
    //    self.additionalSafeAreaInsets
    
//    [self setupMultiPlayer];
}
//强制转屏
- (void)interfaceOrientation:(UIInterfaceOrientation)orientation
{
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector  = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val = orientation;
        // 从2开始是因为0 1 两个参数已经被selector和target占用
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
}


- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self initLayout];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _currentBrightness = _originBrightness = [UIScreen mainScreen].brightness;
    
    //
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _currentBrightness = _originBrightness = [UIScreen mainScreen].brightness;
    _logFilePath = [CNCPlayerCommonFunc getLog:[NSString stringWithFormat:@"%@%@.txt",@"Log",[CNCPlayerCommonFunc getTimeNowWithMilliSecond]]];
    //    [self.player prepareToPlay];
//    [self showLog:CNCLString(@"txt_prepare_to_play")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -- init
// 初始化UI控件
- (void)initViewL {
    [self.buttonReload setTitle:CNCLString(@"txt_reload") forState:UIControlStateNormal];;
    [self.buttonRecordGIF setTitle:CNCLString(@"txt_record_gif") forState:UIControlStateNormal];;
    [self.buttonRecordVideo setTitle:CNCLString(@"txt_record_video") forState:UIControlStateNormal];;
    [self.buttonRecordScreen setTitle:CNCLString(@"txt_record_screen") forState:UIControlStateNormal];;
}
- (void)initData {
    _currentBrightness = -1;
    _originBrightness = -1;
    
    cncDashAudioSelectedIndex = 0;
    cncDashVideoSelectedIndex = 0;

}
- (void)initView {
    [self initViewL];
    // 添加播放view和UIview
    [self.viewDisplayVideo addSubview:self.player.view];
    
    // 信息面板
    self.infoTableViewController = [[CNCInfoTableViewController alloc] init];
    self.infoTableViewController.view.hidden = YES;
    self.infoTableViewController.view.userInteractionEnabled = NO;
    [self.viewDisplayVideo addSubview:self.infoTableViewController.tableView];
    [self setInfoValue:nil forKey:CNCLString(@"txt_realtime_fps")];
    [self setInfoValue:nil forKey:CNCLString(@"txt_resolution")];
    [self setInfoValue:nil forKey:CNCLString(@"txt_bitrate")];
    
    self.viewPlayerUI.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    [self.viewDisplayVideo addSubview:self.viewPlayerUI];
    [self.viewDisplayVideo bringSubviewToFront:self.infoTableViewController.tableView];
    
    // 视频信息tableview
    self.media_infomation = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.media_infomation.backgroundColor = [UIColor whiteColor];
    self.media_infomation.delegate = self;
    self.media_infomation.dataSource = self;
    self.media_infomation.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.media_infomation.hidden = YES;
    [self.view addSubview:self.media_infomation];

    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidden_information)];
    [self.media_infomation addGestureRecognizer:recognizer];

    _refresh_timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(refresh_media_information) userInfo:nil repeats:YES];
    [_refresh_timer setFireDate:[NSDate distantFuture]];
    _refresh_dyinfo_timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(refresh_media_dyinformation) userInfo:nil repeats:YES];
    [_refresh_dyinfo_timer setFireDate:[NSDate date]];

    // 视频标题
    self.labelVideoTitle.text = [NSString stringWithFormat:@"%@", self.URL];
    self.labelVideoTitle.numberOfLines = 0;
    self.labelVideoTitle.adjustsFontSizeToFitWidth = YES;

    // 进度条
    [self.sliderCurrentPlayTime setThumbImage:[CNCPlayerCommonFunc OriginImage:[UIImage imageNamed:@"滑块"] scaleToSize:CGSizeMake(15, 15)] forState:UIControlStateNormal];
    if (self.setting.isLive) {
        self.sliderCurrentPlayTime.enabled = NO;
    }

    [self.sliderTimeShift setThumbImage:[CNCPlayerCommonFunc OriginImage:[UIImage imageNamed:@"滑块"] scaleToSize:CGSizeMake(15, 15)] forState:UIControlStateNormal];
    self.sliderTimeShift.value = self.sliderTimeShift.maximumValue;
    self.sliderTimeShift.hidden = YES;

    // 播放控制界面显示与隐藏
    UITapGestureRecognizer *_recognizerDisplay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOrHideCtrlView)];
    _recognizerDisplay.numberOfTapsRequired = 1;
    [self.viewGesture addGestureRecognizer:_recognizerDisplay];
    [self hideCtrlView];

    // 手势
    UIPanGestureRecognizer *recognizerPan = [[UIPanGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(handleSwipe:)];
    [self.viewGesture addGestureRecognizer:recognizerPan];
    _is_first_translation = YES;
    
    // 声音控制
    MPVolumeView *my_volume_view = [[MPVolumeView alloc] initWithFrame:CGRectMake(20, 150, [UIScreen mainScreen].bounds.size.width - 40, 30)];
    for (UIView *view in [my_volume_view subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            _sliderVolume = (UISlider*)view;
            break;
        }
    }
    // 全屏View
    UITapGestureRecognizer *_recognizerDisplay1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOrHideCtrlView)];
    _recognizerDisplay1.numberOfTapsRequired = 1;
    self.viewFullScreen = [[UIView alloc] initWithFrame:CGRectZero];
    self.viewFullScreen.backgroundColor = [UIColor blackColor];
    self.viewFullScreen.hidden = YES;
    
    [self.view addSubview:self.viewFullScreen];
    [self.view bringSubviewToFront:self.viewFullScreen];
    // 手势
    UIPanGestureRecognizer *recognizerPan1 = [[UIPanGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handleSwipe:)];
    // 双击切换显示比例
    UITapGestureRecognizer *recognizerDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeScalingMode:)];
    recognizerDoubleTap.numberOfTapsRequired = 2;
    //    [self.viewDisplayVideo addGestureRecognizer:recognizerDoubleTap];
    [self.viewGesture addGestureRecognizer:recognizerDoubleTap];
    
    UITapGestureRecognizer *recognizerDoubleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeScalingMode:)];
    recognizerDoubleTap1.numberOfTapsRequired = 2;
    
    self.labelCurrentPlayTime.adjustsFontSizeToFitWidth = YES;
    self.labelVIdeoDuration.adjustsFontSizeToFitWidth = YES;
    
    _arrayAnimation = [NSArray arrayWithObjects:[UIImage imageNamed:@"加载"], [UIImage imageNamed:@"加载_1"], [UIImage imageNamed:@"加载_2"], [UIImage imageNamed:@"加载_3"], [UIImage imageNamed:@"加载_4"], [UIImage imageNamed:@"加载_5"], [UIImage imageNamed:@"加载_6"], [UIImage imageNamed:@"加载_7"], nil];
    
    // 缓冲动画
    self.imageViewWaiting = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    self.imageViewWaiting.animationImages = _arrayAnimation;
    self.imageViewWaiting.animationDuration = 1;
    [self.imageViewWaiting startAnimating];
    self.imageViewWaiting.hidden = YES;
    [self.viewDisplayVideo addSubview:self.imageViewWaiting];
    
    // 日志框
//    [self.textViewLog setEditable:NO];
    
    // 自动播放，修改按钮状态
    if (self.setting.isAutoPlay) {
        [self.buttonPlayOrPause setSelected:YES];
    }
    
    // HLS默认播放码率
    [self.buttonHLSBitrate setTitle:[NSString stringWithFormat:@"%lld", self.setting.HLSDefaultBitrate] forState:UIControlStateNormal];
    
    [self.sliderCurrentPlayTime.superview bringSubviewToFront:self.sliderCurrentPlayTime];
    [self.sliderTimeShift.superview bringSubviewToFront:self.sliderTimeShift];
    
    
    //dash
    self.labelDashStatus.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    if ([[self.URL absoluteString] hasSuffix:@".mpd"]) {
        self.btnDash.alpha = 1;
        self.labelDashStatus.alpha = 1;

    } else {
        self.btnDash.alpha = 0;
        self.labelDashStatus.alpha = 0;
    }
    
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showLogView)];
    swipeGesture.numberOfTouchesRequired = 3;
    [self.view addGestureRecognizer:swipeGesture];

    
}

- (void)showLogView {
    self.textViewLog.hidden = !self.textViewLog.hidden;
}

- (void)initLayout {
    NSInteger width = [UIScreen mainScreen].bounds.size.width;
    NSInteger height = [UIScreen mainScreen].bounds.size.height;
    
    if (width > height) {
        self.viewFullScreen.frame = CGRectMake(0, 0, width, height);
    } else {
        self.media_infomation.frame = CGRectMake(20, self.viewDisplayVideo.frame.origin.y, width-40, height-40);
        self.viewFullScreen.frame = CGRectMake(0, 0, height, width);
        self.player.view.frame = self.viewDisplayVideo.bounds;
        self.viewPlayerUI.frame = self.viewDisplayVideo.bounds;
        self.imageViewWaiting.center = self.viewDisplayVideo.center;
    }
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString: systemInfo.machine encoding:NSASCIIStringEncoding];
    if ([platform isEqualToString:@"iPhone10,3"] || [platform isEqualToString:@"iPhone10,6"] || [platform isEqualToString:@"x86_64"]) {
        self.constraintTopViewDisplayVideo.constant = 30;
        if ([UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeLeft
            || [UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeRight
            ) {
            self.constraintLeftBack.constant = self.constraintLeftVolume.constant = self.constraintLeftRate.constant = self.constraintLeftBitrate.constant = self.constraintLeftPlay.constant = 30;
        } else {
            self.constraintLeftBack.constant = self.constraintLeftVolume.constant = self.constraintLeftRate.constant = self.constraintLeftBitrate.constant = 10;
            self.constraintLeftPlay.constant = 5;
        }
    }
    
    // 信息面板
    [self updateInfoTableViewFrame];
    
    if (adpatorForMutliOpen) {
        self.media_infomation.frame = CGRectMake(20, self.viewDisplayVideo.frame.origin.y, width-40, height/2-40);
    } else {
    }
}
- (void)updateInfoTableViewFrame {
    CGRect selfFrame = self.viewDisplayVideo.frame;
    CGRect newFrame  = CGRectMake(selfFrame.size.width * 3 / 5, 0, selfFrame.size.width * 2 / 5, selfFrame.size.height);
    self.infoTableViewController.tableView.frame = newFrame;
}

// 初始化播放器 设置参数
- (void)initPlayer {
    NSMutableDictionary *option = [[NSMutableDictionary alloc] init];
    //点播缓存如果开启时，并且需要在直播模式下自动关闭，需要设置此参数
    if (self.setting.isLive) {
        [option setObject:@"1" forKey:CNCMediaPlayerOptionIsLive];
    } else {
        [option setObject:@"0" forKey:CNCMediaPlayerOptionIsLive];
    }
    //IV 包名绑定 兼容非注册 live 包名失效
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *bundleID = [bundle objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    if (self.setting.isOpenIV) {
        
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:bundlePath];
        NSString *versionLiveIVKey = [dict objectForKey:@"versionLiveIVKey"];
        NSString *versionFullIVKey = [dict objectForKey:@"versionFullIVKey"];
        
        
        if ([bundleID containsString:@"Live"]) {
            if (versionLiveIVKey) {
                [option setObject:versionLiveIVKey forKey:CNCMediaPlayerOptionIVDevKey];
            }
        } else {
            if (versionFullIVKey) {
                [option setObject:versionFullIVKey forKey:CNCMediaPlayerOptionIVDevKey];
            }
        }
    }
    
    BOOL mainPlayer;
    if (self.setting.isMutliOpen != -1) {
        [option setObject:@"1" forKey:CNCMediaPlayerOptionIsMainPlayer];
        mainPlayer = YES;
        
    } else {
        [option setObject:@"0" forKey:CNCMediaPlayerOptionIsMainPlayer];
        //超分模式暂时未支持多开，非主播放器暂时停用超分模式
        mainPlayer = NO;
    }
    
    if (self.setting.isOpenIV) {
        [option setObject:@"1" forKey:CNCMediaPlayerOptionIsOpenIV];
    } else {
        [option setObject:@"0" forKey:CNCMediaPlayerOptionIsOpenIV];
    }

    self.player = [[CNCMediaPlayerController alloc] initWithContentURL:self.URL option:option];
    NSLog(@"SDK support iv = %d",[self.player cncMediaPlayerLibMngSupportIVFramework]);
    if (self.setting.isOpenIV && [self.player cncMediaPlayerLibMngSupportIVFramework]) {
        //第三方支持
        [self initIV_View:YES];
    } else {
        [self initIV_View:NO];
        if (self.setting.isOpenIV) {
            [self showLog:[NSString stringWithFormat:@"%@ %@ %@",CNCLString(@"txt_resolution_reestablish"),CNCLString(@"comm_open"),CNCLString(@"comm_fail")]];
        }
    }
    [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"comm_create"),CNCLString(@"comm_player")]];
    
    if (self.player) {
        if (self.setting.isOpenIV) {
            [self ivInfoUpdate];
        }
#if DEBUG
        //TODO:debug 模式下可选debug，若日志太多干扰，可选warn
        [self.player setLogLevel:CNC_MediaPlayer_Loglevel_Warn];
#else
        [self.player setLogLevel:CNC_MediaPlayer_Loglevel_SILENT];
        
#endif
        //设置直播或者点播
        [self.player setPlayMode:self.setting.isLive ? CNC_MEDIA_PLAYER_MODE_LIVE : CNC_MEDIA_PLAYER_MODE_VOD];
        
        //设置是否自动播放
        [self.player setShouldAutoPlay:self.setting.isAutoPlay];
        
        //设置解码方式:(软解、硬解)
        [self.player setVideoDecoderMode:self.setting.isHardware ? CNC_VIDEO_DECODER_MODE_HARDWARE : CNC_VIDEO_DECODER_MODE_SOFTWARE];
        
        //设置延时追赶(直播场景下使用，如果在点播使用会导致缓存不断被清除)
        [self.player setShouldAutoClearCache:self.setting.isAutoClearCache];
        
        //设置是否后台播放
        [self.player setAbleVideoPlayingInBackground:self.setting.isPlayBackground];
        
        //设置是否进行后台视频解码，不解码可以节约设备资源，节省电量
        [self.player setDisableVideoDecodeInBackground:self.setting.isDisableDecodeBackground];
        
        //设置快启，加快首屏
        [self.player accelerateOpen:self.setting.isAccelerateOpen];
        
        //设置是否兼容其他app的音频，开启时其他app的音频不会中断播放器音频，否则其他app的音频播放会中断播放器的播放
        [self.player setMixOtherPlayer:self.setting.isMixOtherPlayer];
        
        //设置缓冲最小时长(大小)，卡顿时需加载到这里设置的时长才会恢复播放状态
        if (self.setting.bufferType == 0) {
            [self.player setMinBufferTime:self.setting.minBufferTime];
        } else{
            [self.player setMinBufferByte:self.setting.minBufferByte];
        }
        
        //设置延时追赶时间，缓存时长超过此阈值时，进行追赶
        [self.player setMaxCacheTime:self.setting.maxCacheTime];
        
        //设置低延时模式，延时追赶采用变速播放进行，使追赶更加平滑，点播不生效
        [self.player setShouldLowLatencyMode:self.setting.isLowLatencyMode];
        
        //设置HLS码率自适应
        [self.player setHLSAdaptation:self.setting.isHLSAdaptation];
        
        //设置HLS码率自适应的默认播放码率
        [self.player setDefaultHLSBitrate:self.setting.HLSDefaultBitrate];
        
        //设置解码前缓冲队列大小，默认15MB，设置值较大时则加载更多但影响内存占用
        [self.player setMaxBufferSize:self.setting.maxBufferSize];
        
        //设置缩放模式，参见CncMpVideoScalingMode
        [self.player setScalingMode:CNC_MP_VIDEO_SCALE_MODE_ASPECTFIT];
        
       /*
        设置是否开启精准seek
        精准seek会在触发seek时找到相应位置的关键帧后，
        快速解码到seek位置，不会产生找关键帧而导致seek后位置左右跳动
        */
        [self.player setEnableAccurateSeek:self.setting.isAccurateSeek];
        
        //设置循环播放次数
        [self.player setLoop:self.setting.loopCount];
        
        //设置HLS解密参数
        [_player openHLSEncryption:self.setting.isOpenHLSEncryption withVideoID:self.setting.hlsDecryptionVideoId];
        
        //设置超级快启
        [self.player superAccelerate:self.setting.isSuperAccelerate];

        //设置dns超时时间
        if (self.setting.isOpenDNSTimeout) {
            [_player setCncDNSTimeout:self.setting.dnsTimeout * 1000];
        }

        //设置连接超时时间
        if (self.setting.isOpenConnecTimeout) {
            [_player setCncConnectTimeout:self.setting.connectTimeout * 1000];
        }
        
        //设置dash默认码率
        if (self.setting.isOpenDashDefaultBandWidth) {
            [_player setDefaultDashVideoBandwidth:_setting.dashDefaultVideoBandWidth audioBandwidth:_setting.dashDefaultAudioBandWidth];
        }

        //连接其他超时
//        [_player setCncTimeout:50 * 1000];
        
        if (self.setting.isProxy) {
            switch (self.setting.type) {
                    // socks5
                case 0:
                    [self.player enableSocks5:self.setting.socksUser pwd:self.setting.socksPwd ip:self.setting.socksIP port:self.setting.socksPort];
                    break;
                    // socks4
                case 1:
                    [self.player enableSocks4WithIP:self.setting.socksIP port:self.setting.socksPort];
                    break;
                    // http
                case 2:
                    [self.player enableHttpProxyWithUsername:self.setting.socksUser pwd:self.setting.socksPwd IP:self.setting.socksIP port:self.setting.socksPort isHtpps:self.setting.isHTTPS];
                    break;
                default:
                    break;
            }
        }
        //cnc dash add
        __weak typeof(self) weakSelf = self;
        [self.player setDashVideoBandwidthBlock:^(NSArray *bandwidthList) {
            NSLog(@"video dash callback :%@",bandwidthList);
            weakSelf.cncDashVideoArray = bandwidthList;
            
        } audioBandwidthBlock:^(NSArray *bandwidthList) {
            NSLog(@"audio dash callback :%@",bandwidthList);
            weakSelf.cncDashAudioArray = bandwidthList;
            
        }];
        
        
        [self.player prepareToPlay];
    }
}

// 注册通知
- (void)initNotification {
    // prepare完成
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerDidPrepare) name:CNCMediaPlayerLoadDidPrepareNotification object:self.player];
    // 播放状态通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerPlayStateChanged:) name:CNCMediaPlayerPlayStateDidChangeNotification object:self.player];
    // 加载状态通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerloadStateChanged:) name:CNCMediaPlayerLoadStateDidChangeNotification object:self.player];
    // 播放结束
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerDidFinish:) name:CNCMediaPlayerPlayDidFinishNotification object:self.player];
    // 视频比例改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerScalingModeChanged) name:CNCMediaPlayerScalingModeDidChangeNotification object:self.player];
    // 视频分辨率改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerResolutionChanged:) name:CNCMediaPlayerResolutionChangedNotification object:self.player];
    // 首帧视频
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerFirstVideoRender) name:CNCMediaPlayerFirstVideoFrameRenderedNotification object:self.player];
    // 首帧音频
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerFirstAudioRender) name:CNCMediaPlayerFirstAudioFrameRenderedNotification object:self.player];
    // seek完成
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerSeekDidFinish:) name:CNCMediaPlayerDidSeekCompleteNotification object:self.player];
    // 播放器状态
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerStatus:) name:CNCMediaPlayerStatusCodeNotification object:self.player];
    // 录制状态通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerRecordStatus:) name:CNCMediaPlayerRecordingStatusCodeNotification object:self.player];
    
    // 录屏状态通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerRecordStatus:) name:CNCMediaPlayerScreenRecordingStatusCodeNotification object:nil];
    
    // http返回值
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerHTTPRcode:) name:CNCMediaPlayerHTTPRCodeNotification object:self.player];
    // 播放器释放完毕
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerDidShutdown) name:CNCMediaPlayerDidShutdown object:self.player];
    
    // sei上报通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cncPlayerGetSEI:) name:CNCMediaPlayerWSCustomSEI object:self.player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HLSBitrate:) name:CNCMediaPlayerHLSNotification object:self.player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFindNewStream:) name:CNCMediaPlayerFindNewStream object:self.player];
    
    
    //cnc dash notif
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dashVideoChangedNoti:) name:CNCMediaPlayerDashVideoChanged object:self.player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dashAudioChangedNoti:) name:CNCMediaPlayerDashAudioChanged object:self.player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dashInitBandwidthNoti:) name:CNCMediaPlayerDashInitBandwidth object:self.player];
    
  
    // 系统前台切后台
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    // 系统后台切前台
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    // 设备物理方向旋转
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationDidChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    NSLog(@"add Notification Observer by player id = %@",[self.player description]);
}



#pragma mark -- uninit
// 释放其他资源
- (void)_release {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"_release removeObserver");
    
    // 停止监听网络
    [self stop_notify];
    // 停止刷新UI显示
    [_refresh_timer invalidate];
    _refresh_timer = nil;
    
    [_refresh_dyinfo_timer invalidate];
    _refresh_dyinfo_timer = nil;
    
    if (_alertChangeBitrateTimer != nil) {
        [_alertChangeBitrateTimer invalidate];
        _alertChangeBitrateTimer = nil;
    }
    // 停止缓冲动画
    [self.imageViewWaiting stopAnimating];
    // delegate置为nil
    self.media_infomation.delegate = nil;
    self.media_infomation.dataSource = nil;
    //
    [self.player.view removeFromSuperview];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshMediaControl) object:nil];
    
    

    [self removeAllGestureInView:self.media_infomation];
    [self removeAllGestureInView:self.viewGesture];
    [self removeAllGestureInView:self.view];
    self.viewGesture = nil;
}

- (void) removeAllGestureInView:(UIView *) view {
    NSMutableArray *newges = [NSMutableArray arrayWithArray:view.gestureRecognizers];
    for (int i =0; i<[newges count]; i++) {
        UIGestureRecognizer *g = [newges objectAtIndex:i];
        [g removeTarget:self action:nil];
        NSLog(@"remove gesture = %@",[newges objectAtIndex:i]);
        [view removeGestureRecognizer:[newges objectAtIndex:i]];
    }
}

#pragma mark -- private method
- (void)showLog:(NSString *)message {
    if (loggingCancel) {
        return;
    }
    if (!logMessageCache) {
        logMessageCache = [[NSMutableString alloc] init];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            while (self.player) {
                sleep(1);
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.textViewLog.text = logMessageCache;//[self.textViewLog.text stringByAppendingString:logMessageCache];
                    [self.textViewLog scrollRectToVisible:CGRectMake(0, self.textViewLog.contentSize.height-15, self.textViewLog.contentSize.width, 10) animated:YES];
                });
            }
            
        });
        
    }
//    int maxLen = 10000;
    int maxLine = 200;
    NSArray *lines = [logMessageCache componentsSeparatedByString:@"\n"];
    NSUInteger line = [lines count];
    if (line > maxLine) {
        int loc = line - maxLine;
        NSString *str = [lines objectAtIndex:loc];
        NSRange range = [logMessageCache rangeOfString:str];
        logMessageCache = [NSMutableString stringWithString:[logMessageCache substringFromIndex:range.location]];
        NSLog(@"log msg cut:line=%d loc=%d str=%@,range=%@",line,loc,str,NSStringFromRange(range));
    }
    [logMessageCache appendFormat:@"[%@] %@\n", [CNCPlayerCommonFunc getTimeNow], message];
}

// 保存视频至相册
- (void)saveVideoToPhtotlibrary:(NSString *)filePath {
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    if (author == ALAuthorizationStatusRestricted || author ==ALAuthorizationStatusDenied){
        [self showLog:CNCLString(@"txt_no_photo_auth")];
        return;
    }
    
    if (!filePath) {
        return;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isExist = [fileManager fileExistsAtPath:filePath];
    
    if (!isExist) {
        [self showLog:[NSString stringWithFormat:@"%@==%@",CNCLString(@"txt_file_path_does_not_exist"),filePath]];
        return;
    }
    
    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath)) {
        UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
    } else {
        [self showLog:[NSString stringWithFormat:@"%@ [%@]",CNCLString(@"txt_save_to_photo_album_fail"),filePath]];
    }
}
// 保存视频至相册回调
- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInf{
    if (error) {
        NSLog(@"Error during saving Video ，erro info:%@",error.localizedDescription);
        [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"comm_save"),CNCLString(@"comm_fail")]];
    }else{
        [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"comm_save"),CNCLString(@"comm_success")]];
    }
}
// 保存图片至相册回调
- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (!error) {
        [self showLog:CNCLString(@"txt_save_to_photo_album_suc")];
    }else
    {
        [self showLog:CNCLString(@"txt_save_to_photo_album_fail")];
    }
}
// 刷新播放控件显示
- (void)refreshMediaControl{
    self.btnMute.selected = [self.player mute];
    
    //    NSLog(@"player = %@ refreshMediaControl cur=%f",[self.player description],self.player.currentPlaybackTime);
    //[self updatePlayBackRate];
    if (self.sliderTimeShift.hidden == NO) {
        NSTimeInterval current_position = self.player.currentPlaybackTime;
        self.labelVIdeoDuration.text = @"--:--";
        NSInteger diff = _timeShiftDuration - self.sliderTimeShift.value;
        if (_player.playbackState != CNC_PLAYER_STATE_ON_MEDIA_PAUSE) {
             self.labelCurrentPlayTime.text = [NSString stringWithFormat:@"-%02d:%02d/%02d:%02d", (int)(diff / 60), (int)(diff % 60), (int)current_position/60, (int)current_position%60];
        }
       
        if (_player.playbackState == CNC_PLAYER_STATE_ON_MEDIA_STOP) {
            self.labelCurrentPlayTime.text = @"00:00";
        }
    } else {
        // duration
        NSTimeInterval duration = self.player.duration;
        NSInteger intDuration = floor(duration);
        if (intDuration > 0) {
            self.sliderCurrentPlayTime.maximumValue = intDuration;
            self.labelVIdeoDuration.text = [NSString stringWithFormat:@"%02d:%02d", (int)(intDuration / 60), (int)(intDuration % 60)];
        } else {
            self.labelVIdeoDuration.text = @"--:--";
            self.sliderCurrentPlayTime.maximumValue = 1.0f;
        }
        
        // 当前播放位置
        NSTimeInterval position;
        if (_isMediaSliderBeingDragged) {
            position = self.sliderCurrentPlayTime.value;
        } else {
            position = self.player.currentPlaybackTime;
        }
        
        if (_player.playbackState != CNC_PLAYER_STATE_ON_MEDIA_PAUSE) {
            NSInteger intPosition = ceil(position);
            if (intDuration > 0) {
                self.sliderCurrentPlayTime.value = intPosition;
            } else {
                self.sliderCurrentPlayTime.value = 0.0f;
            }
            
            if ((intPosition >= intDuration) && !self.setting.isLive) {
                self.labelCurrentPlayTime.text = [NSString stringWithFormat:@"%02d:%02d", (int)(intDuration / 60), (int)(intDuration % 60)];
            } else {
                self.labelCurrentPlayTime.text = [NSString stringWithFormat:@"%02d:%02d", (int)(intPosition / 60), (int)(intPosition % 60)];
            }
        }
        
        if (self.player.playbackState == CNC_PLAYER_STATE_ON_MEDIA_STOP) {
            self.labelCurrentPlayTime.text = @"00:00";
        }
        
        // 缓存位置
        NSTimeInterval bufferPosition;
        bufferPosition = self.player.playableDuration;
        NSInteger intbufferPosition = bufferPosition+0.5;
        if (!self.setting.isLive) {
            if (intbufferPosition > 0 && bufferPosition > 0 && self.player.duration > 0) {
                [self.progressViewCache setProgress:intbufferPosition/self.player.duration animated:YES];
            } else {
                [self.progressViewCache setProgress:0.0f];
            }
        }
    }
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshMediaControl) object:nil];
    if (self.viewPlayerUI.alpha != 0) {
        [self performSelector:@selector(refreshMediaControl) withObject:nil afterDelay:0.5];
    }
}


- (void)start_recording:(UIButton *) aButton {
    NSString *recordFormat;
    NSString *recordFilename;
    
    if (!aButton.isSelected) {
        // 开始录制视频后 点击录制gif，点击结束录制 会导致文件名的改变
        // 解决，demo限制条件，若在录制，则return,并且修改按钮状态
        if ([self.player isRecording]) {
            [self showLog:[NSString stringWithFormat:@"%@...",CNCLString(@"txt_recording")]];
            [aButton setSelected:NO];
            return;
        }
        // 类型赋值放在最外层if上面的话会导致录制mp4过程中点击录制gif，不会弹出保存相框提示
        switch (aButton.tag) {
            case 0:
            {
                recordFormat = @"mp4";
                recordFilename = [CNCPlayerCommonFunc getVideoRecord:[NSString stringWithFormat:@"%@%@",@"video",[CNCPlayerCommonFunc getTimeNowWithMilliSecond]]];
            }
                break;
            case 1:
            {
                recordFormat = @"gif";
                recordFilename = [CNCPlayerCommonFunc getGifRecord:[NSString stringWithFormat:@"%@%@",@"gif",[CNCPlayerCommonFunc getTimeNowWithMilliSecond]]];
            }
                break;
            default:
            {
                recordFormat = @"mp4";
                recordFilename = [CNCPlayerCommonFunc getVideoRecord:[NSString stringWithFormat:@"%@%@",@"video",[CNCPlayerCommonFunc getTimeNowWithMilliSecond]]];
            }
                break;
        }
        
        if ([recordFormat isEqualToString:@"gif"]) {
            if ([self.player startRecordingWithFilename: recordFilename format:recordFormat minTime:500 maxTime:30000 gifScale:self.setting.gifScale] < 0 ) {
                [aButton setSelected:NO];
            } else {
                [self startUpdateRecrodingTime:YES type:0 button:aButton];
                
            }
        } else {
            if ([self.player startRecordingWithFilename: recordFilename format:recordFormat minTime:3000 maxTime:60000] < 0 ) {
                [aButton setSelected:NO];
            } else {
                [self startUpdateRecrodingTime:YES type:0 button:aButton];
                
            }
        }
        _isBackgroundStopRecord = NO;
        [aButton setSelected:YES];
    } else {
        if ([self.player stopRecording]) {
            [self startUpdateRecrodingTime:NO type:0 button:aButton];
            [aButton setSelected:NO];
        }
        
    }
}
- (void)openScreanRecorderOntype:(CNC_MediaPlayer_Screen_Record_Type) type button:(UIButton *)button{
    [self openScreanRecorderOntype:type button:button toRecordView:nil];
}
- (void)openScreanRecorderOntype:(CNC_MediaPlayer_Screen_Record_Type) type button:(UIButton *)button toRecordView:(UIView *) toRecordView{
    NSString *title = CNCLString(@"txt_full_screen");
    if ([toRecordView isEqual:self.textViewLog]) {
        title = CNCLString(@"txt_bottom_screen");
    } else if ([toRecordView isEqual:self.viewDisplayVideo]){
        title = CNCLString(@"txt_top_screen");
    }
    
    self.buttonRecordScreenView = [[UIButton alloc] initWithFrame:button.frame];
    self.buttonRecordScreenView.backgroundColor = [UIColor lightGrayColor];
    self.buttonRecordScreenView.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.buttonRecordScreenView.layer.cornerRadius = 5;
    [self.buttonRecordScreenView setTitle:title forState:UIControlStateNormal];
    self.buttonRecordScreenView.backgroundColor = [UIColor blueColor];
    [self.buttonRecordScreenView addTarget:self action:@selector(actionRecordingStartOrStop:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonRecordScreenView addTarget:self action:@selector(actionRecordingCancel:) forControlEvents:UIControlEventTouchUpOutside];
    screenRecordToRecordView = toRecordView;
    NSMutableDictionary *option = [[NSMutableDictionary alloc] init];
    if (toRecordView) {
        toRecordView.layer.borderColor = [[UIColor redColor] CGColor];
        toRecordView.layer.borderWidth = 1.5;
        
        [option setObject:[NSNumber numberWithInteger:25] forKey:@"fps"];
        
        if ([self.player showRecorderView:self.buttonRecordScreenView toRecordView:toRecordView type:type option:option]) {
            [self showLog:CNCLString(@"txt_screen_rec_open_suc")];
        } else {
            //        [self showLog:@"打开录屏失败,可能已经打开过一个"];
            [self showLog:CNCLString(@"txt_screen_rec_open_fail")];
            
        }
    } else {
 
    }
}
- (UIView *)currentRootView:(UIView *) curView {
    UIView *view = curView;
    while (view.superview) {
        view = view.superview;
    }
    return view;
}

- (void)addScreenRecordLayer{
    if (!screenRecordLayer) {
        screenRecordLayer  = [CALayer layer];
        screenRecordLayer.bounds    = self.view.bounds;//CGRectMake(0, 0, 100, 100);
        screenRecordLayer.position  = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
        screenRecordLayer.delegate  = self;
    }
    [screenRecordLayer setNeedsDisplay];
    [[[self currentRootView:self.view] layer] addSublayer:screenRecordLayer];
    
}

- (void)removeScreenRecordBorder{
    self.viewDisplayVideo.layer.borderWidth = 0;
    self.textViewLog.layer.borderWidth = 0;
}
- (void)removeScreenRecordLayer{
    [screenRecordLayer removeFromSuperlayer];
}
- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
    CGRect superBounds = self.view.bounds;
    if (screenRecordRect.origin.x > 0) {
        CGRect rect1 = CGRectMake(0, 0, screenRecordRect.origin.x, superBounds.size.height);
        CGContextAddRect(ctx,rect1);//画方框
        CGContextSetRGBFillColor(ctx,0, 0, 0, 1);
        CGContextSetAlpha(ctx, 0.5);
    }
    if (screenRecordRect.origin.y > 0) {
        CGRect rect1 = CGRectMake(screenRecordRect.origin.x, 0, superBounds.size.width - screenRecordRect.origin.x, screenRecordRect.origin.y);
        CGContextAddRect(ctx,rect1);//画方框
        CGContextSetRGBFillColor(ctx,0, 0, 0, 1);
        CGContextSetAlpha(ctx, 0.5);
    }
    if (screenRecordRect.origin.x + screenRecordRect.size.width < superBounds.size.width) {
        CGRect rect1 = CGRectMake(screenRecordRect.origin.x + screenRecordRect.size.width, screenRecordRect.origin.y, superBounds.size.width - (screenRecordRect.origin.x + screenRecordRect.size.width), superBounds.size.height - screenRecordRect.origin.y);
        CGContextAddRect(ctx,rect1);//画方框
        CGContextSetRGBFillColor(ctx,0, 0, 0, 1);
        CGContextSetAlpha(ctx, 0.5);
        
    }
    if (screenRecordRect.origin.y + screenRecordRect.size.height < superBounds.size.height) {
        CGRect rect1 = CGRectMake(screenRecordRect.origin.x , screenRecordRect.origin.y + screenRecordRect.size.height, screenRecordRect.size.width, superBounds.size.height -( screenRecordRect.origin.y + screenRecordRect.size.height));
        
        CGContextAddRect(ctx,rect1);//画方框
        CGContextSetRGBFillColor(ctx,0, 0, 0, 1);
        CGContextSetAlpha(ctx, 0.5);
        
    }
    
    CGContextFillPath(ctx);
}

- (void) actionRecordingCancel:(UIButton *) sender {
    [self removeScreenRecordLayer];
    [self.player hideRecorderCtrlView];
}

- (void) actionRecordingStartOrStop:(UIButton *) sender {
    UIButton *button = (UIButton *)sender;
    NSString *screenRecordFilename;
    NSString *screenRecordFormat;
    if (sender.isSelected) {
        [self showLog:CNCLString(@"txt_screen_rec_done")];
        
        if (self.player.screenRecordTime <= Min_Screen_Record_Time) {
            [self.player resetScreenRecordWithHandler:^(NSError * _Nullable error) {
                
                [self showLog:CNCLString(@"txt_screen_rec_cancel")];
                [self startUpdateRecrodingTime:NO type:1 button:button];
            }];
        } else {
            NSDictionary *dic = [self.player stopScreenRecordWithHandler:^(NSDictionary * _Nullable object, NSError * _Nullable error) {
                [self startUpdateRecrodingTime:NO type:1 button:button];
                if (error) {
                    //处理发生的错误，如磁盘空间不足而停止等
                    NSString *filePath = [[error userInfo] objectForKey:CNCMediaPlayerRecordFileName];
                    NSString *statukey = [[error userInfo] objectForKey:CNCMediaPlayerRecordingStatusKey];
                    NSString *info = [[error userInfo] objectForKey:CNCMediaPlayerRecordInfo];
                    [self showLog:[NSString stringWithFormat:@"%@,code:%@--info = %@--file path:%@",CNCLString(@"txt_screen_rec_fail"),statukey,info, [[filePath componentsSeparatedByString:@"/"] lastObject]]];
                } else {
                    NSString *filePath = [object objectForKey:CNCMediaPlayerRecordFileName];
                    NSString *statukey = [object objectForKey:CNCMediaPlayerRecordingStatusKey];
                    NSString *info = [object objectForKey:CNCMediaPlayerRecordInfo];
                    [self showLog:[NSString stringWithFormat:@"%@,code:%@--info = %@--file path:%@",CNCLString(@"txt_screen_rec_suc"),statukey,info, [[filePath componentsSeparatedByString:@"/"] lastObject]]];
                    [self saveVideoToPhtotlibrary:filePath];
                }
            }];
            
            if (dic) {
                NSString *filePath = [dic objectForKey:CNCMediaPlayerRecordFileName];
                NSString *statukey = [dic objectForKey:CNCMediaPlayerRecordingStatusKey];
                NSString *duration = [dic objectForKey:CNCMediaPlayerRecordDuration];
                [self showLog:[NSString stringWithFormat:@"%@,code:%@--duration = %@--file path = %@",CNCLString(@"txt_screen_rec_done"),statukey,duration, [[filePath componentsSeparatedByString:@"/"] lastObject]]];
            }
            
        }
        button.selected = NO;
        [self removeScreenRecordLayer];
        [self removeScreenRecordBorder];
        return;
    }
    
    if (!screenRecordToRecordView) {
        [self addScreenRecordLayer];
    }
    [self removeScreenRecordBorder];
    
    
    screenRecordFilename = [CNCPlayerCommonFunc getVideoRecord:[NSString stringWithFormat:@"sr%@",[CNCPlayerCommonFunc getTimeNowWithMilliSecond]]];
    screenRecordFormat = @"mov";
    
    int retcode = 0;
    retcode = [self.player startScreenRecordWithFilename:screenRecordFilename format:screenRecordFormat minTime:Min_Screen_Record_Time maxTime:Max_Screen_Record_Time handler:^(NSError * _Nullable error) {
        //        dispatch_async(dispatch_get_global_queue(0, 0), ^{
        //            sleep(10);
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //                [self.player prepareToPlay];
        //                NSLog(@"self.player prepareToPlay");
        //            });
        //        });
        
        [self showLog:CNCLString(@"txt_screen_rec_start")];
        if (error) {
            //开始失败
            self.buttonRecordScreenView.hidden = YES;//可自定义需不需要隐藏
            [self removeScreenRecordLayer];
            [self removeScreenRecordBorder];
            sender.selected = NO;
            [self showLog:[NSString stringWithFormat:@"%@ %@ : error = %@",CNCLString(@"txt_screen_rec_start"),CNCLString(@"comm_fail"),error]];
            [self startUpdateRecrodingTime:NO type:1 button:button];
        } else{
            [self startUpdateRecrodingTime:YES type:1 button:button];
        }
    } exceptionHandler:^(NSDictionary * _Nullable object,NSError * _Nullable error) {
        //异常中断分支
        [self removeScreenRecordLayer];
        [self removeScreenRecordBorder];
        
        [self startUpdateRecrodingTime:NO type:1 button:button];
        NSString *filePath = [[error userInfo] objectForKey:CNCMediaPlayerRecordFileName];
        NSString *statuskey = [[error userInfo] objectForKey:CNCMediaPlayerRecordingStatusKey];
        NSString *info = [[error userInfo] objectForKey:CNCMediaPlayerRecordInfo];
        if ([statuskey integerValue] == CNC_MediaPlayer_Rcode_Record_Succeed) {
            [self showLog:[NSString stringWithFormat:@"%@，code = %@--info = %@--file path = %@",CNCLString(@"txt_screen_rec_suc"),statuskey, info, [[filePath componentsSeparatedByString:@"/"] lastObject]]];
            [self saveVideoToPhtotlibrary:filePath];
        } else if ([statuskey integerValue] == CNC_MediaPlayer_Rcode_Record_Failed) {
            [self showLog:[NSString stringWithFormat:@"%@，code = %@--info = %@--file path = %@",CNCLString(@"txt_screen_rec_fail"),statuskey, info, [[filePath componentsSeparatedByString:@"/"] lastObject]]];
        }
        button.selected = NO;
    }] ;
    
    if (retcode > 0) {
        button.selected = YES;
    }
}
// 实时更新录制/录屏时间
- (void) startUpdateRecrodingTime:(BOOL) start type:(int) type button:(UIButton *)button{
    switch (type) {
        case 0:
            _isRecordStart = start;
            break;
        case 1:{
            _isScreenRecordStart = start;
            if (start) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CNCMediaPlayerRecordScreenStart" object:nil];
                
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CNCMediaPlayerRecordScreenEnd" object:nil];
            }
        }
            break;
        default:
            break;
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        do {
            CGFloat recordTime = 0;
            switch (type) {
                    // 录制
                case 0:{
                    if (_isRecordStart) {
                        recordTime = self.player.recordingDuration;
                    }
                }
                    break;
                    // 录屏
                case 1:{
                    if (_isScreenRecordStart) {
                        recordTime = self.player.screenRecordTime;
                    }
                }
                    break;
                default:
                    break;
            }
            recordTime /= 1000.0;
            dispatch_async(dispatch_get_main_queue(), ^{
                int hh = recordTime / 60 / 60;
                int mm = (int)(recordTime - hh*3600) / 60;
                int ss = (int)(recordTime - hh*3600 - mm*60) ;
                
                if (hh <= 0 && mm <= 0) {
                    [button setTitle:[NSString stringWithFormat:@"%d",ss ] forState:UIControlStateSelected];
                } else if (hh <= 0 && mm > 0) {
                    [button setTitle:[NSString stringWithFormat:@"%d:%d",mm,ss ] forState:UIControlStateSelected];
                } else if (hh > 0 && mm > 0) {
                    [button setTitle:[NSString stringWithFormat:@"%d:%d:%d",hh,mm,ss ] forState:UIControlStateSelected];
                }
            });
            usleep(500 * 1000);
        } while (_isRecordStart || _isScreenRecordStart);
    });
}

// 监听网络ip变化
- (void) start_notify {
    self.startNotification = YES;
    if (!_isSecondTime) {
        self.lastIPAddress = [CNCPlayerCommonFunc getIPAddress:YES];
    }
    _startNotifyTimer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(timer_action_ip_change_notify) userInfo:nil repeats:YES];
    [_startNotifyTimer fire];
}

- (void) stop_notify {
    [_startNotifyTimer invalidate];
    _startNotifyTimer = nil;
}

- (void) timer_action_ip_change_notify {
    if (![[CNCPlayerCommonFunc getIPAddress:YES] isEqualToString:self.lastIPAddress]) {
        self.lastIPAddress = [CNCPlayerCommonFunc getIPAddress:YES];
        // send error reset
        if (![self.lastIPAddress isEqualToString:@"0.0.0.0"]) {
            [self.player reloadWithContentURL:nil fromStart:NO];
            _isSecondTime = YES;
        }
    }
}

- (void)setInfoValue:(NSString *)value forKey:(NSString *)key
{
    if ([[NSThread currentThread] isMainThread]) {
        [self.infoTableViewController setValue:value forKey:key];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setInfoValue:value forKey:key];
        });
    }
}

- (void)shutdownPlayer {
    [self _release];
    [self.player stopScreenRecordWithHandler:^(NSDictionary * _Nullable object, NSError * _Nullable error) {
    }];
    [self.player shutdown];
    self.player = nil;
    
    if (_originBrightness >= 0) {
        NSLog(@"1 SET BRI = %f",_originBrightness);
        [[UIScreen mainScreen] setBrightness:_originBrightness];
    }
    //    [CNCMediaPlayerSDK setPlyerMute:0];
    [self shutdownMultiPlayer];
}

#pragma mark -- action
- (IBAction)OnClickRefreshPlayer:(id)sender {
    [self showLog:[NSString stringWithFormat:@"%@%@",CNCLString(@"txt_reload"),CNCLString(@"comm_player")]];
    [self.player reloadWithContentURL:nil fromStart:NO];
}
- (IBAction)OnClickRecordVideo:(id)sender {
    UIButton *button = (UIButton *)sender;
    [self start_recording:button];
}
- (IBAction)OnClickRecordGIF:(id)sender {
    UIButton *button = (UIButton *)sender;
    [self start_recording:button];
}
- (IBAction)OnClickRecordScreen:(id)sender {
    if (recordTypeChoose) {
        [self openScreanRecorderOntype:CNC_MediaPlayer_Screen_Record_Type_AVS button:self.buttonRecordScreen];
        return;
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:CNCLString(@"comm_alert_title") message:CNCLString(@"txt_choose_record_area") delegate:self cancelButtonTitle:CNCLString(@"txt_full_screen") otherButtonTitles:CNCLString(@"txt_top_screen"),CNCLString(@"txt_bottom_screen"), nil];
    alertView.tag = Screen_Record_Alert_Tag;
    [alertView show];
}

- (IBAction)OnClickback:(id)sender {
    
    if (self.viewFullScreen.hidden == YES || !self.viewFullScreen) {
        [self shutdownPlayer];
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CNCPlayer2OpenViewControllerBack" object:self.player];
        
    } else {
        [self.buttonFullScreen sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    [self removeScreenRecordLayer];
}
- (IBAction)OnClickVideoInfo:(id)sender {
    if (self.buttonMediaInfo.selected == NO) {
        [self show_information];
        [self.buttonMediaInfo setSelected:YES];
    }
}
- (IBAction)OnClickVideoInfoDy:(UIButton *)sender {
    self.infoTableViewController.tableView.hidden = !self.infoTableViewController.tableView.hidden;
    if (!self.infoTableViewController.tableView.hidden) {
        [_refresh_dyinfo_timer setFireDate:[NSDate date]];
    } else {
        [_refresh_dyinfo_timer setFireDate:[NSDate distantFuture]];
    }
}

- (IBAction)OnClickScreenshot:(id)sender {
    UIImage *screenshot = [self.player watermarkWithImage:[self.player screenShot] watermark:[UIImage imageNamed:@"水印"] inRect:CGRectMake([UIScreen mainScreen].bounds.size.width-100, [UIScreen mainScreen].bounds.size.height-100, 100, 100)];
    if (screenshot) {
        [self showLog:[NSString stringWithFormat:@"%@%@",CNCLString(@"txt_screen_shot"),CNCLString(@"comm_success")]];
        UIImageWriteToSavedPhotosAlbum(screenshot, self, @selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
    }
}
- (IBAction)OnClickTimeShift:(id)sender {
    if (!self.setting.isLive) {
        [self showLog:CNCLString(@"txt_vod_timestamp_offset_not_supported")];
        return;
    }
    UIButton *button = (UIButton *)sender;
    if (button.selected == NO) {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:CNCLString(@"txt_timestamp_offset_input") message:nil delegate:self cancelButtonTitle:CNCLString(@"comm_cancel") otherButtonTitles:CNCLString(@"comm_confirm"), nil];
        [alertview setAlertViewStyle:UIAlertViewStylePlainTextInput];
        UITextField *time_TF = [alertview textFieldAtIndex:0];
        time_TF.delegate = self;
        [alertview show];
    } else {
        [self.player closeTimeShift];
        [self.player setShouldAutoClearCache:self.setting.isAutoClearCache];
        [self.player reloadWithContentURL:nil fromStart:NO];
        self.sliderTimeShift.hidden = YES;
        self.sliderCurrentPlayTime.hidden = NO;
        [button setSelected:NO];
    }
}
- (IBAction)OnClickPlayOrPause:(id)sender {
    UIButton *button = (UIButton *)sender;
    // stop后再次点击开始按钮，重新加载视频源
    if (_isVideoOver) {
        [self.player reloadWithContentURL:nil fromStart:YES];
        _isVideoOver = NO;
        return;
    }
    
    if (!self.player.isPlaying) {
        if (self.player.isStop) {
            [self.player reloadWithContentURL:nil fromStart:YES];
        } else {
            if (_isFinishError) {
                 [self.player reloadWithContentURL:nil fromStart:NO];
                 _isFinishError = NO;
            } else {
                [self.player play];
            }
            [button setSelected:YES];
        }
    } else {
        [self.player pause];
        [button setSelected:NO];
    }
}
- (IBAction)OnClickSeekFinish:(id)sender {
    if (self.sliderTimeShift.hidden == NO) {
        [_player timeshiftWithtime:self.sliderTimeShift.value];
    } else {
        //测试seek 由于网络卡顿时所造成的回退问题
        self.player.currentPlaybackTime = self.sliderCurrentPlayTime.value;
        
    }
}
- (IBAction)OnClickSeekValueChanged:(id)sender {
    [self refreshMediaControl];
}
- (IBAction)OnclickSeekTouchDown:(id)sender {
    NSLog(@"OnclickSeekTouchDown");
    _isMediaSliderBeingDragged = YES;
}

- (IBAction)OnClickFullScreen:(id)sender {
    UIDeviceOrientation currentOrientation = [[UIDevice currentDevice] orientation];
    int type = 0;
    if (currentOrientation == (NSInteger)UIDeviceOrientationLandscapeLeft
        || currentOrientation == (NSInteger)UIDeviceOrientationLandscapeRight
        ) {
        // 全屏状态To 半
        type = 1;
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait] forKey:@"orientation"];
        [self.viewFullScreen.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj removeFromSuperview];
        }];
        [self.viewDisplayVideo addSubview:self.player.view];
        [self.viewDisplayVideo addSubview:self.infoTableViewController.tableView];
        [self.viewDisplayVideo addSubview:self.viewPlayerUI];
        self.imageViewWaiting.center = self.viewDisplayVideo.center;
        [self.viewDisplayVideo addSubview:self.imageViewWaiting];
        self.viewFullScreen.hidden = YES;
        self.viewPlayerUI.frame = self.player.view.frame = self.viewDisplayVideo.bounds;
        
        [self.buttonFullScreen setSelected:NO];

        CGRect selfFrame = self.viewDisplayVideo.frame;
        CGRect newFrame  = CGRectMake(selfFrame.size.width * 3 / 5, 0, selfFrame.size.width * 2 / 5, selfFrame.size.height);
        self.infoTableViewController.tableView.frame = newFrame;
        
        [self.viewDisplayVideo bringSubviewToFront:self.viewPlayerUI];
         [self.viewDisplayVideo bringSubviewToFront:self.infoTableViewController.tableView];
    } else {
        // 半屏状态to q全
        type = 0;
        
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeLeft] forKey:@"orientation"];
        [self.viewDisplayVideo.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj removeFromSuperview];
        }];
        [self.viewFullScreen addSubview:self.player.view];
        [self.viewFullScreen addSubview:self.viewPlayerUI];
        self.imageViewWaiting.center = self.viewFullScreen.center;
        [self.viewFullScreen addSubview:self.imageViewWaiting];
        self.viewFullScreen.hidden = NO;
        self.viewPlayerUI.frame = self.player.view.frame = self.viewFullScreen.bounds;
        [self.buttonFullScreen setSelected:YES];
        CGRect selfFrame = self.viewFullScreen.frame;
        CGRect newFrame  = CGRectMake(selfFrame.size.width * 3 / 5, 0, selfFrame.size.width * 2 / 5, selfFrame.size.height);
        self.infoTableViewController.tableView.frame = newFrame;
     
        [self.viewFullScreen bringSubviewToFront:self.viewPlayerUI];
        [self.viewFullScreen bringSubviewToFront:self.infoTableViewController.tableView];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CNCPlayer2OpenViewControllerToFullScreen" object:nil userInfo:@{@"player":self,@"type":[NSNumber numberWithInt:type]}];
    
    [self updateCurrentScaling];
    
}
- (IBAction)OnClickMuted:(id)sender {
    UIButton *button = (UIButton *)sender;
    [button setSelected:!button.selected];
    //    [CNCMediaPlayerSDK setPlyerMute:button.selected];
    [_player cncSetPlyerMute:button.selected];
}
- (IBAction)OnClickPlayBackRateChanged:(id)sender {
    if (self.setting.isLive) {
        [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"txt_live"),CNCLString(@"txt_playback_rate_not_support")]];
        return;
    }
    UIButton *button = (UIButton *)sender;
    _playBackRate += 1;
    NSInteger rate = _playBackRate % 3;
    if (rate == 0) {
        // 正常速率
        [self.player setPlayBackRate:1.0];
        [button setImage:[UIImage imageNamed:@"1倍"] forState:UIControlStateNormal];
    } else if (rate == 1) {
        // 0.5
        [self.player setPlayBackRate:0.5];
        [button setImage:[UIImage imageNamed:@"05倍"] forState:UIControlStateNormal];
    } else {
        // 2倍
        [self.player setPlayBackRate:2.0];
        [button setImage:[UIImage imageNamed:@"2倍"] forState:UIControlStateNormal];
    }
}

- (IBAction)OnClickHLSBitrateChanged:(UIButton *)sender {
    if (_HLSBitrateArray.count > 1) {
        _HLSBitrateCount += 1;
        NSInteger bitrate = _HLSBitrateCount % _HLSBitrateArray.count;
        [sender setTitle:[NSString stringWithFormat:@"%@", [_HLSBitrateArray objectAtIndex:bitrate]] forState:UIControlStateNormal];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            //            [self.player selectBitrate:543000];
            //            sleep(1);
            
            [self.player selectBitrate:[[_HLSBitrateArray objectAtIndex:bitrate] integerValue]];
        });
    }
}

- (void)refresh_media_information {
    [self.media_infomation reloadData];
}

- (void)show_information {
    self.media_infomation.hidden = NO;
    [_refresh_timer setFireDate:[NSDate date]];
}

- (void)hidden_information {
    self.media_infomation.hidden = YES;
    [_refresh_timer setFireDate:[NSDate distantFuture]];
    [self.buttonMediaInfo setSelected:NO];
}

- (void)refresh_media_dyinformation {
    [self.infoTableViewController setValue:[NSString stringWithFormat:@"%2.2f/%2.2f", self.player.fpsInOutput, self.player.avgFpsInOutput] forKey:CNCLString(@"txt_realtime_fps")];
    
    if (self.player.total_bitrate < INT_MAX) {
        [self.infoTableViewController setValue:[NSString stringWithFormat:@"%.2fkb/s", self.player.total_bitrate / 1024] forKey:CNCLString(@"txt_bitrate")];
    }
    
    [self.infoTableViewController setValue:[NSString stringWithFormat:@"%.1f", self.player.playBackRate ] forKey:@"P_R"];
}

// 播放器控制界面显示与隐藏

- (void)showOrHideCtrlView {
    for (UIView *subview in self.viewPlayerUI.subviews) {
        if ([subview isEqual:self.viewGesture]) {
            
        } else {
            if (subview.hidden == YES) {
                [self showCtrlView];
                [self performSelector:@selector(hideCtrlView) withObject:nil afterDelay:5];
            }else{
                [self hideCtrlView];
            }
            break;
        }
    }
    return;
    if (self.viewPlayerUI.hidden == YES) {
        [self showCtrlView];
        [self performSelector:@selector(hideCtrlView) withObject:nil afterDelay:5];
    } else {
        [self hideCtrlView];
    }
}

- (void)showCtrlView {
    for (UIView *subview in self.viewPlayerUI.subviews) {
        if ([subview isEqual:self.sliderTimeShift]) {
            
        } else {
            subview.hidden = NO;
        }
    }
    self.buttonHLSBitrate.hidden = YES;
    self.sliderTimeShift.hidden = !self.buttonTimeShift.selected;
    self.sliderCurrentPlayTime.hidden =  self.buttonTimeShift.selected;
    self.ivPercentSlide.hidden = (!self.setting.isOpenIV) || (![self.player cncMediaPlayerLibMngSupportIVFramework]);
    [self refreshMediaControl];
}

- (void)hideCtrlView {
    
    for (UIView *subview in self.viewPlayerUI.subviews) {
        if ([subview isEqual:self.viewGesture]) {
            
        } else {
            subview.hidden = YES;
        }
    }
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideCtrlView) object:nil];
}

- (void)changeScalingMode:(UITapGestureRecognizer *)recognizer{
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        if (self.setting.isOpenIV) {
            _currentScaling = (_currentScaling+1)%3;
            [self.player setScalingMode:_currentScaling];
        } else {
            _currentScaling = (_currentScaling+1)%5;
            [self.player setScalingMode:_currentScaling];
        }
    }
}

- (void)handleSwipe:(UIPanGestureRecognizer *)recognizer{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        _first_point = [recognizer locationInView:self.viewDisplayVideo];
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        float currentH = self.viewFullScreen.hidden?self.viewDisplayVideo.frame.size.height:self.viewFullScreen.frame.size.height;
        
        CGPoint translation_position = [recognizer translationInView:self.viewDisplayVideo];
        if (_is_first_translation) {
            if (fabs(translation_position.x) >= fabs(translation_position.y)) {
                _pan_direction = WS_GESTURE_DIRECTIOIN_HORIZONTAL;
            } else {
                _pan_direction = WS_GESTURE_DIRECTIOIN_VERTICAL;
            }
            _is_first_translation = NO;
        }
        if (_pan_direction == WS_GESTURE_DIRECTIOIN_VERTICAL) {
            if (_first_point.x >= self.viewDisplayVideo.bounds.size.width/2) {
                // right volume
#ifdef Use_Global_Volume
                float systemVolume = 0;//translation_position.y * (100 / currentH);//
                systemVolume = _sliderVolume.value;
                if (translation_position.y > _lastPosition_y) {
                    if (systemVolume > 0) {
                        if (systemVolume < 5 * CNC_VOLUME_RANGE) {
                            [_sliderVolume setValue:0 animated:YES];
                        } else {
                            [_sliderVolume setValue:(systemVolume-CNC_VOLUME_RANGE) animated:YES];
                        }
                    }
                } else if (translation_position.y < _lastPosition_y) {
                    if (systemVolume < 1) {
                        if (systemVolume > 1-CNC_VOLUME_RANGE) {
                            [_sliderVolume setValue:1 animated:YES];
                        } else {
                            [_sliderVolume setValue:(systemVolume+CNC_VOLUME_RANGE) animated:YES];
                        }
                    }
                }
#else
                float systemVolume = 0;//translation_position.y * (100 / currentH);//
                systemVolume = [_player cncPlaybackVolume];
                if (translation_position.y > _lastPosition_y) {
                    if (systemVolume > 0) {
                        if (systemVolume < 5 * CNC_VOLUME_RANGE) {
                            //                            [_sliderVolume setValue:0 animated:YES];
                            [_player cncSetPlaybackVolume:0];
                        } else {
                            //                            [_sliderVolume setValue:(systemVolume-CNC_VOLUME_RANGE) animated:YES];
                            [_player cncSetPlaybackVolume:systemVolume-CNC_VOLUME_RANGE];
                            
                        }
                    }
                } else if (translation_position.y < _lastPosition_y) {
                    if (systemVolume < 1) {
                        if (systemVolume > 1-CNC_VOLUME_RANGE) {
                            //                            [_sliderVolume setValue:1 animated:YES];
                            [_player cncSetPlaybackVolume:1];
                            
                        } else {
                            [_player cncSetPlaybackVolume:systemVolume+CNC_VOLUME_RANGE];
                            
                            //                            [_sliderVolume setValue:(systemVolume+CNC_VOLUME_RANGE) animated:YES];
                        }
                    }
                }
                systemVolume = [_player cncPlaybackVolume];
                NSString *str = [NSString stringWithFormat:@"Vol:%d%%",(int)(systemVolume * 100.f)];
                NSLog(@"toast msg: %@", str);
                
#endif
                
            } else if (_first_point.x < self.viewDisplayVideo.bounds.size.width/2) {
                // 左边
                CGFloat systemBright = [UIScreen mainScreen].brightness;
                if (translation_position.y > _lastPosition_y) {
                    // 减小亮度
                    if (systemBright > 0) {
                        if (systemBright < CNC_BRIGHTNESS_RANGE) {
                            [[UIScreen mainScreen] setBrightness:0.f];
                        }else {
                            systemBright -= CNC_BRIGHTNESS_RANGE;
                            [[UIScreen mainScreen] setBrightness:systemBright];
                        }
                    }
                } else if (translation_position.y < _lastPosition_y) {
                    // 加大亮度
                    if (systemBright < 1) {
                        if (systemBright > 1-CNC_BRIGHTNESS_RANGE) {
                            [[UIScreen mainScreen] setBrightness:1.f];
                        } else {
                            systemBright += CNC_BRIGHTNESS_RANGE;
                            [[UIScreen mainScreen] setBrightness:systemBright];
                        }
                    }
                } else {
                    
                }
                _currentBrightness = [UIScreen mainScreen].brightness;
            }
        }
        _lastPosition_x = translation_position.x;
        _lastPosition_y = translation_position.y;
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        _is_first_translation = YES;
        _lastPosition_x = 0;
        _lastPosition_y = 0;
        _isMediaSliderBeingDragged = NO;
    }
}
//

- (void)calcWaitTime {
    NSInteger now = [[CNCPlayerCommonFunc getTimeNowWithMilliSecond] integerValue];
    if (now - _startBufferTime > self.setting.minBufferTime + 4000) {
        // 进行提醒 在开启码率自适应时才提醒。
        if (self.setting.isHLSAdaptation) {
            [self showLog:CNCLString(@"txt_rate_adaptation_alert_turn_down")];
        }
        _startBufferTime = now;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            if (self.textViewLog.isHidden) {
                return self.textViewLog.text;
            }
            return CNCLString(@"txt_stream_info");
            break;
        case 1:
            return CNCLString(@"txt_video_info");
            break;
        case 2:
            return CNCLString(@"txt_audio_info");
            break;
        default:
            break;
    }
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 11;
            break;
        case 1:
            return 5;
            break;
        case 2:
            return 3;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    switch (indexPath.section) {
        case 0: {
            switch (indexPath.row) {
                case 0: {
                    cell.textLabel.text = CNCLString(@"txt_resolution");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d*%d", self.player.width, self.player.height];
                }
                    break;
                case 1: {
                    cell.textLabel.text = CNCLString(@"txt_duration");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.0fs", self.player.duration];
                }
                    break;
                case 2: {
                    cell.textLabel.text = CNCLString(@"txt_stream_fmt");
                    cell.detailTextLabel.text = self.player.videoFormat;
                }
                    break;
                case 3: {
                    cell.textLabel.text = CNCLString(@"txt_fps");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1f", self.player.fpsInMeta];
                }
                    break;
                case 4: {
                    cell.textLabel.text = CNCLString(@"txt_speed");
                    cell.detailTextLabel.text = self.player.tcpTransportSpeed;
                }
                    break;
                case 5: {
                    cell.textLabel.text = CNCLString(@"txt_decoder_type");
                    if ([self.player is_vtb_open]) {
                        cell.detailTextLabel.text = CNCLString(@"txt_decoder_hardware");
                    } else {
                        cell.detailTextLabel.text = CNCLString(@"txt_decoder_software");
                    }
                }
                    break;
                case 6: {
                    cell.textLabel.text = CNCLString(@"txt_delayed_fix");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld ms", (long)self.setting.maxCacheTime];
                }
                    break;
                case 7: {
                    cell.textLabel.text = CNCLString(@"txt_buffering_threshold");
                    switch (self.setting.bufferType) {
                        case 0:
                            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld ms", (long)self.player.minBufferTime];

                            break;
                            
                        case 1:
                        {
                            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld KB", (long)self.player.minBufferByte / 1000];

                            break;
                        }
                        default:
                            break;
                    }
                }
                    break;
                case 8: {
                    cell.textLabel.text = CNCLString(@"txt_rending_first_frame_time");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2f s", self.player.prepareDuraion/1000];
                }
                    break;
                case 9: {
                    cell.textLabel.text = CNCLString(@"txt_recording_duration");
                    if (self.player.isRecording) {
                        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d s", self.player.recordingDuration/1000];
                    }
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 1: {
            switch (indexPath.row) {
                case 0: {
                    cell.textLabel.text = CNCLString(@"txt_encoder");
                    cell.detailTextLabel.text = self.player.vcodec;
                }
                    break;
                case 1: {
                    cell.textLabel.text = CNCLString(@"txt_realtime_fps");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%2.2f(%2.2f)", self.player.fpsInOutput,self.player.avgFpsInOutput];
                }
                    break;
                case 2: {
                    cell.textLabel.text = CNCLString(@"txt_stream_cache_duration");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1f", self.player.cacheDuration/1000];
                }
                    break;
                case 3: {
                    cell.textLabel.text = CNCLString(@"txt_video_cache_duration");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1f", self.player.videoDuration/1000];
                }
                    break;
                case 4: {
                    cell.textLabel.text = CNCLString(@"txt_audio_cache_duration");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1f", self.player.audioDuration/1000];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 2: {
            switch (indexPath.row) {
                case 0: {
                    cell.textLabel.text = CNCLString(@"txt_encoder");
                    cell.detailTextLabel.text = self.player.acodec;
                }
                    break;
                case 1: {
                    cell.textLabel.text = CNCLString(@"txt_audio_sample");
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%dHz", self.player.sampleRate];
                }
                    break;
                case 2: {
                    cell.textLabel.text = CNCLString(@"txt_audio_channel");
                    cell.detailTextLabel.text = self.player.channels;
                }
                    break;
                default:
                    break;
            }
        }
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == Screen_Record_Alert_Tag) {
        if (buttonIndex == alertView.cancelButtonIndex) {
            [self openScreanRecorderOntype:CNC_MediaPlayer_Screen_Record_Type_AVS button:self.buttonRecordScreen];
        } else {
            switch (buttonIndex) {
                case 1:{
                    [self openScreanRecorderOntype:CNC_MediaPlayer_Screen_Record_Type_AVS button:self.buttonRecordScreen toRecordView:self.viewDisplayVideo];
                    
                }
                    break;
                case 2:{
                    [self openScreanRecorderOntype:CNC_MediaPlayer_Screen_Record_Type_AVS button:self.buttonRecordScreen toRecordView:self.textViewLog];
                    
                }
                    break;
                default:
                    break;
            }
        }
    } else {
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            UITextField *time_TF = [alertView textFieldAtIndex:0];
            if (![time_TF.text isEqualToString:@""]) {
                _timeShiftDuration = [time_TF.text integerValue];
                [self.player openTimeshiftWithKey:nil andMaxTime:_timeShiftDuration];
                [self.player setShouldAutoClearCache:NO];
                self.sliderCurrentPlayTime.hidden = YES;
                self.sliderTimeShift.hidden = NO;
                self.sliderTimeShift.maximumValue = _timeShiftDuration;
                self.sliderTimeShift.value = self.sliderTimeShift.maximumValue;
                [self.buttonTimeShift setSelected:YES];
            }
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([result length] == 0) return YES; // Allow delete all character which are entered.
    NSString *regex = @"^[0-9]*[1-9][0-9]*$";
    NSPredicate *prd = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    NSInteger integer = [result integerValue];
    NSLog(@"result = %@ int = %ld",result,(long)integer);
    if (integer > 1800 || integer < 0) {
        NSLog([NSString stringWithFormat:@"%@ 0~1800s",CNCLString(@"txt_timestamp_offset_range")]);
        return NO;
    }
    return [prd evaluateWithObject:result];
}



- (void)questionViewFinishSelectWithResult:(BOOL)result {
    if (result) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:CNCLString(@"txt_answer_correct") delegate:self cancelButtonTitle:CNCLString(@"comm_confirm") otherButtonTitles:nil, nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:CNCLString(@"txt_answer_wrong") delegate:self cancelButtonTitle:CNCLString(@"comm_confirm") otherButtonTitles:nil, nil];
        [alert show];
    }
    _isQuestion = NO;
}

- (void)questionviewTimeOut {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:CNCLString(@"txt_time_out") delegate:self cancelButtonTitle:CNCLString(@"comm_confirm") otherButtonTitles:nil, nil];
    [alert show];
    _isQuestion = NO;
}

#pragma mark -- 通知回调
- (void)cncPlayerDidPrepare {
    [self showLog:CNCLString(@"txt_prepare_done")];
    [self refreshMediaControl];
    // 开启实时更新信息面板
    [self.infoTableViewController setValue:[NSString stringWithFormat:@"%d*%d", self.player.width, self.player.height] forKey:CNCLString(@"txt_resolution")];
    _lastErrorCode = @"";
    if (self.setting.isOpenIV) {
        if (self.segIVDisplayModel.numberOfSegments > 1) {
            self.segIVDisplayModel.selectedSegmentIndex = 1;//默认开始 lrsr
            [self actionIVShowModelValueChanged:self.segIVDisplayModel];
        }
    }
}

- (void)cncPlayerPlayStateChanged:(NSNotification *)notification {
    NSString *playState = (NSString *)[[notification userInfo] objectForKey:CNCMediaPlayerPlayStateDidChangeUserInfoKey];
    switch ([playState integerValue]) {
        case CNC_PLAYER_STATE_ON_MEDIA_START: {
            [self showLog:CNCLString(@"txt_playing_start")];
            [self.buttonPlayOrPause setSelected:YES];
            _isMediaSliderBeingDragged = NO;
            //            NSLog(@"filesize = %f",[self.player bitrate] / 8 * [self.player duration]);
            
        }
            break;
        case CNC_PLAYER_STATE_ON_MEDIA_PAUSE:
            [self showLog:CNCLString(@"txt_playing_pause")];
            [self.buttonPlayOrPause setSelected:NO];
            break;
        case CNC_PLAYER_STATE_ON_MEDIA_STOP:
            [self showLog:CNCLString(@"txt_playing_stop")];
            [self.buttonPlayOrPause setSelected:NO];
            break;
        case CNC_PLAYER_STATE_ON_MEDIA_SEEKING_FORWARD:
            [self showLog:@"Seeking..."];
            break;
        default:
            break;
    }
}

- (void)cncPlayerloadStateChanged:(NSNotification *)notification {
    NSString *playState = (NSString *)[[notification userInfo] objectForKey:CNCMediaPlayerLoadStateDidChangeUserInfoKey];
    switch ([playState integerValue]) {
        case CNC_MEDIA_LOAD_STATE_STALLED:
            [self showLog:[NSString stringWithFormat:@"%@...",CNCLString(@"txt_buffering")]];
            self.imageViewWaiting.hidden = NO;
            _startBufferTime = [[CNCPlayerCommonFunc getTimeNowWithMilliSecond] integerValue];
            // 开启计时器？
            if (_alertChangeBitrateTimer == nil && [self.URL.absoluteString containsString:@"m3u8"]) {
                _alertChangeBitrateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(calcWaitTime) userInfo:nil repeats:YES];
            }
            break;
        case CNC_MEDIA_LOAD_STATE_PLAYABLE:
            [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"txt_buffering_end"), CNCLString(@"txt_can_play")]];
            self.imageViewWaiting.hidden = YES;
            _startBufferTime = 0;
            if (_alertChangeBitrateTimer != nil) {
                [_alertChangeBitrateTimer invalidate];
                _alertChangeBitrateTimer = nil;
            }
            break;
        case CNC_MEDIA_LOAD_STATE_PLAYTHROUGHOK:
            [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"txt_buffering_end"),CNCLString(@"txt_playing")]];
            self.imageViewWaiting.hidden = YES;
            _startBufferTime = 0;
            if (_alertChangeBitrateTimer != nil) {
                [_alertChangeBitrateTimer invalidate];
                _alertChangeBitrateTimer = nil;
            }
            break;
        case CNC_MEDIA_LOAD_STATE_UNKNOWN:
            [self showLog:CNCLString(@"txt_buffering_err")];
            self.imageViewWaiting.hidden = YES;
            break;
        default:
            break;
    }
}

- (void)cncPlayerDidFinish:(NSNotification *)notification {
    NSString *playState = (NSString *)[[notification userInfo] objectForKey:CNCMediaPlayerPlayDidFinishUserInfoKey];
    NSString *value = [NSString stringWithFormat:@"%@",[[notification userInfo] objectForKey:@"error"]];
    switch ([playState integerValue]) {
        case CNC_MEDIA_PLAYER_DID_FINISH_END: {
            [self showLog:CNCLString(@"txt_play_end")];
            [self cncPlayerRtmpStopReloadOnce];
        }
            break;
        case CNC_MEDIA_PLAYER_DID_FINISH_ERROR: {
            if (![_lastErrorCode isEqualToString:value]) {
                [self showLog:[NSString stringWithFormat:@"%@(%@)", CNCLString(@"txt_reloading"),value]];
                [self.player reloadWithContentURL:nil fromStart:NO];
                [self showLog:[NSString stringWithFormat:@"%@,%@",CNCLString(@"txt_network_none"),CNCLString(@"txt_reload_fail")]];
                _lastErrorCode = value;
            }
             _isFinishError = YES;
        }
            break;
        default:
            break;
    }
}

- (void)cncPlayerRtmpStopReloadOnce {
    if ([[self.URL absoluteString] hasPrefix:@"rtmp://"]) {
        NSLog(@"RTMP stop try reload once");
        [self.player reloadWithContentURL:nil fromStart:NO];
    }
}

- (void)cncPlayerScalingModeChanged {
    NSLog(@"cncPlayerScalingModeChanged");
    [self showLog:CNCLString(@"txt_scale_mode_changed")];
}

- (void)cncPlayerResolutionChanged:(NSNotification *) notification {
    
    NSLog(@"Receive notification:video resolution changed,detail : %@",[notification userInfo]);
    int key_value = [[[notification userInfo] valueForKey:@"IJKMPMoviePlayerVideoSizeChangedWidthKey"] intValue];
    int key_value2 = [[[notification userInfo] valueForKey:@"IJKMPMoviePlayerVideoSizeChangedHeightKey"] intValue];
    
    [self showLog:[NSString stringWithFormat:@"%@ to %d x %d",CNCLString(@"txt_resolution_changed"),key_value,key_value2]];
}

- (void)cncPlayerFirstVideoRender {
    [self showLog:CNCLString(@"txt_first_video_frame_render")];
}

- (void)cncPlayerFirstAudioRender {
    [self showLog:CNCLString(@"txt_first_audio_frame_render")];
}

- (void)cncPlayerSeekDidFinish:(NSNotification *)notification {
    if ([self.player isEqual:[notification object]]) {
        NSLog(@"cncPlayerSeekDidFinish:self.player isEqual:%@",[self.player description]);
        [self showLog:CNCLString(@"txt_seek_done")];
        
    } else {
        NSLog(@"cncPlayerSeekDidFinish:other player notify:%@",[self.player description]);
    }
}


- (void)cncPlayerStatus:(NSNotification *)notification {
    NSString *key_value = [NSString stringWithFormat:@"%@", [[notification userInfo] objectForKey:CNCMediaPlayerStatusKey]];
    NSInteger detail_value = [[NSString stringWithFormat:@"%@", [[notification userInfo] objectForKey:CNCMediaPlayerDetailCode]] integerValue];
    
    CNC_MediaPlayer_ret_Code int_key_value = [[[notification userInfo] valueForKey:CNCMediaPlayerStatusKey] integerValue];
    NSString *msg;
    
    switch (int_key_value) {
        case CNC_MediaPlayer_RCode_Proxy_Port_Connect_Failed:
        {
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@ %@",(unsigned long)detail_value,key_value,CNCLString(@"txt_proxy_open"),CNCLString(@"comm_fail"),@"(SOCKS4/SOCKS5/HTTP/HTTPS PROXY)"];
            [self showLog:msg];
            _isVideoOver = YES;
        }
            break;
        case CNC_MediaPlayer_RCode_Player_Initialization_Failed:
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@ %@",(unsigned long)detail_value,key_value,CNCLString(@"comm_player"),CNCLString(@"comm_initialization"),CNCLString(@"comm_fail")];
            [self showLog:msg];
            _isVideoOver = YES;
            break;
        case CNC_MediaPlayer_RCode_Player_Request_Success:
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@",(unsigned long)detail_value,key_value,CNCLString(@"comm_request"),CNCLString(@"comm_success")];
            [self showLog:msg];
            break;
        case CNC_MediaPlayer_RCode_Decode_HW_Decoder_Initialization_Failed:
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@ %@ %@",(unsigned long)detail_value,key_value,CNCLString(@"txt_decoder_hardware"),CNCLString(@"comm_decoder"),CNCLString(@"comm_initialization"),CNCLString(@"comm_fail")];
            [self showLog:msg];
            
            break;
        case CNC_MediaPlayer_RCode_Decode_SW_Decoder_Initialization_Failed:
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@ %@ %@",(unsigned long)detail_value,key_value,CNCLString(@"txt_decoder_software"),CNCLString(@"comm_decoder"),CNCLString(@"comm_initialization"),CNCLString(@"comm_fail")];
            [self showLog:msg];
            
            break;
        case CNC_MediaPlayer_RCode_Decode_Video_Decoding_Failed:
            //            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)detail_value,key_value,@"当前帧视频解码失败"];
            
            break;
        case CNC_MediaPlayer_RCode_Decode_Audio_Decoding_Failed:
            //            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)detail_value,key_value,@"当前帧音频解码失败"];
            
            break;
        case CNC_MediaPlayer_RCode_Player_Unresolved_URL: {
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@ %@",(unsigned long)detail_value,key_value,CNCLString(@"comm_can_not"),CNCLString(@"comm_parse"),@"URL"];
            [self showLog:msg];
            //            [_player_ui set_buffer_image_hidden:YES];
            _isVideoOver = YES;
        }
            break;
        case CNC_MediaPlayer_RCode_Parse_URL_Failed: {
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@ %@",(unsigned long)detail_value,key_value,CNCLString(@"comm_parse"),@"URL",CNCLString(@"comm_fail")];
            [self showLog:msg];
            //            [_player_ui set_buffer_image_hidden:YES];
            _isVideoOver = YES;
        }
            break;
        case CNC_MediaPlayer_RCode_Player_Connect_Server_Failed: {
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@",(unsigned long)detail_value,key_value,CNCLString(@"txt_connect_server"),CNCLString(@"comm_fail")];
            [self showLog:msg];
            //            [_player_ui set_buffer_image_hidden:YES];
            _isVideoOver = YES;
        }
            break;
        case CNC_MediaPlayer_RCode_Player_SW_Decoder_Switched_on: {
            msg = [NSString stringWithFormat:@"code:%lu(%@)--%@ %@ %@ %@ %@",(unsigned long)detail_value,key_value,CNCLString(@"txt_decoder_hardware"),CNCLString(@"comm_initialization"),CNCLString(@"comm_fail"),CNCLString(@"txt_switch_to"),CNCLString(@"txt_decoder_software")];
            [self showLog:msg];
        }
            break;
        default:
            break;
    }
}

- (void)cncPlayerRecordStatus:(NSNotification *)notification {
    NSLog(@"cncPlayerRecordStatus");
    NSString *key_value = [NSString stringWithFormat:@"%@", [[notification userInfo] objectForKey:CNCMediaPlayerRecordingStatusKey]];
    CNC_MediaPlayer_ret_Code int_key_value = [[[notification userInfo] valueForKey:CNCMediaPlayerRecordingStatusKey] integerValue];
    NSString *msg;
    //    NSString *info = [[notification userInfo] valueForKey:CNCMediaPlayerRecordInfo];
    NSInteger detail_value = [[NSString stringWithFormat:@"%@", [[notification userInfo] objectForKey:CNCMediaPlayerDetailCode]] integerValue];
    if (detail_value == 0) {
        detail_value = int_key_value;
    }
    
    switch (int_key_value) {
        case CNC_MediaPlayer_Rcode_Record_Complete: {
            if (self.player.isRecording) {
                //录制异步结束
                NSString *filePath = [[notification userInfo] valueForKey:CNCMediaPlayerRecordFileName];
                
                msg = [NSString stringWithFormat:@"[%@] code:%lu(%@)--%@",CNCLString(@"comm_rec"),(unsigned long)detail_value,key_value,[NSString stringWithFormat:@"%@，file path：%@", CNCLString(@"txt_rec_done"),[[filePath componentsSeparatedByString:@"/"] lastObject]]];
                [self showLog:msg];
                if ([filePath containsString:@"mp4"]) {
                    [self startUpdateRecrodingTime:NO type:0 button:self.buttonRecordVideo];
                } else {
                    [self startUpdateRecrodingTime:NO type:0 button:self.buttonRecordGIF];
                }
            } else {
                NSString *duration = [[notification userInfo] valueForKey:CNCMediaPlayerRecordDuration];
                NSString *filePath = [[notification userInfo] valueForKey:CNCMediaPlayerRecordFileName];
                
                [self showLog:[NSString stringWithFormat:@"[%@],code:%lu--duration = %@--fila path = %@",CNCLString(@"txt_screen_rec_done"),(unsigned long)detail_value,duration, [[filePath componentsSeparatedByString:@"/"] lastObject]]];
                [self startUpdateRecrodingTime:NO type:1 button:self.buttonRecordScreen];
            }
        }
            break;
        case CNC_MediaPlayer_Rcode_Not_Enough_Freedisk: {
            //走这边只存在录制超过最低时间要求的情况
            
            msg = [NSString stringWithFormat:@"[%@] code:%lu(%@)--%@",CNCLString(@"comm_rec"),(unsigned long)detail_value,key_value,CNCLString(@"txt_storage_not_enough")];
            [self showLog:msg];
        }
            break;
        case CNC_MediaPlayer_Rcode_Record_Succeed: {
            
            int record_duration = [[NSString stringWithFormat:@"%@", [[notification userInfo] objectForKey:CNCMediaPlayerRecordDuration]] intValue];
            NSString *record_info = _isBackgroundStopRecord?@"suspend":[[notification userInfo] valueForKey:CNCMediaPlayerRecordInfo];
            NSString *filePath = [[notification userInfo] valueForKey:CNCMediaPlayerRecordFileName];
            msg = [NSString stringWithFormat:@"[%@] code:%lu(%@)--%@",CNCLString(@"comm_rec"),(unsigned long)detail_value,key_value,[NSString stringWithFormat:@"%@，info：%@ file path:%@(duration:%d ms)",CNCLString(@"txt_rec_suc"), record_info,[[filePath componentsSeparatedByString:@"/"] lastObject],record_duration]];
            [self showLog:msg];
            if ([filePath containsString:@".mp4"]) {
                [self saveVideoToPhtotlibrary:filePath];
                [self.buttonRecordVideo setSelected:NO];
            } else {
                [self.buttonRecordGIF setSelected:NO];
            }
        }
            break;
        case CNC_MediaPlayer_Rcode_Record_Failed: {
            int record_duration = [[NSString stringWithFormat:@"%@", [[notification userInfo] objectForKey:CNCMediaPlayerRecordDuration]] intValue];
            
            NSString *filePath = [[notification userInfo] valueForKey:CNCMediaPlayerRecordFileName];
            NSString *record_info = [[notification userInfo] valueForKey:CNCMediaPlayerRecordInfo];
            msg = [NSString stringWithFormat:@"[%@] code:%lu(%@)--%@",CNCLString(@"comm_rec"),(unsigned long)detail_value,key_value,[NSString stringWithFormat:@"%@，info：%@ file path:%@(duration:%d ms)", CNCLString(@"txt_rec_fail"),record_info,[[filePath componentsSeparatedByString:@"/"] lastObject],record_duration]];
            [self showLog:msg];
            if ([filePath containsString:@".mp4"]) {
                [self.buttonRecordVideo setSelected:NO];
            } else {
                [self.buttonRecordGIF setSelected:NO];
            }
            
            if ([filePath containsString:@"mp4"]) {
                [self startUpdateRecrodingTime:NO type:0 button:self.buttonRecordVideo];
            } else {
                [self startUpdateRecrodingTime:NO type:0 button:self.buttonRecordGIF];
            }
        }
            break;
        case CNC_MediaPlayer_Rcode_Invalid_Storage_Path: {
            msg = [NSString stringWithFormat:@"[%@] code:%lu(%@)--%@",CNCLString(@"comm_rec"),(unsigned long)detail_value,key_value,CNCLString(@"txt_path_err_encoder_err")];
            [self showLog:msg];
        }
            break;
            
            
        default:{
            msg = [NSString stringWithFormat:@"[%@] code:%lu(%@)--%@",CNCLString(@"comm_rec"),(unsigned long)detail_value,key_value,CNCLString(@"txt_unknow_exception")];
            [self showLog:msg];
        }
            break;
    }
}

- (void)cncPlayerHTTPRcode:(NSNotification *)notification {
    NSString *key_value = [NSString stringWithFormat:@"%@", [[notification userInfo] objectForKey:CNCMediaPlayerHTTPRcodeKey]];
    NSString *msg;
    
    if ([key_value isEqualToString:@"400"]) {
        msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)0,key_value,@"HTTP_BAD_REQUEST 400"];
    } else if ([key_value isEqualToString:@"401"]) {
        msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)0,key_value,@"HTTP_UNAUTHORIZED 401"];
    } else if ([key_value isEqualToString:@"403"]) {
        msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)0,key_value,@"HTTP_FORBIDDEN 403"];
    } else if ([key_value isEqualToString:@"404"]) {
        msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)0,key_value,@"HTTP_NOT_FOUND 404"];
    } else if ([key_value isEqualToString:@"4xx"]) {
        msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)0,key_value,@"HTTP_OTHER_4XX 4XX"];
    } else if ([key_value isEqualToString:@"5xx"]) {
        msg = [NSString stringWithFormat:@"code:%lu(%@)--%@",(unsigned long)0,key_value,@"HTTP_SERVER_ERROR 5XX"];
    } else {
        return;
    }
    [self showLog:msg];
}

// 若是shutdown，则接受不到该通知
- (void)cncPlayerDidShutdown {
    [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"comm_player"),CNCLString(@"txt_release_done")]];
}

// 获取sei
- (void)cncPlayerGetSEI:(NSNotification *)notification {
    if (_isQuestion) {
        return;
    }
    NSString *seiString = [[notification userInfo] valueForKey:CNCMediaPlayerWSCustomSEI];
    // 防止答题过快，导致出现重复题目
    if ([_lastQuestionStr isEqualToString:seiString]) {
        return;
    }
    _lastQuestionStr = seiString;
    [self showLog:seiString];
    if (![seiString containsString:@"content"]) {
        return;
    }
    

    
    // 解析最外层json
    NSData *seiStringData = [seiString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *seiStringDataDic = [NSJSONSerialization JSONObjectWithData:seiStringData options:NSJSONReadingMutableLeaves error:nil];
    
    if (seiStringDataDic == nil) {
        return;
    }
    
    NSInteger time = [[seiStringDataDic valueForKey:CNC_TIME] integerValue];
    NSInteger type = [[seiStringDataDic valueForKey:CNC_TYPE] integerValue];
    NSString *strContent = [seiStringDataDic valueForKey:CNC_CONTENT];
    
    // 解析content
    NSData *strContentData = [strContent dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *strContentDataDic = [NSJSONSerialization JSONObjectWithData:strContentData options:NSJSONReadingMutableLeaves error:nil];
    
    if (strContentDataDic == nil) {
        return;
    }
    
    NSInteger userType = [[strContentDataDic valueForKey:CNC_USERTYPE] integerValue];
    NSArray *questionList = [strContentDataDic valueForKey:CNC_QUESTIONLIST];
    if (questionList == nil) {
        return;
    }
    
    if ([questionList respondsToSelector:@selector(enumerateObjectsUsingBlock:)]) {
        [questionList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            // 解析题目
            NSDictionary *strQuestionDataDic = (NSDictionary *)obj;
            if (strQuestionDataDic == nil) {
                return;
            }
            NSString *strQuestion = [strQuestionDataDic valueForKey:CNC_QUESTION];
            NSArray *arrayStrAnswer = [strQuestionDataDic valueForKey:CNC_ANSWER];
            NSArray *strCorrect = [strQuestionDataDic valueForKey:CNC_CORRECT];
            NSInteger questionType = [[strQuestionDataDic valueForKey:CNC_QUESTION_TYPE] integerValue];
            
          
            _isQuestion = YES;
        }];
    }
}

// 获取当前hls播放码率
- (void)HLSBitrate:(NSNotification *)notification {
    int64_t firstHLSBitrate = [[[notification userInfo] valueForKey:CNCMediaPlayerFirstHLSBitrate] integerValue];
    int64_t currentHLSBitrate = [[[notification userInfo] valueForKey:CNCMediaPlayerCurrentHLSBitrate] integerValue];
    NSArray *array = [[notification userInfo] valueForKey:CNCMediaPlayerHLSBitrateList];
    int64_t changeBitrate = [[[notification userInfo] valueForKey:CNCMediaPlayerHLSChangeBitrate] integerValue];
    
    if (firstHLSBitrate > 0) {
        // 首次播放码率
        [self showLog:[NSString stringWithFormat:@"[%@]%@：%lld",CNCLString(@"txt_hls_bitrate_adaptation_play"), CNCLString(@"txt_first_bitrate"),firstHLSBitrate]];
        [_buttonHLSBitrate setTitle:[NSString stringWithFormat:@"%lld", firstHLSBitrate] forState:UIControlStateNormal];
        if (_HLSBitrateArray != nil) {
            [_HLSBitrateArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj integerValue] == firstHLSBitrate) {
                    _HLSBitrateCount = idx;
                }
            }];
        }
    }
    
    if (currentHLSBitrate > 0) {
        // 当前播放码率
        [self showLog:[NSString stringWithFormat:@"[%@]%@：%lld",CNCLString(@"txt_hls_bitrate_adaptation_play"),CNCLString(@"txt_switching_bitrate"), currentHLSBitrate]];
        [_buttonHLSBitrate setTitle:[NSString stringWithFormat:@"%lld", currentHLSBitrate] forState:UIControlStateNormal];
        if (_HLSBitrateArray != nil) {
            [_HLSBitrateArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([obj integerValue] == currentHLSBitrate) {
                    _HLSBitrateCount = idx;
                }
            }];
        }
    }
    
    if (array != nil) {
        // 多码率列表
        _HLSBitrateArray = array;
        if (self.setting.HLSDefaultBitrate == 0) {
            self.setting.HLSDefaultBitrate = [[array objectAtIndex:0] integerValue];
        }
        [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj integerValue] == self.setting.HLSDefaultBitrate) {
                _HLSBitrateCount = idx;
            }
        }];
    }
    
    if (changeBitrate) {
        [self showLog:[NSString stringWithFormat:@"[%@]%@ %@",CNCLString(@"txt_hls_bitrate_adaptation_play"),CNCLString(@"txt_bitrate_switch"),CNCLString(@"comm_success")]];
    }
}

- (void)playerFindNewStream:(NSNotification *) noti {
    NSLog(@"demo playerFindNewStream");
    [self.player reloadWithContentURL:nil fromStart:YES];
    
}


- (void)applicationWillResignActive {
    //停止录制
    if (self.player.isRecording) {
        _isBackgroundStopRecord = YES;
        if (self.buttonRecordVideo.selected) {
            [self.buttonRecordVideo sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
        if (self.buttonRecordGIF.selected) {
            [self.buttonRecordGIF sendActionsForControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    // 停止录屏
    if (_isScreenRecordStart) {
        [self.buttonRecordScreenView sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
    
    // 恢复亮度
    if (_originBrightness >= 0) {
        NSLog(@"2 SET BRI = %f",_originBrightness);
        [[UIScreen mainScreen] setBrightness:_originBrightness];
    }
}

- (void)applicationDidBecomeActive {
    if (_currentBrightness < 0) {
        return;
    }
    if (_currentBrightness >= 0) {
        NSLog(@"3 SET BRI = %f c %f",_originBrightness,_currentBrightness);
        [[UIScreen mainScreen] setBrightness:_currentBrightness];
    }
}

- (void)orientationDidChanged:(NSNotification *)notification {
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    switch (deviceOrientation) {
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            
            [self.viewDisplayVideo.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [obj removeFromSuperview];
            }];
            [self.viewFullScreen addSubview:self.player.view];
            [self.viewFullScreen addSubview:self.infoTableViewController.tableView];
            [self.viewFullScreen addSubview:self.viewPlayerUI];
            self.imageViewWaiting.center = self.viewFullScreen.center;
            [self.viewFullScreen addSubview:self.imageViewWaiting];
            self.viewFullScreen.hidden = NO;
            self.viewPlayerUI.frame = self.player.view.frame = self.viewFullScreen.bounds;
            [self.buttonFullScreen setSelected:YES];
            CGRect selfFrame = self.viewFullScreen.frame;
            CGRect newFrame  = CGRectMake(selfFrame.size.width * 2 / 5, 0, selfFrame.size.width * 2 / 5, selfFrame.size.height);
            self.infoTableViewController.tableView.frame = newFrame;
            [self.viewFullScreen bringSubviewToFront:self.viewPlayerUI];
             [self.viewFullScreen bringSubviewToFront:self.infoTableViewController.tableView];
            break;
        case UIDeviceOrientationPortrait:
            [self.viewFullScreen.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [obj removeFromSuperview];
            }];
            [self.viewDisplayVideo addSubview:self.player.view];
        
            [self.viewDisplayVideo addSubview:self.viewPlayerUI];
            self.imageViewWaiting.center = self.viewDisplayVideo.center;
            [self.viewDisplayVideo addSubview:self.imageViewWaiting];
            self.viewFullScreen.hidden = YES;
            self.viewPlayerUI.frame = self.player.view.frame = self.viewDisplayVideo.bounds;
            [self.buttonFullScreen setSelected:NO];
            selfFrame = self.viewDisplayVideo.frame;
            newFrame  = CGRectMake(selfFrame.size.width * 3 / 5, 0, selfFrame.size.width * 2 / 5, selfFrame.size.height);
            self.infoTableViewController.tableView.frame = newFrame;
        
            [self.viewDisplayVideo bringSubviewToFront:self.viewPlayerUI];
            [self.viewDisplayVideo bringSubviewToFront:self.infoTableViewController.tableView];
            break;
        default:
            break;
    }
    
    [self updateCurrentScaling];
    
}
- (void)updateCurrentScaling {
    
    if (self.setting.isOpenIV) {
        if (!self.player.isPlaying) {
            dispatch_async(dispatch_get_global_queue(0,0), ^{
                usleep(30*1000);
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"setScalingMode = %ld",(long)self->_currentScaling);
                    [self.player setScalingMode:self->_currentScaling];
                });
            });
        }
    }
}

- (void)updatePlayBackRate {
    float rate = self.player.playBackRate;
    if (rate == 1.0f) {
        // 正常速率
        _playBackRate = 0;
        [self.btnPlayBackRate setImage:[UIImage imageNamed:@"1倍"] forState:UIControlStateNormal];
    } else if (rate == 0.5) {
        // 0.5
        _playBackRate = 1;
        [self.btnPlayBackRate setImage:[UIImage imageNamed:@"05倍"] forState:UIControlStateNormal];
    } else if (rate == 2.0){
        // 2倍
        _playBackRate = 2;
        [self.btnPlayBackRate setImage:[UIImage imageNamed:@"2倍"] forState:UIControlStateNormal];
    }
    
}

#pragma mark -IV
- (void)setModelName:(NSString *) mName {
    NSError *err;
    [self.player cncMediaPlayerLibMngLoadIVModelName:mName error:&err];
    
}
- (IBAction)actionIVModelBtn:(id)sender {
    NSArray *ivModelNames = [self.player cncMediaPlayerLibMngGetSupportIVModels];
    if ([ivModelNames count] > 0) {
        UIButton *btn =(UIButton *) sender;
        NSString *currentBtnTitle = [btn titleForState:UIControlStateNormal];
        int noFountCount = 0;
        for (int i = 0; i < [ivModelNames count];i++) {
            NSString *model = [ivModelNames objectAtIndex:i];
            if ([currentBtnTitle isEqualToString:model]) {
                int new_index = (i + 1) % [ivModelNames count];
                NSString *mName = [ivModelNames objectAtIndex:new_index];
                [btn setTitle:mName forState:UIControlStateNormal];
                [self setModelName:mName];
                
                break;
            } else {
                noFountCount ++;
            }
        }
        if (noFountCount == [ivModelNames count]) {
            NSString *mName = [ivModelNames objectAtIndex:0];
            [btn setTitle:mName forState:UIControlStateNormal];
            [self setModelName:mName];
            
        }
        
    }
}
- (IBAction)actionIVSlideValueChanged:(id)sender {
    UISlider *slider = sender;
    [_player cncMediaPlayerLibMngSetIVShowPercent:slider.value];
    
}

- (IBAction)actionIVShowModelValueChanged:(id)sender {
    UISegmentedControl *segment = sender;
    if (self.player) {
        self.ivPercentSlide.value = 0.5;
        [self.player cncMediaPlayerLibMngSetDisplayMode:segment.selectedSegmentIndex];
    }
}

-(void)ivInfoUpdate {
    __weak CNCPlayerViewController *weak_Self = self;
    [self.player cncMediaPlayerLibMngSetExtraBlock:^(float detect_fps, double detect_value, int output_w, int output_h) {
      NSLog(@"IV(fps):%@", [NSString stringWithFormat:@"%.2f",detect_fps]);
      NSLog(@"IV(time):%@", [NSString stringWithFormat:@"%.2fms",detect_value]);
      NSLog(@"IV(Res):%@", [NSString stringWithFormat:@"%dx%d",output_w,output_h]);
    }];
}

-(void) initIV_View:(BOOL) isOpenIV{
    NSLog(@"initIV_View :%d",isOpenIV);
    self.btnIVModel.hidden = !isOpenIV;
    self.segIVDisplayModel.hidden = !isOpenIV;
    self.ivPercentSlide.hidden = !isOpenIV;
    if (!isOpenIV) {
        self.ivTvTopConstraint.constant = - 35;
    }
}

#pragma mark - external

+ (BOOL)isPhoneX {
    BOOL iPhoneX = NO;
    if (UIDevice.currentDevice.userInterfaceIdiom != UIUserInterfaceIdiomPhone) {//判断是否是手机
        return iPhoneX;
    }
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.bottom > 0.0) {
            iPhoneX = YES;
        }
    }
    return iPhoneX;
}


- (void)adpaterFor2OpenIn1 {
    self.urlHeightConstraint.constant = 21;
    self.urlTopConstraint.constant = 1;
    self.textViewLog.hidden = YES;
    recordTypeChoose = YES;
    adpatorForMutliOpen = YES;
}


- (void)dealloc {
    logMessageCache = nil;
    NSLog(@"CNCPlayerViewController dealloc");
}

- (void)setupMultiPlayer {
    NSInteger width = [UIScreen mainScreen].bounds.size.width;
    NSInteger height = [UIScreen mainScreen].bounds.size.height;
    
    UIView *backgroundView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, width / 16 * 9 + 100, width, height - width / 16 * 9 + 100)];
        view.backgroundColor = [UIColor whiteColor];
        
        view;
    });
    
    [self.view addSubview:backgroundView];
    
    self.player1 = [self playerWithFrame:CGRectMake(0, 0, width / 2, width / 2 /16 * 9)];
    [backgroundView addSubview:self.player1.view];
    self.player2 = [self playerWithFrame:CGRectMake(width / 2, 0, width / 2, width / 2 /16 * 9)];
    [backgroundView addSubview:self.player2.view];
    self.player3 = [self playerWithFrame:CGRectMake(0, width / 2 /16 * 9, width / 2, width / 2 /16 * 9)];
    [backgroundView addSubview:self.player3.view];
    self.player4 = [self playerWithFrame:CGRectMake(width / 2, width / 2 /16 * 9, width / 2, width / 2 /16 * 9)];
    [backgroundView addSubview:self.player4.view];
}


// 初始化播放器 设置参数
-(CNCMediaPlayerController*)playerWithFrame:(CGRect) rect {
    NSMutableDictionary *option = [[NSMutableDictionary alloc] init];
    //点播缓存如果开启时，并且需要在直播模式下自动关闭，需要设置此参数
    if (self.setting.isLive) {
        [option setObject:@"1" forKey:CNCMediaPlayerOptionIsLive];
    } else {
        [option setObject:@"0" forKey:CNCMediaPlayerOptionIsLive];
    }
    //IV 包名绑定 兼容非注册 live 包名失效
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *bundleID = [bundle objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    if (self.setting.isOpenIV) {
        
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:bundlePath];
        NSString *versionLiveIVKey = [dict objectForKey:@"versionLiveIVKey"];
        NSString *versionFullIVKey = [dict objectForKey:@"versionFullIVKey"];
        
        
        if ([bundleID containsString:@"Live"]) {
            if (versionLiveIVKey) {
                [option setObject:versionLiveIVKey forKey:CNCMediaPlayerOptionIVDevKey];
            }
        } else {
            if (versionFullIVKey) {
                [option setObject:versionFullIVKey forKey:CNCMediaPlayerOptionIVDevKey];
            }
        }
    }
    
    BOOL mainPlayer;
    if (self.setting.isMutliOpen != -1) {
        [option setObject:@"1" forKey:CNCMediaPlayerOptionIsMainPlayer];
        mainPlayer = YES;
    } else {
        [option setObject:@"0" forKey:CNCMediaPlayerOptionIsMainPlayer];
        //超分模式暂时未支持多开，非主播放器暂时停用超分模式
        mainPlayer = NO;
    }
    
    if (self.setting.isOpenIV) {
        [option setObject:@"1" forKey:CNCMediaPlayerOptionIsOpenIV];
    } else {
        [option setObject:@"0" forKey:CNCMediaPlayerOptionIsOpenIV];
    }
    
    CNCMediaPlayerController *player = [[CNCMediaPlayerController alloc] initWithContentURL:self.URL option:option];
    player.view.frame = rect;
    NSLog(@"SDK support iv = %d",[player cncMediaPlayerLibMngSupportIVFramework]);
    if (self.setting.isOpenIV && [player cncMediaPlayerLibMngSupportIVFramework]) {
        //第三方支持
        [self initIV_View:YES];
    } else {
        [self initIV_View:NO];
        if (self.setting.isOpenIV) {
            [self showLog:[NSString stringWithFormat:@"%@ %@ %@",CNCLString(@"txt_resolution_reestablish"),CNCLString(@"comm_open"),CNCLString(@"comm_fail")]];
        }
    }
    [self showLog:[NSString stringWithFormat:@"%@ %@",CNCLString(@"comm_create"),CNCLString(@"comm_player")]];
    
    if (player) {
        if (self.setting.isOpenIV) {
            [self ivInfoUpdate];
        }
#if DEBUG
        //TODO:debug 模式下可选debug，若日志太多干扰，可选warn
        [player setLogLevel:CNC_MediaPlayer_Loglevel_Warn];
#else
        [player setLogLevel:CNC_MediaPlayer_Loglevel_SILENT];
        
#endif
        //设置直播或者点播
        [player setPlayMode:self.setting.isLive ? CNC_MEDIA_PLAYER_MODE_LIVE : CNC_MEDIA_PLAYER_MODE_VOD];
        
        //设置是否自动播放
        [player setShouldAutoPlay:self.setting.isAutoPlay];
        
        //设置解码方式:(软解、硬解)
        [player setVideoDecoderMode:self.setting.isHardware ? CNC_VIDEO_DECODER_MODE_HARDWARE : CNC_VIDEO_DECODER_MODE_SOFTWARE];
        
        //设置延时追赶(直播场景下使用，如果在点播使用会导致缓存不断被清除)
        [player setShouldAutoClearCache:self.setting.isAutoClearCache];
        
        //设置是否后台播放
        [player setAbleVideoPlayingInBackground:self.setting.isPlayBackground];
        
        //设置是否进行后台视频解码，不解码可以节约设备资源，节省电量
        [player setDisableVideoDecodeInBackground:self.setting.isDisableDecodeBackground];
        
        //设置快启，加快首屏
        [player accelerateOpen:self.setting.isAccelerateOpen];
        
        //设置是否兼容其他app的音频，开启时其他app的音频不会中断播放器音频，否则其他app的音频播放会中断播放器的播放
        [player setMixOtherPlayer:self.setting.isMixOtherPlayer];
        
        //设置缓冲最小时长(大小)，卡顿时需加载到这里设置的时长才会恢复播放状态
        if (self.setting.bufferType == 0) {
            [player setMinBufferTime:self.setting.minBufferTime];
        } else{
            [player setMinBufferByte:self.setting.minBufferByte];
        }
        
        //设置延时追赶时间，缓存时长超过此阈值时，进行追赶
        [player setMaxCacheTime:self.setting.maxCacheTime];
        
        //设置低延时模式，延时追赶采用变速播放进行，使追赶更加平滑，点播不生效
        [player setShouldLowLatencyMode:self.setting.isLowLatencyMode];
        
        //设置HLS码率自适应
        [player setHLSAdaptation:self.setting.isHLSAdaptation];
        
        //设置HLS码率自适应的默认播放码率
        [player setDefaultHLSBitrate:self.setting.HLSDefaultBitrate];
        
        //设置解码前缓冲队列大小，默认15MB，设置值较大时则加载更多但影响内存占用
        [player setMaxBufferSize:self.setting.maxBufferSize];
        
        //设置缩放模式，参见CncMpVideoScalingMode
        [player setScalingMode:CNC_MP_VIDEO_SCALE_MODE_ASPECTFIT];
        
        /*
         设置是否开启精准seek
         精准seek会在触发seek时找到相应位置的关键帧后，
         快速解码到seek位置，不会产生找关键帧而导致seek后位置左右跳动
         */
        [player setEnableAccurateSeek:self.setting.isAccurateSeek];
        
        //设置循环播放次数
        [player setLoop:self.setting.loopCount];
        
        //设置HLS解密参数
        [_player openHLSEncryption:self.setting.isOpenHLSEncryption withVideoID:self.setting.hlsDecryptionVideoId];
        
        //设置超级快启
        [player superAccelerate:self.setting.isSuperAccelerate];
        
        //设置dns超时时间
        if (self.setting.isOpenDNSTimeout) {
            [player setCncDNSTimeout:self.setting.dnsTimeout * 1000];
        }
        
        //设置连接超时时间
        if (self.setting.isOpenConnecTimeout) {
            [player setCncConnectTimeout:self.setting.connectTimeout * 1000];
        }
        
        //设置dash默认码率
        if (self.setting.isOpenDashDefaultBandWidth) {
            [player setDefaultDashVideoBandwidth:_setting.dashDefaultVideoBandWidth audioBandwidth:_setting.dashDefaultAudioBandWidth];
        }
        
        //连接其他超时
        //        [_player setCncTimeout:50 * 1000];
        
        if (self.setting.isProxy) {
            switch (self.setting.type) {
                    // socks5
                case 0:
                    [player enableSocks5:self.setting.socksUser pwd:self.setting.socksPwd ip:self.setting.socksIP port:self.setting.socksPort];
                    break;
                    // socks4
                case 1:
                    [player enableSocks4WithIP:self.setting.socksIP port:self.setting.socksPort];
                    break;
                    // http
                case 2:
                    [player enableHttpProxyWithUsername:self.setting.socksUser pwd:self.setting.socksPwd IP:self.setting.socksIP port:self.setting.socksPort isHtpps:self.setting.isHTTPS];
                    break;
                default:
                    break;
            }
        }
        //cnc dash add
        __weak typeof(self) weakSelf = self;
        [player setDashVideoBandwidthBlock:^(NSArray *bandwidthList) {
            NSLog(@"video dash callback :%@",bandwidthList);
            weakSelf.cncDashVideoArray = bandwidthList;
            
        } audioBandwidthBlock:^(NSArray *bandwidthList) {
            NSLog(@"audio dash callback :%@",bandwidthList);
            weakSelf.cncDashAudioArray = bandwidthList;
            
        }];
        
        
        [player prepareToPlay];
    }
    
    return player;
}

- (void)shutdownMultiPlayer {
    if (self.player1) {
        [self.player1 stopScreenRecordWithHandler:^(NSDictionary * _Nullable object, NSError * _Nullable error) {
        }];
        [self.player1 shutdown];
        self.player1 = nil;
    }
    
    if (self.player2) {
        [self.player2 stopScreenRecordWithHandler:^(NSDictionary * _Nullable object, NSError * _Nullable error) {
        }];
        [self.player2 shutdown];
        self.player2 = nil;
    }
    
    if (self.player3) {
        [self.player3 stopScreenRecordWithHandler:^(NSDictionary * _Nullable object, NSError * _Nullable error) {
        }];
        [self.player3 shutdown];
        self.player3 = nil;
    }
    
    if (self.player4) {
        [self.player4 stopScreenRecordWithHandler:^(NSDictionary * _Nullable object, NSError * _Nullable error) {
        }];
        [self.player4 shutdown];
        self.player4 = nil;
    }
}
@end
