//
//  CNCInfoTableViewController.h
//  CNCMediaPlayerDemo
//
//  Created by Hjf on 2018/3/14.
//  Copyright © 2018年 CNC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNCInfoTableViewCell : UITableViewCell

- (id)init;

- (void)setValue:(NSString *)value forKey:(NSString *)key;

@end


@interface CNCInfoTableViewController : UITableViewController

- (id)init;

- (void)setValue:(NSString *)value forKey:(NSString *)key;

@end
