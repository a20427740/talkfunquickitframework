//
//  CNCInfoTableViewController.m
//  CNCMediaPlayerDemo
//
//  Created by Hjf on 2018/3/14.
//  Copyright © 2018年 CNC. All rights reserved.
//
#import "CNCInfoTableViewController.h"

#define COLUMN_COUNT    2
#define CELL_MARGIN     10

@implementation CNCInfoTableViewCell
{
    UILabel *_column[COLUMN_COUNT];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        
        for (int i = 0; i < COLUMN_COUNT; ++i) {
            _column[i] = [[UILabel alloc] init];
            _column[i].textColor = [UIColor redColor];
            _column[i].font = [UIFont fontWithName:@"Menlo-Bold" size:15];
            _column[i].adjustsFontSizeToFitWidth = YES;
            _column[i].numberOfLines = 1;
            _column[i].minimumScaleFactor = 0.5;
            _column[i].textAlignment = i ? NSTextAlignmentLeft : NSTextAlignmentRight;
            [self.contentView addSubview:_column[i]];
        }
    }
    return self;
}

- (void)setValue:(NSString *)value forKey:(NSString *)key
{
    _column[0].text = key;
    _column[1].text = value;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect parentFrame = self.contentView.frame;
    CGRect newFrame    = parentFrame;
    CGFloat nextX      = CELL_MARGIN;
    
    newFrame.origin.x   = nextX;
    newFrame.size.width = parentFrame.size.width * 0.3;
    _column[0].frame    = newFrame;
    nextX               = newFrame.origin.x + newFrame.size.width + CELL_MARGIN;
    
    newFrame.origin.x   = nextX;
    newFrame.size.width = parentFrame.size.width - nextX - CELL_MARGIN;
    _column[1].frame = newFrame;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@interface CNCViewCellData : NSObject
@property(nonatomic) NSString *key;
@property(nonatomic) NSString *value;
@end

@implementation CNCViewCellData
@end

@interface CNCInfoTableViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation CNCInfoTableViewController
{
    NSMutableDictionary *_keyIndexes;
    NSMutableArray      *_DataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)init
{
    self = [super init];
    if (self) {
        _keyIndexes = [[NSMutableDictionary alloc] init];
        _DataArray = [[NSMutableArray alloc] init];
        
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    }
    return self;
}

- (void)setValue:(NSString *)value forKey:(NSString *)key
{
    CNCViewCellData *data = nil;
    NSNumber *index = [_keyIndexes objectForKey:key];
    if (index == nil) {
        data = [[CNCViewCellData alloc] init];
        data.key = key;
        [_keyIndexes setObject:[NSNumber numberWithUnsignedInteger:_DataArray.count]
                        forKey:key];
        [_DataArray addObject:data];
    } else {
        data = [_DataArray objectAtIndex:[index unsignedIntegerValue]];
    }
    
    data.value = value;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    assert(section == 0);
    return _DataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    assert(indexPath.section == 0);
    
    CNCInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"info"];
    if (cell == nil) {
        cell = [[CNCInfoTableViewCell alloc] init];
    }
    
    CNCViewCellData *data = [_DataArray objectAtIndex:indexPath.item];
    
    [cell setValue:data.value forKey:data.key];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 20.f;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
