//
//  ScanQrcodeViewController.h
//  WSQuicKitDemo
//
//  Created by BUJIBUJI可 on 2019/7/17.
//  Copyright © 2019 WangSu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScanQrcodeDelegate <NSObject>

- (void)scanQrcodeResult:(NSString *)metadata;

@end

@interface ScanQrcodeViewController : UIViewController

@property (assign, nonatomic) id<ScanQrcodeDelegate> delegate;

@end

