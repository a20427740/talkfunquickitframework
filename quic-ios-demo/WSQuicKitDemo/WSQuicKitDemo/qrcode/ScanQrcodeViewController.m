//
//  ScanQrcodeViewController.m
//  WSQuicKitDemo
//
//  Created by BUJIBUJI可 on 2019/7/17.
//  Copyright © 2019 WangSu Inc. All rights reserved.
//

#import "ScanQrcodeViewController.h"
#import "CNCDemoFunc.h"

static const CGFloat kBorderW = 100;
static const CGFloat kMargin = 30;

@interface ScanQrcodeViewController ()<UIAlertViewDelegate, AVCaptureMetadataOutputObjectsDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    
    
}

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong)   UIView *maskView;
@property (nonatomic, strong) UIView *scanWindow;
@property (nonatomic, strong) UIImageView *scanNetImageView;

@end

@implementation ScanQrcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.clipsToBounds = YES;
    [self setupMaskView];
    [self setupTipTitleView];
    [self setupNavView];
    [self setupScanWindowView];
    [self beginScanning];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeAnimation) name:@"EnterForeground" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    [self resumeAnimation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)setupTipTitleView {
    UIView*mask = ({
       UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, self.maskView.frame.origin.y +  self.maskView.frame.size.height, self.maskView.frame.size.width, kBorderW)];
        v.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        
        v;
    });
    [self.view addSubview:mask];
    
    UILabel * tipLabel = ({
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - kBorderW*2, self.view.bounds.size.width, kBorderW)];
        lab.text = @"将取景框对准二维码，即可自动扫描";
        lab.textColor = [UIColor whiteColor];
        lab.textAlignment = NSTextAlignmentCenter;
        lab.lineBreakMode = NSLineBreakByWordWrapping;
        lab.numberOfLines = 2;
        lab.font=[UIFont systemFontOfSize:12];
        lab.backgroundColor = [UIColor clearColor];
        
        lab;
    });
    [self.view addSubview:tipLabel];
}

-(void)setupNavView{
    UIButton *backBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(20, 30 + (IPHONE_X ? 24 : 0), 25, 25);
        [btn setBackgroundImage:[UIImage imageNamed:@"qrcode_scan_titlebar_back_nor"] forState:UIControlStateNormal];
        btn.contentMode = UIViewContentModeScaleAspectFit;
        [btn addTarget:self action:@selector(disMiss) forControlEvents:UIControlEventTouchUpInside];
        
        btn;
    });
    [self.view addSubview:backBtn];

    UIButton * albumBtn =  ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, 35, 49);
        btn.center = CGPointMake(self.view.frame.size.width/2, 20 + (IPHONE_X ? 24 : 0) + 49/2.0);
        [btn setBackgroundImage:[UIImage imageNamed:@"qrcode_scan_btn_photo_down"] forState:UIControlStateNormal];
        btn.contentMode = UIViewContentModeScaleAspectFit;
        [btn addTarget:self action:@selector(myAlbum) forControlEvents:UIControlEventTouchUpInside];
        
        btn;
    });
    [self.view addSubview:albumBtn];
    
    UIButton * flashBtn =({
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(self.view.frame.size.width-55, 20 + (IPHONE_X ? 24 : 0), 35, 49);
        [btn setBackgroundImage:[UIImage imageNamed:@"qrcode_scan_btn_flash_down"] forState:UIControlStateNormal];
        btn.contentMode = UIViewContentModeScaleAspectFit;
        [btn addTarget:self action:@selector(openFlash:) forControlEvents:UIControlEventTouchUpInside];
        
        btn;
    });
    [self.view addSubview:flashBtn];
}

- (void)setupMaskView {
    self.maskView = ({
        UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.height)];
        mask.layer.borderColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7].CGColor;
        mask.layer.borderWidth = (self.view.frame.size.height - self.view.frame.size.width) / 2 + kMargin;
        mask.bounds = CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.height);
        mask.center = CGPointMake(self.view.frame.size.width * 0.5, self.view.frame.size.height * 0.5);
        [self.view addSubview:mask];
        
        mask;
    });
}

- (void)setupScanWindowView {
    CGFloat scanWindowH = self.view.frame.size.width - kMargin * 2;
    CGFloat scanWindowW = self.view.frame.size.width - kMargin * 2;
    self.scanWindow = ({
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, scanWindowW, scanWindowH)];
        v.clipsToBounds = YES;
        v.center = CGPointMake( self.view.frame.size.width * 0.5,  self.view.frame.size.height * 0.5);
       
        v;
    });
    [self.view addSubview:self.scanWindow];
    
    self.scanNetImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"scan_net"]];
    CGFloat buttonWH = 18;
    
    UIButton *topLeft = ({
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWH, buttonWH)];
        [btn setImage:[UIImage imageNamed:@"scan_1"] forState:UIControlStateNormal];
        
        btn;
    });
    [self.scanWindow addSubview:topLeft];
    
    UIButton *topRight = ({
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(scanWindowW - buttonWH, 0, buttonWH, buttonWH)];
        [btn setImage:[UIImage imageNamed:@"scan_2"] forState:UIControlStateNormal];
        
        btn;
    });
    [self.scanWindow addSubview:topRight];
    
    UIButton *bottomLeft = ({
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, scanWindowH - buttonWH, buttonWH, buttonWH)];
        [btn setImage:[UIImage imageNamed:@"scan_3"] forState:UIControlStateNormal];
        
        btn;
    });
    [self.scanWindow addSubview:bottomLeft];
    
    UIButton *bottomRight = ({
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(topRight.frame.origin.x, bottomLeft.frame.origin.y, buttonWH, buttonWH)];
        [btn setImage:[UIImage imageNamed:@"scan_4"] forState:UIControlStateNormal];
        
        btn;
    });
    [self.scanWindow addSubview:bottomRight];
}

- (void)beginScanning {
    AVCaptureDevice * device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput * input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    if (!input) return;
    AVCaptureMetadataOutput * output = [[AVCaptureMetadataOutput alloc]init];
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    CGRect scanCrop=[self getScanCrop:self.scanWindow.bounds readerViewBounds:self.view.frame];
    output.rectOfInterest = scanCrop;
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    
    [_session addInput:input];
    [_session addOutput:output];
    output.metadataObjectTypes = @[AVMetadataObjectTypeQRCode,AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code];
    
    AVCaptureVideoPreviewLayer * layer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    layer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    layer.frame = self.view.layer.bounds;
    [self.view.layer insertSublayer:layer atIndex:0];
    [_session startRunning];
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    if (metadataObjects.count>0) {
        [_session stopRunning];
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex : 0 ];
        
        if (self.delegate) {
            [self.delegate scanQrcodeResult:metadataObject.stringValue];
        }
        [self disMiss];
    }
}

#pragma mark-> 我的相册
- (void)myAlbum {
    NSLog(@"我的相册");
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        //1.初始化相册拾取器
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        //2.设置代理
        controller.delegate = self;
 
        controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //4.随便给他一个转场动画
        controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        if (@available(iOS 13.0, *)) {
            controller.modalPresentationStyle = UIModalPresentationFullScreen;
        }
        [self presentViewController:controller animated:YES completion:NULL];
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"设备不支持访问相册，请在设置->隐私->照片中进行设置！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark-> imagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    CIDetector*detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{ CIDetectorAccuracy : CIDetectorAccuracyHigh }];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
        if (features.count >=1) {
            CIQRCodeFeature *feature = [features objectAtIndex:0];
            NSString *scannedResult = feature.messageString;
            
            if (self.delegate) {
                [self.delegate scanQrcodeResult:scannedResult];
            }
            [self disMiss];
        } else {
            UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"该图片没有包含一个二维码！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

#pragma mark-> 闪光灯
-(void)openFlash:(UIButton*)button{
    NSLog(@"闪光灯");
    button.selected = !button.selected;
    if (button.selected) {
        [self turnTorchOn:YES];
    } else {
        [self turnTorchOn:NO];
    }
}

#pragma mark-> 开关闪光灯
- (void)turnTorchOn:(BOOL)on {
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            [device lockForConfiguration:nil];
            if (on) {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
            } else {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
            }
            
            [device unlockForConfiguration];
        }
    }
}

#pragma mark 恢复动画
- (void)resumeAnimation {
    CAAnimation *anim = [self.scanNetImageView.layer animationForKey:@"translationAnimation"];
    if (anim) {
        CFTimeInterval pauseTime = self.scanNetImageView.layer.timeOffset;
        CFTimeInterval beginTime = CACurrentMediaTime() - pauseTime;
        [self.scanNetImageView.layer setTimeOffset:0.0];
        [self.scanNetImageView.layer setBeginTime:beginTime];
        [self.scanNetImageView.layer setSpeed:1.0];
    } else {
        CGFloat scanNetImageViewH = 241;
        CGFloat scanWindowH = self.view.frame.size.width - kMargin * 2;
        CGFloat scanNetImageViewW = self.scanWindow.frame.size.width;
        
        self.scanNetImageView.frame = CGRectMake(0, -scanNetImageViewH, scanNetImageViewW, scanNetImageViewH);
        CABasicAnimation *scanNetAnimation = [CABasicAnimation animation];
        scanNetAnimation.keyPath = @"transform.translation.y";
        scanNetAnimation.byValue = @(scanWindowH);
        scanNetAnimation.duration = 1.0;
        scanNetAnimation.repeatCount = MAXFLOAT;
        [self.scanNetImageView.layer addAnimation:scanNetAnimation forKey:@"translationAnimation"];
        [self.scanWindow addSubview:self.scanNetImageView];
    }
}

#pragma mark-> 获取扫描区域的比例关系
-(CGRect)getScanCrop:(CGRect)rect readerViewBounds:(CGRect)readerViewBounds {
    CGFloat x, y, width, height;
    x = (CGRectGetHeight(readerViewBounds)-CGRectGetHeight(rect))/2/CGRectGetHeight(readerViewBounds);
    y = (CGRectGetWidth(readerViewBounds)-CGRectGetWidth(rect))/2/CGRectGetWidth(readerViewBounds);
    width = CGRectGetHeight(rect)/CGRectGetHeight(readerViewBounds);
    height = CGRectGetWidth(rect)/CGRectGetWidth(readerViewBounds);
    
    return CGRectMake(x, y, width, height);
}

#pragma mark-> 返回
- (void)disMiss {
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"EnterForeground" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self disMiss];
    } else if (buttonIndex == 1) {
        [_session startRunning];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
