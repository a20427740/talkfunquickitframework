//
//  main.m
//  WSQuicKitDemo
//
//  Created by yansheng wei on 2019/6/24.
//  Copyright © 2019 WangSu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
