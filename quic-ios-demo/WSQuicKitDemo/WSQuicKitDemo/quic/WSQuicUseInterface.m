//
//  WSQuicUseInterface.m
//  WSQuicKitDemo
//
//  Created by yansheng wei on 2019/7/10.
//  Copyright © 2019 WangSu Inc. All rights reserved.
//

#import "WSQuicUseInterface.h"

// quic相关库
#import <MUF/MUFEngine.h>
#import <MUF/MUFKitConfig.h>
#import <WSQuicKit/WSQuicKit.h>
#import <MUFCrashKit/MUFCrashKit.h>
#import <MUFCrashKit/MUFCrashKitConfig.h>

@implementation WSQuicUseInterface

+ (instancetype)instance {
  static dispatch_once_t onceToken;
  static WSQuicUseInterface *globalQuicInterface = nil;
  dispatch_once(&onceToken, ^{
    globalQuicInterface = [[self alloc] init];
  });
  
  return globalQuicInterface;
}

+ (void)async_register_quic_kit:(NSString *)appKey with_app_secret:(NSString *)appSecret {
  [[WSQuicUseInterface instance] auth_quic_kit:appKey with_app_secret:appSecret];
}

+ (void)set_rtmp_over_quic:(id)object byPassData:(ByPassData)byPassData; {

  __block NSObject *obj = object;
  dispatch_async(dispatch_get_global_queue(0, 0), ^(){
    NSMutableArray *array = [NSMutableArray array];
    
    if ([obj isKindOfClass:[NSArray class]]) {
      NSArray *objArray = [NSArray arrayWithArray:(NSArray *)obj];
      [objArray enumerateObjectsUsingBlock:^(NSDictionary* dic, NSUInteger idx, BOOL * _Nonnull stop) {
        WSQuicItem *item = [[WSQuicItem alloc] init];
        item.address =[dic objectForKey:@"address"];
        item.port = [[dic objectForKey:@"port"] integerValue];
        item.isDomain = [[dic objectForKey:@"isDomain"] boolValue];
        item.urlRegex = [dic objectForKey:@"urlRegex"];
        item.quicLogStatus = [[dic objectForKey:@"quicLogStatus"] integerValue];
        [array addObject:item];
      }];
    } else if ([obj isKindOfClass:[NSDictionary class]]) {
      NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary *)obj];
      WSQuicItem *item = [[WSQuicItem alloc] init];
      item.address =[dic objectForKey:@"address"];
      item.port = [[dic objectForKey:@"port"] integerValue];
      item.isDomain = [[dic objectForKey:@"isDomain"] boolValue];
      item.urlRegex = [dic objectForKey:@"urlRegex"];
      item.quicLogStatus = [[dic objectForKey:@"quicLogStatus"] integerValue];
      [array addObject:item];
    } else if ([obj isKindOfClass:[NSString class]]) {
      // domain可以是域名也可以是ip
      // 对应isDomain=yes，为域名，反之否。
      NSString *address = (NSString *)obj;
      WSQuicItem *item = [[WSQuicItem alloc] init];
      item.address = address;
      item.port = 1935;
      item.isDomain = NO;
      item.urlRegex = @".*:1935";
      item.quicLogStatus = 1;
      [array addObject:item];
    }
    
      [WSQuicKit setupQuicItems:array byPassData:^(NSArray *data) {
          NSLog(@"byPassData:%@", data);
          byPassData(data);
      }];
  });
}

- (instancetype)init {
  self = [super init];
  if (self) {
    // 监听消息、添加崩溃日志路径、注册组件；
    [self registWSQuicKit];
  }
  return self;
}

#pragma mark - MUF
- (void)auth_quic_kit:(NSString *)appKey with_app_secret:(NSString *)appSecret {
  [MUFEngine setAppKey:appKey appSecret:appSecret];
}

- (void)registWSQuicKit {
  // 监听鉴权结果通知
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notify_auth:) name:kNotifyWSQuicKitAuthDone object:nil];
  // 生成崩溃日志文件路径。
  [self setup_crash_kit];
  
  // 注册组件
  [WSQuicKit registWSQuicKit];
  {
    NSArray *arr = [MUFEngine getKitNameList];
    NSLog(@"getKitNameList = %@",  arr);
  }
}

- (void)setup_crash_kit {
  MUFCrashKitConfig *config = [[MUFCrashKitConfig alloc] init];
  config.crashLogPath = [self get_cache_filename_with_name:@"CNCMUF"];
  config.isDeleteLog = NO;
  [MUFEngine registKit:NSClassFromString(@"MUFCrashKit") withConfiger:config];
}

- (NSString *)get_cache_filename_with_name:(NSString *) filename {
  NSFileManager *fileManager = [[NSFileManager alloc] init];
  NSString *pathDocuments = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  NSString *createPath = nil;
  if (filename) {
    createPath = [NSString stringWithFormat:@"%@/%@", pathDocuments, filename];
  }
  
  // 判断文件夹是否存在，如果不存在，则创建
  if (![[NSFileManager defaultManager] fileExistsAtPath:createPath]) {
    [fileManager createDirectoryAtPath:createPath withIntermediateDirectories:YES attributes:nil error:nil];
  }
  NSString *string = [NSString stringWithFormat:@"%@",createPath];
  
  return string;
}

- (void)notify_auth:(NSNotification *)notify {
  NSLog(@"notify_auth %@ - %@", notify.name, notify.object);
  NSDictionary *d = notify.object;
  if (self.delegate && [self.delegate respondsToSelector:@selector(registerResult:)]) {
    [self.delegate registerResult:[[d objectForKey:@"code"] integerValue]];
  }
}


@end
