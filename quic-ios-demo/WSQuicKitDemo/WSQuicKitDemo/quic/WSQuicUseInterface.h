//
//  WSQuicUseInterface.h
//  WSQuicKitDemo
//
//  Created by yansheng wei on 2019/7/10.
//  Copyright © 2019 WangSu Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ByPassData)(NSArray *data);

@protocol WSQuicUseDelegate;

@interface WSQuicUseInterface : NSObject

@property (nonatomic, weak) id<WSQuicUseDelegate> delegate;

+ (instancetype)instance;

// 异步，结果通过回调返回,此接口只第一次调用有效果，后续无效。
+ (void)async_register_quic_kit:(NSString *)appKey
                with_app_secret:(NSString *)appSecret;


// 设置rtmp over quic 规则，具体传入值请看具体实现，可自行构造。
+ (void)set_rtmp_over_quic:(id)object byPassData:(ByPassData)byPassData;

@end

@protocol  WSQuicUseDelegate <NSObject>

// return Zero is Success，otherwise Failed.
- (void)registerResult:(NSInteger)resultCode;

@end

NS_ASSUME_NONNULL_END
