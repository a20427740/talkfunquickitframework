//
//  AppDelegate.h
//  WSQuicKitDemo
//
//  Created by yansheng wei on 2019/6/24.
//  Copyright © 2019 WangSu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

