//
//  CNCDemoFunc.m
//  CNCMobStreamLibDemo
//
//  Created by 82008223 on 2018/1/23.
//  Copyright © 2018年 chinanetcenter. All rights reserved.
//

#import "CNCDemoFunc.h"
#import <mach/mach.h>

@implementation CNCDemoFunc
+ (NSDictionary *)read_question_file:(NSString *)path
{
    NSString *content = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *dictionary = [[[NSDictionary alloc] initWithDictionary:[CNCDemoFunc jsonStringToKeyValues:content]] autorelease];
    [content release];
    return dictionary;
    
}

//json字符串转化成OC键值对
+ (NSDictionary *)jsonStringToKeyValues:(NSString *)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = nil;
    if (JSONData) {
        responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];
    }
    
    return responseJSON;
}
+ (NSArray *)get_folder_list_with_name:(NSString *)folder_name {
    
    NSString *directory = [CNCDemoFunc get_folder_directory_with_name:folder_name];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:directory]) {
        NSError *err = nil;
        
        if (![[NSFileManager defaultManager] createDirectoryAtPath:directory
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&err]) {
            NSLog(@"DDFileLogManagerDefault: Error creating logsDirectory: %@", err);
        }
    }
    
    NSArray *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:nil];
    
    return fileNames;
}
+ (NSString *)get_folder_directory_with_name:(NSString *)folder_name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *baseDir = paths.firstObject;
    NSString *directory = [baseDir stringByAppendingPathComponent:folder_name];
    
    return directory;
}
+ (NSString*)convertToJSONData:(id)infoDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:infoDict
                                                       options:0
                                                         error:&error];
    
    NSString *jsonString = @"";
    
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
    }else
    {
        jsonString = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
    }
    
//    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    return jsonString;
}

+ (NSDictionary *)parseUrlHost:(NSString *)urlStr {
    if (!urlStr) {
        return nil;
    }
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString *host = [url host];
    NSNumber *port = [url port];
    NSString *uri = [url path];
    NSString *scheme = [url scheme];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (scheme) {
         [dic setObject:scheme forKey:@"scheme"];
    }
    
    if (host) {
        [dic setObject:host forKey:@"host"];
        NSString *predicateFormat = @"^(1\\d{2}|2[0-4]\\d|25[0-4]|[1-9])\\.+((1\\d{2}|2[0-4]\\d|25[0-5]|\\d)\\.){2}+(1\\d{2}|2[0-4]\\d|25[0-5]|\\d)$";
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", predicateFormat];
        BOOL isIp = [predicate evaluateWithObject:host];
         
         [dic setObject:@(isIp) forKey:@"isIp"];
    }
    
    if (port > 0) {
         [dic setObject:port forKey:@"port"];
    } else {
        NSString *low_sch = [scheme lowercaseString];
        if ([low_sch isEqualToString:@"http"]) {
            [dic setObject:@(80) forKey:@"port"];
        } else if ([low_sch isEqualToString:@"https"]) {
            [dic setObject:@(443) forKey:@"port"];
        } else if ([low_sch isEqualToString:@"rtmp"]) {
            [dic setObject:@(1935) forKey:@"port"];
        }
    }
    
    if (uri) {
        [dic setObject:uri forKey:@"uri"];
    }
    
    NSLog(@"dic:%@", dic);
    return dic;
}

+ (int64_t)memoryUsage {
    int64_t memoryUsageInByte = 0;
    task_vm_info_data_t vmInfo;
    mach_msg_type_number_t count = TASK_VM_INFO_COUNT;
    kern_return_t kernelReturn = task_info(mach_task_self(), TASK_VM_INFO, (task_info_t) &vmInfo, &count);
    if(kernelReturn == KERN_SUCCESS) {
        memoryUsageInByte = (int64_t) vmInfo.phys_footprint;
        NSLog(@"Memory in use (in bytes): %lld", memoryUsageInByte);
    } else {
        NSLog(@"Error with task_info(): %s", mach_error_string(kernelReturn));
    }
    return memoryUsageInByte;
}
@end
