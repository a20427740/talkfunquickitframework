//
//  CNCDemoFunc.h
//  CNCMobStreamLibDemo
//
//  Created by 82008223 on 2018/1/23.
//  Copyright © 2018年 chinanetcenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#define RECORD_CODE_COUNT 50
#define RECORDCOLOR [UIColor colorWithRed:252.f/255 green:51.f/255 blue:66.f/255 alpha:0.3f]

#define IPHONE_X \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})

@interface CNCDemoFunc : NSObject
+ (NSArray *)get_folder_list_with_name:(NSString *)folder_name;
+ (NSString *)get_folder_directory_with_name:(NSString *)folder_name;
+ (NSDictionary *)read_question_file:(NSString *)path;
+ (NSString*)convertToJSONData:(id)infoDict;

/**
 解析URLString
 @param urlStr URLString
 @return     [...forKeys:@"scheme", @"host",@"port",@"uri",@"isIp", nil];
 */
+ (NSDictionary *)parseUrlHost:(NSString *)urlStr;

/**
 获取物理内存使用大小
 @return 物理内存使用大小
 */
+ (int64_t)memoryUsage;
@end
