//
//  CNCRecorderMenuViewController.m
//  CNCIJKPlayerDemo
//
//  Created by mfm on 16/5/12.
//  Copyright © 2016年 cad. All rights reserved.
//

#import "CNCRecorderMenuViewController.h"
#import "CNCVideoRecordViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ScanQrcodeViewController.h"
#import "CNCMobStreamMediaPlayer.h"
#import "WSQuicUseInterface.h"
#import "CNCDemoFunc.h"
#import "CNCPlayerViewController.h"
#import "CNCPlayerCommonFunc.h"


@interface CNCRecorderMenuViewController ()<UIAlertViewDelegate, ScanQrcodeDelegate> {
    NSMutableString *logMessageCache;
}

@property (nonatomic, assign) BOOL bAuthPusher;
@property (nonatomic, assign) BOOL bAuthPuller;

@property (nonatomic, assign) NSInteger pushTag;
@property (nonatomic, assign) NSInteger pullTag;

// 开关控制是否走quic
@property (nonatomic, assign) BOOL bQuicEnable;
// 强制启用QRCODE加载配置，不再识别URL做相应配置。
@property (nonatomic, assign) BOOL bForceUseQrcodeConfig;

@property (nonatomic, strong) NSString *current_hook_host;
@property (nonatomic, strong) NSNumber *current_hook_port;

@property (nonatomic, assign) BOOL isLive;

@property (weak, nonatomic) IBOutlet UITextView *textViewLog;

@end

@implementation CNCRecorderMenuViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.bQuicEnable = YES;
    self.bForceUseQrcodeConfig = NO;
    self.current_hook_host = nil;
    self.current_hook_port = nil;
    self.bAuthPuller = NO;
    self.bAuthPusher = NO;
    self.pushTag = 1;
    self.pullTag = 2;
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"推流测试";
  
    self.navigationItem.rightBarButtonItem = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(scanQrcodeAction:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0 , 0, 30, 25);
        [button setImage:[UIImage imageNamed:@"scan_qrcode"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"scan_qrcode"] forState:UIControlStateHighlighted];
        [self.view addSubview:button];
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
      
        rightItem;
    });
    
    ///视频分辨率数组
    self.array_video_resolution = [NSArray arrayWithObjects:
                                   @"360P 4:3",
                                   @"360P 16:9",
                                   @"480P 4:3",
                                   @"480P 16:9",
                                   @"540P 4:3",
                                   @"540P 16:9",
                                   @"720P 4:3",
                                   @"720P 16:9",
                                   @"高级自定义",
                                   nil];
    ///分辨率-type 映射
    self.array_value_video_resolution = [NSArray arrayWithObjects:
                                         @(CNCVideoResolution_360P_4_3),
                                         @(CNCVideoResolution_360P_16_9),
                                         @(CNCVideoResolution_480P_4_3),
                                         @(CNCVideoResolution_480P_16_9),
                                         @(CNCVideoResolution_540P_4_3),
                                         @(CNCVideoResolution_540P_16_9),
                                         @(CNCVideoResolution_720P_4_3),
                                         @(CNCVideoResolution_720P_16_9),
                                         @(99999),nil];

    NSString *app_id = [[NSUserDefaults standardUserDefaults] objectForKey:kQrCodeKey_StreamPushAppId];
    if([app_id isEqualToString:@"wstest001"]) {
        self.array = [NSArray arrayWithObjects:
                      @"640*480",
                      @"352x288",
                      @"960*540",
                      @"1280*720",
//                                        @"1920x1080",
                      //@"3840x2160",
                      nil];
        
        self.array_value = [NSArray arrayWithObjects:
                            @(CNCResolution_4_3__640x480),
                            @(CNCResolution_5_4__352x288),
                            @(CNCResolution_16_9__960x540),
                            @(CNCResolution_16_9__1280x720),
//                                                    @(CNCResolution_16_9__1920x1080),
                            nil];
    } else {
        self.array = [NSArray arrayWithObjects:
                      @"640*480",
                      //                  @"352x288",
                      //                  @"960*540",
                      @"1280*720",
                      //                  @"1920x1080",
                      //@"3840x2160",
                      nil];
        self.array_value = [NSArray arrayWithObjects:
                            @(CNCResolution_4_3__640x480),
                            //                        @(CNCResolution_5_4__352x288),
                            //                        @(CNCResolution_16_9__960x540),
                            @(CNCResolution_16_9__1280x720),
                            //                        @(CNCResolution_16_9__1920x1080),
                            nil];
    }
    
    self.cur_idx = 0;
    
    self.cur_idx_video = 2;
    
    CGFloat height= 40;
    {
        self.use_recommend_video_resolution = YES;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(_screenWidth/3.0, _currentUIMaxY, _screenWidth/3.0, height);
        [btn setTitle:[self.array_video_resolution objectAtIndex:self.cur_idx_video] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(action_set_video_resolution:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        btn.tag = 1009;
        
        
    }
    
    
    _currentUIMaxY += height;
    
    {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(_screenWidth/3.0, _currentUIMaxY, _screenWidth/3.0, height);
        [btn setTitle:[self.array objectAtIndex:self.cur_idx] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(action_switch_w_x_h:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        btn.tag = 1001;
        
        btn.hidden = self.use_recommend_video_resolution;
        [self set_def_w_x_h];
    }
    
    _currentUIMaxY += height;
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(100, _currentUIMaxY, _screenWidth-200, 50);
        [btn setTitle:@"开始录制" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(action_start_push:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    
    //初始化拉流输入框
    _currentUIMaxY += height;
    [self infrastrucnture_init_media_play_address_input];
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(50, _currentUIMaxY, 100, 50);
        [btn setTitle:@"直播拉流" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(action_start_live:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(_screenWidth - 150, _currentUIMaxY, 100, 50);
        [btn setTitle:@"点播拉流" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(action_start_play:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }

    _currentUIMaxY += 100;
    {
        CGRect rect = CGRectMake(20, _currentUIMaxY, 80, 30);
        UILabel *label = [[[UILabel alloc]init] autorelease];
        label.frame = rect;
        label.text = @"后台推流：";
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:15.f];
        [self.view addSubview:label];
      
        rect = CGRectMake(100, _currentUIMaxY, 40, 30);
        UISwitch *continuePushSwitch = [[[UISwitch alloc] initWithFrame:rect] autorelease];
        BOOL bPushInBk = [CNCMobStreamSDK isContinuePushInBk];
        [continuePushSwitch setSelected:bPushInBk];
        [continuePushSwitch setOn:bPushInBk];
        [continuePushSwitch addTarget:self action:@selector(actionOpenOrClosePushInBk:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:continuePushSwitch];
    }
    
    {
        CGRect rect = CGRectMake(200, _currentUIMaxY, 40, 30);
        UILabel *label = [[[UILabel alloc]init] autorelease];
        label.frame = rect;
        label.text = @"quic:";
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:15.f];
        [self.view addSubview:label];
        
        rect = CGRectMake(240, _currentUIMaxY, 40, 30);
        UISwitch *continuePushSwitch = [[[UISwitch alloc] initWithFrame:rect] autorelease];
        BOOL bPushInBk = self.bQuicEnable;
        [continuePushSwitch setSelected:bPushInBk];
        [continuePushSwitch setOn:bPushInBk];
        [continuePushSwitch addTarget:self action:@selector(actionOpenOrcloseSocks5:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:self.socksSwitch = continuePushSwitch];
    }
    
    _currentUIMaxY += 40;
    
    self.textViewLog = [[UITextView alloc] initWithFrame:CGRectMake(10, _currentUIMaxY, _screenWidth - 20, _screenHeight - _currentUIMaxY - 10)];
    self.textViewLog.editable = NO;
    [self.view addSubview:self.textViewLog];
}

- (void)scanQrcodeAction:(UIButton *)sender {
    ScanQrcodeViewController *scanQrcodeViewController = [ScanQrcodeViewController new];
    scanQrcodeViewController.delegate = self;
    [self.navigationController pushViewController:scanQrcodeViewController animated:YES];
}

- (void)action_set_video_resolution:(UIButton *)btn {
    //初始化AlertView
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:nil];
    //设置标题与信息，通常在使用frame初始化AlertView时使用
    alert.title = @"选择输出视频分辨率";
    //    alert.message = @"AlertViewMessage";
    
    //这个属性继承自UIView，当一个视图中有多个AlertView时，可以用这个属性来区分
    alert.tag = 6;
    //只读属性，看AlertView是否可见
    NSLog(@"%d",alert.visible);
    //通过给定标题添加按钮
    
    for (NSString *s in self.array_video_resolution) {
        [alert addButtonWithTitle:s];
    }
    
    //显示AlertView
    [alert show];
    [alert release];
}



- (void)set_def_w_x_h {
    CNCResolutionType type = [[self.array_value objectAtIndex:self.cur_idx] integerValue];
    
    switch (type) {
        case CNCResolution_4_3__640x480: {
            self.def_width = 640;
            self.def_height = 480;
        }
            break;
        case CNCResolution_5_4__352x288: {
            self.def_width = 352;
            self.def_height = 288;
        }
            break;
        case CNCResolution_16_9__960x540: {
            self.def_width = 960;
            self.def_height = 540;
            
        }
            break;
        case CNCResolution_16_9__1280x720: {
            self.def_width = 1280;
            self.def_height = 720;
            
        }
            break;
        case CNCResolution_16_9__1920x1080: {
            self.def_width = 1920;
            self.def_height = 1080;
        }
            break;
        default: {
            self.def_width = 640;
            self.def_height = 480;
            
        }
            break;
    }
    
    self.width_textField.text = [NSString stringWithFormat:@"%@", @(self.def_width)];
    self.height_textField.text = [NSString stringWithFormat:@"%@", @(self.def_height)];
    
}

- (void)action_switch_w_x_h:(UIButton *)btn {
    //初始化AlertView
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:nil];
    //设置标题与信息，通常在使用frame初始化AlertView时使用
    alert.title = @"选择分辨率";
    //    alert.message = @"AlertViewMessage";
    
    //这个属性继承自UIView，当一个视图中有多个AlertView时，可以用这个属性来区分
    alert.tag = 0;
    //只读属性，看AlertView是否可见
    NSLog(@"%d",alert.visible);
    //通过给定标题添加按钮
    
    for (NSString *s in self.array) {
        [alert addButtonWithTitle:s];
    }
    
    //显示AlertView
    [alert show];
    [alert release];
}

#pragma marks -- UIAlertViewDelegate --
//根据被点击按钮的索引处理点击事件
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 0) {
        if (buttonIndex > 0) {
            NSString *s = [self.array objectAtIndex:buttonIndex-1];
            UIButton *btn = [self.view viewWithTag:1001];
            [btn setTitle:s forState:UIControlStateNormal];
            NSInteger new_idx = buttonIndex-1;
            if (new_idx != self.cur_idx) {
                self.cur_idx = new_idx;
                [self set_def_w_x_h];
            }
            
        }
    } else  if (alertView.tag == self.pushTag) {//推流器鉴权
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            UITextField* appid_textfield = [alertView textFieldAtIndex:0];
            UITextField* auth_key_textfield = [alertView textFieldAtIndex:1];
            NSString *app_id = appid_textfield.text;
            NSString *auth_key = auth_key_textfield.text;
        
            [self do_auth_with_id:app_id key:auth_key alertViewTag:alertView.tag];
        }
    } else  if (alertView.tag == self.pullTag) {//播放器鉴权
        if (buttonIndex == alertView.firstOtherButtonIndex) {
            UITextField* appid_textfield = [alertView textFieldAtIndex:0];
            UITextField* auth_key_textfield = [alertView textFieldAtIndex:1];
            NSString *app_id = appid_textfield.text;
            NSString *auth_key = auth_key_textfield.text;
            [self do_auth_with_id:app_id key:auth_key alertViewTag:alertView.tag];
        }
    } else if (alertView.tag == 6) {
        if (buttonIndex > 0) {
            NSString *s = [self.array_video_resolution objectAtIndex:buttonIndex-1];
            UIButton *btn = [self.view viewWithTag:1009];
            [btn setTitle:s forState:UIControlStateNormal];
            NSInteger new_idx = buttonIndex-1;
            if (new_idx != self.cur_idx_video) {
                self.cur_idx_video = new_idx;
            }
            
            if (new_idx == [self.array_video_resolution count]-1) {
                self.use_recommend_video_resolution = NO;
//                self.width_textField.hidden = NO;
//                self.height_textField.hidden = NO;
            } else {
                self.use_recommend_video_resolution = YES;
//                self.need_custom_w_x_h = NO;
//                UIButton *btn = (id)[self.view viewWithTag:6001];
//                [btn setTitle:@"默认宽高" forState:UIControlStateNormal];
//                self.width_textField.hidden = YES;
//                self.height_textField.hidden = YES;
            }
            
            UIButton *btn2 = [self.view viewWithTag:1001];
            UIButton *btn3 = [self.view viewWithTag:6001];
            btn2.hidden = self.use_recommend_video_resolution;
            btn3.hidden = self.use_recommend_video_resolution;
        }
    }

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)action_start_push:(UIButton *)btn {
    if (self.bAuthPusher) {
        [self start_push];
    } else {
        NSString *app_id = [[NSUserDefaults standardUserDefaults] objectForKey:kQrCodeKey_StreamPushAppId];
        NSString *auth_key = [[NSUserDefaults standardUserDefaults] objectForKey:kQrCodeKey_StreamPushAuthKey];

        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"请输入推流器鉴权信息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alertView.tag = self.pushTag;
        [alertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
        
        UITextField* appid_textfield = [alertView textFieldAtIndex:0];
        UITextField* auth_key_textfield = [alertView textFieldAtIndex:1];
        
        appid_textfield.placeholder = @"Please enter your App ID";
        auth_key_textfield.placeholder = @"Please enter your Auth Key";
        
        appid_textfield.secureTextEntry = NO;
        auth_key_textfield.secureTextEntry = NO;
        
        appid_textfield.text = app_id;
        auth_key_textfield.text = auth_key;
        
        [alertView show];
        alertView = nil;
    }
}

- (void)action_start_live:(UIButton *)btn {
    self.isLive = YES;
    if (self.bAuthPuller) {
        [self start_pull];
    } else {
        [self start_auth];
    }
}

- (void)action_start_play:(UIButton *)btn {
    self.isLive = NO;
    if (self.bAuthPuller) {
        [self start_pull];
    } else {
        [self start_auth];
    }
}

- (void)start_auth {
    NSString *app_id = [[NSUserDefaults standardUserDefaults] objectForKey:kQrCodeKey_PlayerAppId];
    NSString *auth_key = [[NSUserDefaults standardUserDefaults] objectForKey:kQrCodeKey_PlayerAuthKey];
    
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"请输入播放器鉴权信息" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.tag = self.pullTag;
    [alertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    UITextField* appid_textfield = [alertView textFieldAtIndex:0];
    UITextField* auth_key_textfield = [alertView textFieldAtIndex:1];
    
    appid_textfield.placeholder = @"Please enter your App ID";
    auth_key_textfield.placeholder = @"Please enter your Auth Key";
    
    appid_textfield.secureTextEntry = NO;
    auth_key_textfield.secureTextEntry = NO;
    
    appid_textfield.text = app_id;
    auth_key_textfield.text = auth_key;
    
    [alertView show];
    alertView = nil;
}

- (void)start_push {
    [self textField_resignFirstResponder];
    
    NSString *rtmp_name = self.rtmp_address_inputTF.text;
    if (rtmp_name.length > 0) {
        // 更新设置hook正则匹配规则
        [[NSUserDefaults standardUserDefaults] setObject:rtmp_name forKey:kQrCodeKey_StreamPushRtmpUrl];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (!self.bForceUseQrcodeConfig && self.bQuicEnable) {
            [self updateQuicConfigWithUrl:rtmp_name isPush:YES];
        } else {
            [self present_push_view:rtmp_name];
        }
    } else {
        NSLog(@"请输入正确的推流地址！");
    }
}

- (void)start_pull {
    [self textField_resignFirstResponder];
    
    NSString *rtmp_name = self.media_address_inputTF.text;
    if (rtmp_name.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:rtmp_name forKey:kQrCodeKey_PlayerRtmpUrl];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // 更新设置hook正则匹配规则
        if (!self.bForceUseQrcodeConfig && self.bQuicEnable) {
            [self updateQuicConfigWithUrl:rtmp_name isPush:NO];
        } else {
            [self present_pull_view:rtmp_name];
        }
    } else {
        NSLog(@"请输入正确的拉流地址！");
    }
}

- (void)present_push_view:(NSString *)rtmp_name {
    CNCStreamCfg *para = [[[CNCStreamCfg alloc] init] autorelease];
    para.rtmp_url = rtmp_name;
    para.encoder_type = self.encoder_type;
    para.camera_position = self.came_pos;
    para.direction = self.direct_type;
    para.video_bit_rate = self.video_bit_rate;
    para.video_fps = self.video_frame_rate;
    
    if (self.came_sel_type == 2) {
        para.has_video = NO;
    }
    if (self.use_recommend_video_resolution) {
        CNCVideoResolutionType video_resolution_type = [[self.array_value_video_resolution objectAtIndex:self.cur_idx_video] integerValue];
        [para set_video_resolution_type:video_resolution_type];
    } else {
        CNCResolutionType resolution_type = [[self.array_value objectAtIndex:self.cur_idx] integerValue];
        
        [para set_camera_resolution_type:resolution_type];
    }
    
    CNCVideoRecordViewController *vc = [[[CNCVideoRecordViewController alloc] init] autorelease];
    vc.stream_cfg = para;
    vc.sw_encoder_priority_type = self.sw_encoder_priority_type;
    vc.bQuicEnable = self.bQuicEnable;
    
    if (@available(iOS 13.0, *)) {
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    [self presentViewController:vc animated:YES completion:^(){}];
}

- (void)present_pull_view:(NSString *)rtmp_name {
    if (self.isLive) {
        CNCMobStreamMediaPlayer *vc = [[[CNCMobStreamMediaPlayer alloc] init] autorelease];
        vc.rtmp_name = rtmp_name;
        vc.bQuicEnable = self.bQuicEnable;
        NSLog(@"pull controller %@, %@", vc, rtmp_name);
        if (@available(iOS 13.0, *)) {
            vc.modalPresentationStyle = UIModalPresentationFullScreen;
        }
        [self presentViewController:vc animated:YES completion:^(){}];
    } else {
        CNCPlayerViewController *vc = [[[CNCPlayerViewController alloc] init] autorelease];
        vc.URL = [NSURL URLWithString:rtmp_name];
        vc.setting = [CNCPlayerSetting defaultSetting];
        vc.bQuicEnable = self.bQuicEnable;
        NSLog(@"pull controller %@, %@", vc, rtmp_name);
        if (@available(iOS 13.0, *)) {
            vc.modalPresentationStyle = UIModalPresentationFullScreen;
        }
        [self presentViewController:vc animated:YES completion:^(){}];
    }
}

- (void)updateQuicConfigWithUrl:(NSString *)url isPush:(BOOL)isPush {
    __block BOOL will_push = isPush;
    __block NSString *block_url = [url copy];
    __weak __typeof__(self) weakSelf = self;
    [self do_block_has_hud:@"配置hook规则" will_dowill_do:^NSInteger{
        __strong __typeof__(self) strongSelf = weakSelf;
        NSDictionary *dic = [CNCDemoFunc parseUrlHost:block_url];
        NSMutableDictionary *config = [NSMutableDictionary dictionaryWithCapacity:5];
        do {
            NSString *host = [dic objectForKey:@"host"];
            NSNumber *current_port = [dic objectForKey:@"port"];
            if (strongSelf.current_hook_host && [host isEqualToString:weakSelf.current_hook_host]
                && (strongSelf.current_hook_port && [current_port integerValue] == [weakSelf.current_hook_port integerValue])) {
                break;
            }
            if (host && host.length > 0) {
                [config setObject:host forKey:@"address"];
                //                [config setObject:host forKey:@"urlRegex"];//由SDK来生成
            }
            
            NSObject *port = [dic objectForKey:@"port"];
            if (port != nil) {
                [config setObject:port forKey:@"port"];
            }
            
            
            BOOL isIP = [[dic objectForKey:@"isIp"] boolValue];
            [config setObject:isIP?@(0):@(1) forKey:@"isDomain"];
            
            BOOL open_quic_log = YES;
            [config setObject:open_quic_log?@(1):@(0) forKey:@"quicLogStatus"];
            
            NSMutableArray *marray = [NSMutableArray arrayWithCapacity:3];
            {
                NSDictionary *dic = [NSDictionary dictionaryWithDictionary:config];
                [marray addObject:dic];
            }
            
            //            if ([[config allKeys] count] > 0) {
            //                NSArray *array = [NSArray arrayWithObjects:@"80", @"443", @"1935", nil];
            //                [array enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            //                    [config setObject:obj forKey:@"port"];
            //                    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:config];
            //                    [marray addObject:dic];
            //                }];
            //            }
            
            NSLog(@"quic config %@-%lu", marray, (unsigned long)[marray count]);
            if ([marray count] > 0) {
                weakSelf.current_hook_host = [config objectForKey:@"address"];
                weakSelf.current_hook_port = [config objectForKey:@"port"];
                [WSQuicUseInterface set_rtmp_over_quic:marray byPassData:^(NSArray * byPassData) {
                    for (NSDictionary *pass in byPassData) {
                        NSInteger code = [[pass objectForKey:@"code"] integerValue];
                        NSString *host = [pass objectForKey:@"host"];
                        NSInteger port = [[pass objectForKey:@"port"] integerValue];
                        
                        NSString *errorLog = [NSString stringWithFormat:@"host:%@ port:%@ error:%@", host, @(port), @(code)];
                        [self showLog:errorLog];
                    }
                }];
                
                sleep(3);
            }
            
        } while (0);
        
        return 0;
        
    } complete:^(NSInteger ret) {
        __strong __typeof__(self) strongSelf = weakSelf;
        if (will_push) {
            [strongSelf present_push_view:block_url];
        } else {
            [strongSelf present_pull_view:block_url];
        }
    }];
    
}


- (void)textField_resignFirstResponder {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.rtmp_address_inputTF resignFirstResponder];
        [self.media_address_inputTF resignFirstResponder];
        [self.width_textField resignFirstResponder];
        [self.height_textField resignFirstResponder];
    });
}

- (void)actionOpenOrClosePushInBk:(UISwitch *)sender {
    
    sender.selected = !sender.isSelected;
    ///设置为可以在后台继续（推流或拉流）音频，不断开链接；
    ///此方法需要在info.list里面添加<UIBackgroundModes:audio>
    ///或打开Capabilities的Background Modes开关，勾选audio选项
    ///如需切到后台时静音推流，请监听UIApplicationWillResignActiveNotification事件，
    ///并在回调方法里面调用[CNCMobStreamSDK set_muted_statu:YES]
    [CNCMobStreamSDK set_whether_continue_push_inBk:sender.isSelected];
    
}

- (void)actionOpenOrcloseSocks5:(UISwitch *)sender {
    if (sender.isOn) {
        self.bQuicEnable = YES;
    } else {
        self.bQuicEnable = NO;
        self.current_hook_host = nil;
        [WSQuicUseInterface set_rtmp_over_quic:nil byPassData:nil];
    }
}

#pragma mark - UIAlertViewDelegate
- (void)do_block_has_hud:(NSString *)showMsg will_dowill_do:(NSInteger (^ __nullable)(void))do_something complete:(void (^)(NSInteger))complete {
    MBProgressHUD* progressHud_ = [[MBProgressHUD alloc] initWithView:self.view];
    progressHud_.removeFromSuperViewOnHide = YES;
    progressHud_.labelText = showMsg;
    [self.view addSubview:progressHud_];
    [progressHud_ show:YES];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSInteger ret = do_something();
        dispatch_sync(dispatch_get_main_queue(), ^{
            [progressHud_ hide:NO];
            complete(ret);
        });
    });
};

- (void)do_auth_with_id:(NSString *)app_id key:(NSString *)auth_key alertViewTag:(NSInteger)tag {
    __block NSInteger bk_tag = tag;
    __weak __typeof__(self) weakSelf = self;
    [self do_block_has_hud:@"正在鉴权" will_dowill_do:^NSInteger{
        __strong __typeof__(self) strongSelf = weakSelf;
        NSInteger ret = -1;
        if (bk_tag == strongSelf.pushTag) {
            [CNCMobStreamSDK set_log_system_enable:YES];
            ret = [CNCMobStreamSDK regist_app:app_id auth_key:auth_key];
            NSLog(@"registApp RET = %@", @(ret));
        } else if (bk_tag == strongSelf.pullTag) {
            ret = [CNCMediaPlayerSDK regist_app:app_id auth_key:auth_key];
        }
        
        sleep(0.5);
        return ret;
    } complete:^(NSInteger ret) {
        __strong __typeof__(self) strongSelf = weakSelf;
        if (ret == CNC_RCode_Com_SDK_Init_Success || ret == CNC_MediaPlayer_RCode_Success) {
            if (bk_tag == strongSelf.pushTag) {
                self.bAuthPusher = YES;
                [[NSUserDefaults standardUserDefaults] setObject:app_id forKey:@"CNCMobSteamSDK_App_ID"];
                [[NSUserDefaults standardUserDefaults] setObject:auth_key forKey:@"CNCMobSteamSDK_Auth_Key"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [strongSelf start_push];
            } else if (bk_tag == strongSelf.pullTag) {
                self.bAuthPuller = YES;
                [[NSUserDefaults standardUserDefaults] setObject:app_id forKey:@"CNCMediaPlayerSDK_App_ID"];
                [[NSUserDefaults standardUserDefaults] setObject:auth_key forKey:@"CNCMediaPlayerSDK_Auth_Key"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [strongSelf start_pull];
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"notice" message:[NSString stringWithFormat:@"SDK鉴权失败 业务接口不可用 ret = %@", @(ret)] delegate:strongSelf cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
}

#pragma --mark ScanQrcodeDelegate
- (void)scanQrcodeResult:(NSString *)metadata {
    NSLog(@"%@", metadata);
    NSError *jsonError;
    NSData *objectData = [metadata dataUsingEncoding:NSUTF8StringEncoding];
    NSObject *result = [NSJSONSerialization JSONObjectWithData:objectData
                                                       options:NSJSONReadingMutableContainers
                                                         error:&jsonError];
    
    NSDictionary *d = (NSDictionary *)result;
    NSObject *quic_hook_config = [d objectForKey:@"quic_cfg"];
    if (quic_hook_config != nil && self.bQuicEnable) {
        [WSQuicUseInterface set_rtmp_over_quic:quic_hook_config byPassData:^(NSArray * byPassData) {
            for (NSDictionary *pass in byPassData) {
                NSInteger code = [[pass objectForKey:@"code"] integerValue];
                NSString *host = [pass objectForKey:@"host"];
                NSInteger port = [[pass objectForKey:@"port"] integerValue];
                
                NSString *errorLog = [NSString stringWithFormat:@"host:%@ port:%@, err:%@", host, @(port), @(code)];
                 [self showLog:errorLog];
            }
        }];
        
        self.bForceUseQrcodeConfig = YES;
    }
}

- (void)handleTapGesture:(id)sender {
    NSLog(@"handleTapGesture");
    [self textField_resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)dealloc {
    
    self.array_encode_way = nil;
    self.array_camera_side = nil;
    self.array_camera_direction = nil;
    self.array_bit_rate = nil;
    self.array_frame_rate = nil;
    
    self.rtmp_address_inputTF = nil;
    self.array = nil;
    self.array_value = nil;
    
    self.array_value_video_resolution = nil;
    [self.width_textField removeFromSuperview];
    self.width_textField = nil;
    [self.height_textField removeFromSuperview];
    self.height_textField = nil;
    self.array_video_resolution = nil;
    
    self.rtmp_config_pickview = nil;
    self.socksSwitch = nil;
    logMessageCache = nil;
    [super dealloc];
}

#pragma mark - 保持竖屏
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return NO;
}

#pragma mark -- private method
- (void)showLog:(NSString *)message {
    if (!logMessageCache) {
        logMessageCache = [[NSMutableString alloc] init];
    }
    //    int maxLen = 10000;
    int maxLine = 200;
    NSArray *lines = [logMessageCache componentsSeparatedByString:@"\n"];
    NSUInteger line = [lines count];
    if (line > maxLine) {
        int loc = line - maxLine;
        NSString *str = [lines objectAtIndex:loc];
        NSRange range = [logMessageCache rangeOfString:str];
        logMessageCache = [NSMutableString stringWithString:[logMessageCache substringFromIndex:range.location]];
        NSLog(@"log msg cut:line=%d loc=%d str=%@,range=%@",line,loc,str,NSStringFromRange(range));
    }
    [logMessageCache appendFormat:@"[%@] %@\n", [CNCPlayerCommonFunc getTimeNow], message];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            self.textViewLog.text = logMessageCache;//[self.textViewLog.text stringByAppendingString:logMessageCache];
            [self.textViewLog scrollRectToVisible:CGRectMake(0, self.textViewLog.contentSize.height-15, self.textViewLog.contentSize.width, 10) animated:YES];
        });
    });
}

@end
