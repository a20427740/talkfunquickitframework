//
//  CNCMediaPlayerFramework.h
//  CNCMediaPlayerFramework
//
//  Created by Chen on 16/8/10.
//  Copyright © 2016年 CNC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMediaPlayerComDef.h"
#import "CNCMediaPlayerController.h"
#import "CNCMediaPlayerSDK.h"
#import "CNCMediaPlayerLiteCtrl.h"
#import "CNCMediaPlayerPreloadMgr.h"

//#ifdef kCNC_Player_Full
//#define CNC_Player_Full 1
//#endif

#define kCNC_MediaPlayerSDK_Version @"1.6.5"

#define kCNC_MediaPlayerSDK_Version_Int 16530

//! Project version number for CNCMediaPlayerFramework.
FOUNDATION_EXPORT double CNCMediaPlayerFrameworkVersionNumber;

//! Project version string for CNCMediaPlayerFramework.
FOUNDATION_EXPORT const unsigned char CNCMediaPlayerFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CNCMediaPlayerFramework/PublicHeader.h>


