//
//  TalkfunQuicKit.h
//  CloudLive
//
//  Created by 莫瑞权 on 2019/11/27.
//  Copyright © 2019 Talkfun. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TalkfunQuicKitDelegate <NSObject>
@optional
- (void)QuicKitPushUrl;

@end

@interface TalkfunQuicKit : NSObject

/** 代理对象 */
@property (nonatomic, weak) id<TalkfunQuicKitDelegate> delegate;
@property(nonatomic,assign)BOOL isInit;//是否单例了
@property(nonatomic,assign)BOOL isQuicKit;//是否已经代理了
@property(nonatomic,strong)NSString *address;
+ (instancetype)shared;

- (void)loadSDK;

@end


