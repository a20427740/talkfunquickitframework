//
//  TalkfunQuicKitFramework.m
//  CloudLive
//
//  Created by 莫瑞权 on 2019/12/2.
//  Copyright © 2019 Talkfun. All rights reserved.
//

#import "TalkfunQuicKitFramework.h"
#import "TalkfunQuicKit.h"
@implementation TalkfunQuicKitFramework
+ (void)load
{
    [[TalkfunQuicKit shared] loadSDK];
}

@end
