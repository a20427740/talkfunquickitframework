//
//  TalkfunQuicKit.m
//  CloudLive
//
//  Created by 莫瑞权 on 2019/11/27.
//  Copyright © 2019 Talkfun. All rights reserved.
//

#import "TalkfunQuicKit.h"
#import <MUF/MUFEngine.h>
#import <MUF/MUFKitConfig.h>
#import <WSQuicKit/WSQuicKit.h>
#import <MUFCrashKit/MUFCrashKit.h>
#import <MUFCrashKit/MUFCrashKitConfig.h>
@implementation TalkfunQuicKit
+ (instancetype)shared {
    static TalkfunQuicKit *s_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [[self alloc] init];
       
    });
    return s_instance;
}
-(void)KillTalkfunQuicKitFramework
{

       if (self.isQuicKit) {
           self.isQuicKit= NO;
          [self set_upQuicItems:@""];
       }
}
- (void)loadSDK
{
        
   
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KillTalkfunQuicKitFramework) name:@"KillTalkfunQuicKitFramework" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(plusClick:) name:@"plusTalkfunQuicKitFramework" object:nil];
}
#pragma mark - 监听通知
- (void)plusClick:(NSNotification *)note
{
 
    NSString *address = note.object;
    //网宿加速
    if (self.isQuicKit) {
    self.isQuicKit= NO;
    [self set_upQuicItems:@""];
    }
    //初始化数据
    [self registWSQuicKit];
    //f设置域名
    self.address  = address;
    
    
    NSDictionary * params = note.userInfo;
    NSString *AppKey = params[@"AppKey"];
    NSString *AppSecret = params[@"AppSecret"];
    //鉴权
    [self Login_setAppKey:AppKey setAppSecret:AppSecret];
    if (self.isInit) {
        [self set_upQuicItems:address];
    }
    

}
- (void)registWSQuicKit {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // 监听鉴权结果通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notify_auth:) name:kNotifyWSQuicKitAuthDone object:nil];
        // 生成崩溃日志文件路径。
        [self setup_crash_kit];
        
        // 注册组件
        [WSQuicKit registWSQuicKit];
        {
            NSArray *arr = [MUFEngine getKitNameList];
            NSLog(@"getKitNameList = %@",  arr);
        }
    });
}
 // 监听鉴权结果通知
- (void)notify_auth:(NSNotification *)notify {
  
    __block NSDictionary *d = notify.object;
    dispatch_async(dispatch_get_main_queue(), ^{

        if ([[d objectForKey:@"code"] integerValue] == 0 && [[d objectForKey:@"srvRet"] boolValue]) {
            NSLog(@"MUF鉴权成功");
            if(self.address.length>0){
                   self.isInit = YES;
                [self set_upQuicItems:self.address];
            }
          
        }else{
              NSLog(@"MUF鉴权失败%@",d);
        }

        //});
    });
}
- (void)setup_crash_kit {
    MUFCrashKitConfig *config = [[MUFCrashKitConfig alloc] init];
    config.crashLogPath = [self get_cache_filename_with_name:@"CNCMUF"];
    config.isDeleteLog = NO;
    [MUFEngine registKit:NSClassFromString(@"MUFCrashKit") withConfiger:config];
}
- (NSString *)get_cache_filename_with_name:(NSString *) filename {
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *pathDocuments = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *createPath = nil;
    if (filename) {
        createPath = [NSString stringWithFormat:@"%@/%@", pathDocuments, filename];
    }
    
    // 判断文件夹是否存在，如果不存在，则创建
    if (![[NSFileManager defaultManager] fileExistsAtPath:createPath]) {
        [fileManager createDirectoryAtPath:createPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *string = [NSString stringWithFormat:@"%@",createPath];
    
    return string;
}
//鉴权中
- (void)Login_setAppKey:(NSString*)AppKey setAppSecret:(NSString*)appSecret
{
       self.isQuicKit = YES;
 
     static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
     
      [MUFEngine setAppKey:AppKey appSecret:appSecret];
    });
    
}

- (void)set_upQuicItems:(NSString*)address {

       
         NSMutableArray *array = [NSMutableArray array];
        
//        if([address isKindOfClass:[NSString class] ]){
        if (address.length>0) {
            //推流
             WSQuicItem *item = [[WSQuicItem alloc] init];
             item.address = address;//这边填写您的推流域名
             item.port = 1935;
             item.isDomain = YES;
             item.urlRegex = @".*:1935";
             [array addObject:item];
        }
//        }
        
         [WSQuicKit setupQuicItems:array byPassData:^(NSArray *data) {
                  NSLog(@"byPassData:%@", data);
                //  byPassData(data);
        }];
        
        //让外面 重新 推流
        if (array.count>0) {
//             if ([self.delegate respondsToSelector:@selector(QuicKitPushUrl)]) {
//                   [self.delegate QuicKitPushUrl];
//               }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{


                    [[NSNotificationCenter defaultCenter] postNotificationName:@"getTalkfunQuicKitFramework" object:nil userInfo:@{}];
                            
              });
           
        }
   
}

@end
