
demo源码示例使用教程：

	1.“quic-ios-demo”为demo源码，嵌入framework可参考demo设置；
	2.“Release”为网宿公司提供的quic功能相关的sdk。
	3.demo示例中使用的CNCMediaPlayerFramework和CNCMobStreamFramework为网宿公司研发的播放器和推     流器，如需使用，须先申请appid和appkey；
	4.demo编译时，会出现错误，是因为需要填入正确的“推流或者拉流的域名、IP、端口”等。

具体接口使用，请查考嵌入说明书。