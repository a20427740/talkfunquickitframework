//
//  WSQuicKit.h
//  WSQuicKit
//
//  Created by mfm on 2019/6/20.
//  Copyright © 2019 ws. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MUF/MUFKit.h>

//! Project version number for WSQuicKit.
FOUNDATION_EXPORT double WSQuicKitVersionNumber;

//! Project version string for WSQuicKit.
FOUNDATION_EXPORT const unsigned char WSQuicKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WSQuicKit/PublicHeader.h>

//
extern NSString *kNotifyWSQuicKitAuthDone;

@interface WSQuicItem : NSObject

// 进行推流或者拉流的源ip或者域名
@property (nonatomic, retain) NSString *address;

// 进行推流或者拉流的端口号，有效值[1-65534];
@property (nonatomic) NSInteger port;

// address是域名还是IP 是域名填YES 是IP填NO
@property (nonatomic) BOOL isDomain;

// hook正则匹配规则，可以传nil sdk内部根据address port自己拼接
@property (nonatomic, retain) NSString *urlRegex;

// quic日志状态（0：关闭日志  1：开启日志）
@property (nonatomic, assign) NSInteger quicLogStatus;

@end

// tcp回源回调
typedef void(^ByPassData)(NSArray *data);

@interface WSQuicKit : MUFKit {
    
}



+ (NSString *)getVersion;

/*! @brief 注册组件 在注册前要监听kNotifyWSQuicKitAuthDone以确定是否注册成功
 *
 * @return 返回值0指示组件注册成功 返回值-1指示组件创建失败  返回值-2指示组件已经注册过
 */
+ (int)registWSQuicKit;


/*! @brief 配置quic设置
 *
 * @param array 设置当前要进行over-quic的流设置 每次设置全局生效 成员类型为WSQuicItem
 *              array为nil或者count为0时 表示当前不需要进行over-quic处理
  * @param byPassData tcp回源回调，quic设置失败时会有回调信息
 */
+ (void)setupQuicItems:(NSArray *)array byPassData:(ByPassData)byPassData;


/*! @brief 上传quic日志
 *
 */
+ (void)reportQuicLog;


@end

