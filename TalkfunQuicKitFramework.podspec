Pod::Spec.new do |s|
    s.name             = "TalkfunQuicKitFramework"
    s.version          = "1.3.3"
    s.summary          = "IJKPlayer framework。"
    s.license          = 'LGPLv2.1'
    s.author           = { "littleplayer" => "20727740@qq.com" }
    s.homepage         = "https://bitbucket.org/a20427740/talkfunquickitframework/src/master/"
    s.source           = { :git => "https://a20427740@bitbucket.org/a20427740/talkfunquickitframework.git", :tag => s.version.to_s }
    s.platform     = :ios, '8.0'
    s.requires_arc = true
    s.vendored_frameworks = 'Release/MUF.framework' ,'Release/MUFCrashKit.framework' , 'Release/MUFTUProxyKit.framework', 'Release/WSQuicKit.framework' , 'Release/TalkfunQuicKitFramework.framework'
    s.frameworks  = "AudioToolbox", "AVFoundation", "CoreGraphics", "CoreMedia", "CoreVideo", "MobileCoreServices", "OpenGLES", "QuartzCore", "VideoToolbox", "Foundation", "UIKit", "MediaPlayer"
    s.libraries   = "bz2", "z", "c++"
end
